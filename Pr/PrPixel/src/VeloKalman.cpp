/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"

#include "Event/PrFittedForwardTracks.h"
#include "Event/PrForwardTracks.h"
#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"

#include "PrKernel/PrPixelFastKalman.h"
/**
 * Velo only Kalman fit
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */
namespace LHCb::Pr::Velo {

  class Kalman : public Gaudi::Functional::Transformer<Fitted::Forward::Tracks(
                     const EventContext&, const Hits&, const Tracks&, const Forward::Tracks& )> {
    using TracksVP  = Tracks;
    using TracksFT  = Forward::Tracks;
    using TracksFit = Fitted::Forward::Tracks;
    using dType     = SIMDWrapper::scalar::types;
    using I         = dType::int_v;
    using F         = dType::float_v;

  public:
    Kalman( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"TracksVPLocation", "Rec/Track/VP"},
                        KeyValue{"TracksFTLocation", "Rec/Track/FT"}},
                       KeyValue{"OutputTracksLocation", "Rec/Track/Fit"} ) {}

    StatusCode initialize() override {
      StatusCode sc = Transformer::initialize();
      if ( sc.isFailure() ) return sc;
      return StatusCode::SUCCESS;
    };

    TracksFit operator()( const EventContext& evtCtx, const Hits& hits, const TracksVP& tracksVP,
                          const TracksFT& tracksFT ) const override {
      // Forward tracks and its fit are zipable as there is a one to one correspondence.
      TracksFit out{&tracksFT, tracksFT.zipIdentifier(), LHCb::getMemResource( evtCtx )};
      m_nbTracksCounter += tracksFT.size();

      for ( int t = 0; t < tracksFT.size(); t += dType::size ) {
        auto loop_mask = dType::loop_mask( t, tracksFT.size() );

        // TODO: v2 with less gathers (with transposition)
        // TODO: masked gathers (for avx2/avx512)

        I       idxVP = tracksFT.trackVP<I>( t );
        const F qop   = tracksFT.stateQoP<F>( t );

        auto [stateInfo, chi2, nDof] = trackFitter( hits, tracksVP, idxVP, qop );

        // Store tracks in output
        out.store_trackFT<I>( t, t ); // TODO: index vector (for avx2/avx512)

        out.store_QoP<F>( t, qop );
        out.store_beamStatePos<F>( t, stateInfo.pos );
        out.store_beamStateDir<F>( t, Vec3<F>( stateInfo.tx, stateInfo.ty, 1. ) );
        out.store_covX<F>( t, Vec3<F>( stateInfo.covXX, stateInfo.covXTx, stateInfo.covTxTx ) );
        out.store_covY<F>( t, Vec3<F>( stateInfo.covYY, stateInfo.covYTy, stateInfo.covTyTy ) );

        out.store_chi2<F>( t, chi2 / F( nDof ) );
        out.store_chi2nDof<I>( t, nDof );

        out.size() += dType::popcount( loop_mask );
      }

      return out;
    };

  private:
    PrPixel::SimplifiedKalmanFilter<F, I> trackFitter;

    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::Kalman, "VeloKalman" )
