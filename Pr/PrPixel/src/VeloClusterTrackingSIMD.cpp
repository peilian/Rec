/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "DetDesc/IConditionDerivationMgr.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "PrKernel/PrPixelUtils.h"

// Local
#include "VPClusCache.h"
#include "VPDet/VPDetPaths.h"
#include "VeloClusterTrackingSIMD.h"
#include "VeloKalmanHelpers.h"

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::ClusterTrackingSIMD, "VeloClusterTrackingSIMD" )
namespace LHCb::Pr::Velo {

  namespace {
    //=============================================================================
    // Internal data structures:
    //=============================================================================

    class LightTracksSoA {
      constexpr static int max_tracks = 2048;
      constexpr static int max_hits   = 26;
      constexpr static int max_size   = max_tracks * ( max_hits + 12 );

    public:
      inline int  size() const { return m_size; }
      inline int& size() { return m_size; }

      VEC3_SOA_ACCESSOR( p0, (float*)&m_data[( 0 ) * max_tracks], (float*)&m_data[( 1 ) * max_tracks],
                         (float*)&m_data[( 2 ) * max_tracks] )

      VEC3_SOA_ACCESSOR( p1, (float*)&m_data[( 3 ) * max_tracks], (float*)&m_data[( 4 ) * max_tracks],
                         (float*)&m_data[( 5 ) * max_tracks] )

      VEC3_SOA_ACCESSOR( p2, (float*)&m_data[( 6 ) * max_tracks], (float*)&m_data[( 7 ) * max_tracks],
                         (float*)&m_data[( 8 ) * max_tracks] )

      SOA_ACCESSOR( nHits, &m_data[9 * max_tracks] )

      SOA_ACCESSOR( skipped, &m_data[10 * max_tracks] )

      SOA_ACCESSOR( sumScatter, (float*)&m_data[11 * max_tracks] )

      SOA_ACCESSOR_VAR( hit, &m_data[( 12 + h ) * max_tracks], int h )

    private:
      alignas( 64 ) int m_data[max_size];
      int m_size = 0;
    };

    class HitsPlane {
      constexpr static int max_hits = 1024;
      constexpr static int max_size = max_hits * 5;

    public:
      inline int  size() const { return m_size; }
      inline int& size() { return m_size; }

      VEC3_SOA_ACCESSOR( pos, (float*)&m_data[( 0 ) * max_hits], (float*)&m_data[( 1 ) * max_hits],
                         (float*)&m_data[( 2 ) * max_hits] )

      SOA_ACCESSOR( phi, (float*)&m_data[( 3 ) * max_hits] )

      SOA_ACCESSOR( idx, &m_data[( 4 ) * max_hits] )

      template <typename T, typename M>
      inline void remove( M mask, T indices ) {
        int new_size = m_size;
        for ( int i = 0; i < new_size; i++ ) {
          if ( any( mask && ( T( m_data[4 * max_hits + i] ) == indices ) ) ) {
            new_size--;
            for ( int j = 0; j < 5; j++ ) { m_data[j * max_hits + i] = m_data[j * max_hits + new_size]; }
            i--;
          }
        }
        m_size = new_size;
      }

    private:
      alignas( 64 ) int m_data[max_size];
      int m_size = 0;
    };

    //=============================================================================
    // Union-Find functions used in clustering:
    //=============================================================================
    inline __attribute__( ( always_inline ) ) int find( int* L, int i ) {
      int ai = L[i];
      while ( ai != L[ai] ) ai = L[ai];
      return ai;
    }

    inline __attribute__( ( always_inline ) ) int merge( int* L, int ai, int j ) { // Union
      int aj = find( L, j );
      if ( ai < aj )
        L[aj] = ai;
      else {
        L[ai] = aj;
        ai    = aj;
      }
      return ai;
    }

    //=============================================================================
    // Clustering Algorithm
    //=============================================================================
    inline __attribute__( ( always_inline ) ) void getHits( const uint32_t** VPRawBanks, // List of input Super-Pixels
                                                            const int sensor0, const int sensor1,
                                                            int*        pixel_L,  // Equivalence table
                                                            uint32_t*   pixel_SP, // SP candidates for clustering
                                                            uint32_t*   pixel_S,  // Output Number of pixel per cluster
                                                            uint32_t*   pixel_SX, // Output Sum of pixel x per cluster
                                                            uint32_t*   pixel_SY, // Output Sum of pixel y per cluster
                                                            const DeVP& vp, HitsPlane& Pout, Velo::Hits& hits ) {
      using simd = SIMDWrapper::avx256::types;
      using F    = simd::float_v;

      int n_hits = 0; // Number of clusters after filtering
      int offset = hits.size();
      for ( int s = sensor0; s < sensor1; s++ ) {
        const uint32_t* bank    = VPRawBanks[s];
        const uint32_t  nsp     = *bank++;
        int             pixel_N = 0; // Pixels to cluster count
        int             labels  = 0; // Total number of generated clusters

        for ( unsigned int i = 0; i < nsp; ++i ) {
          const uint32_t sp_word = *bank++;
          uint8_t        sp      = SP_getPixels( sp_word );

          if ( 0 == sp ) continue; // protect against zero super pixels.

          const auto sp_row = SP_getRow( sp_word );
          const auto sp_col = SP_getCol( sp_word );

          // This SP is isolated, skip clustering :
          if ( SP_isIsolated( sp_word ) ) {
            uint8_t mask = s_SPMasks[sp];

            auto n_kx_ky = s_SPn_kx_ky[sp & mask];
            auto n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
            auto kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
            auto ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

            pixel_SX[labels] = sp_col * n * 2 + kx;
            pixel_SY[labels] = sp_row * n * 4 + ky;
            pixel_S[labels]  = n;
            labels++;

            if ( mask != 0xFF ) { // Add 2nd cluster
              n_kx_ky = s_SPn_kx_ky[sp & ( ~mask )];
              n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
              kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
              ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

              pixel_SX[labels] = sp_col * n * 2 + kx;
              pixel_SY[labels] = sp_row * n * 4 + ky;
              pixel_S[labels]  = n;
              labels++;
            }

            continue;
          }

          // This one is not isolated, add it to clustering :
          uint32_t mask     = 0x7FFFFF00 | s_SPMasks[sp];
          pixel_SP[pixel_N] = sp_word & mask;
          pixel_L[pixel_N]  = pixel_N;
          pixel_N++;

          if ( mask != 0x7FFFFFFF ) {                      // Add 2nd cluster
            pixel_SP[pixel_N] = sp_word & ( mask ^ 0xFF ); // ~ of low 8 bits
            pixel_L[pixel_N]  = pixel_N;
            pixel_N++;
          }
        } // loop over super pixels in raw bank

        // SparseCCL
        // (This version assume SP are ordered by col then row)
        int start_j = 0;
        for ( int i = 0; i < pixel_N; i++ ) { // Pixel Scan
          uint32_t sp_word_i, sp_word_j;
          uint32_t ai;
          uint8_t  spi, spj;
          int      x_i, x_j, y_i, y_j;

          sp_word_i = pixel_SP[i];
          spi       = SP_getPixels( sp_word_i );
          y_i       = SP_getRow( sp_word_i );
          x_i       = SP_getCol( sp_word_i );

          pixel_L[i] = i;
          ai         = i;

          for ( int j = start_j; j < i; j++ ) {
            sp_word_j = pixel_SP[j];
            spj       = SP_getPixels( sp_word_j );
            y_j       = SP_getRow( sp_word_j );
            x_j       = SP_getCol( sp_word_j );
            if ( is_adjacent_8C_SP( x_i, y_i, x_j, y_j, spi, spj ) ) {
              ai = merge( pixel_L, ai, j );
            } else if ( x_j + 1 < x_i )
              start_j++;
          }
        }

        for ( int i = 0; i < pixel_N; i++ ) { // Transitive Closure + CCA
          uint32_t sp_word = pixel_SP[i];
          uint8_t  sp      = SP_getPixels( sp_word );
          uint32_t y_i     = SP_getRow( sp_word );
          uint32_t x_i     = SP_getCol( sp_word );

          auto n_kx_ky = s_SPn_kx_ky[sp];
          auto n       = SPn_kx_ky_getN( n_kx_ky );                // number of pixel in this sp
          auto kx      = x_i * n * 2 + SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
          auto ky      = y_i * n * 4 + SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

          uint32_t l;
          if ( pixel_L[i] == i ) {
            l           = labels++; // new label
            pixel_SX[l] = kx;
            pixel_SY[l] = ky;
            pixel_S[l]  = n;
          } else {
            l = pixel_L[pixel_L[i]]; // transitive closure
            pixel_SX[l] += kx;
            pixel_SY[l] += ky;
            pixel_S[l] += n;
          }
          pixel_L[i] = l;
        }

        auto ltg = vp.ltg( s );
        for ( int i = 0; i < labels; i++ ) {
          uint32_t n = pixel_S[i];

          uint32_t x = pixel_SX[i];
          uint32_t y = pixel_SY[i];

          const uint32_t cx = x / n;
          const uint32_t cy = y / n;

          // store target (3D point for tracking)
          const float fx      = x / static_cast<float>( n ) - cx;
          const float fy      = y / static_cast<float>( n );
          const float local_x = vp.local_x( cx ) + fx * vp.x_pitch( cx );
          const float local_y = ( 0.5f + fy ) * vp.pixel_size();

          const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          auto pos = Vec3<SIMDWrapper::scalar::float_v>( gx, gy, gz );
          hits.store_pos( offset + n_hits, pos );
          hits.store_ChannelId( offset + n_hits, SIMDWrapper::scalar::int_v( VPChannelID( s, cx, cy ) ) );

          Pout.store_pos( n_hits, pos );
          Pout.store_idx( n_hits, SIMDWrapper::scalar::int_v( offset + n_hits ) );

          n_hits++;
        }
      } // Loop over sensors

      // Pre-compute phi
      for ( int i = 0; i < n_hits; i += simd::size ) {
        auto pos = Pout.pos<F>( i );
        Pout.store_phi( i, pos.phi() );
      }

      Pout.size() = n_hits;
      hits.size() += n_hits;
    }

    // ===========================================================================
    // Tracking algorithm
    // ===========================================================================

    template <int N, typename F, typename I>
    inline void closestsHitsInPhi( HitsPlane* P0, F phi1, Vec3<F> p0_pos[N], I p0_idx[N] ) {
      using sI = SIMDWrapper::scalar::int_v;
      using sF = SIMDWrapper::scalar::float_v;

      F distances[N];

      // fill the first hits
      for ( int i = 0; i < N; i++ ) {
        auto sPos = P0->pos<sF>( i );
        auto sIdx = P0->idx<sI>( i );
        auto sPhi = P0->phi<sF>( i );

        p0_pos[i]    = Vec3<F>( sPos.x, sPos.y, sPos.z );
        p0_idx[i]    = sIdx.cast();
        distances[i] = abs( phi1 - F( sPhi ) );
      }

      for ( int i = N; i < P0->size(); i++ ) {
        auto sPos = P0->pos<sF>( i );
        auto sIdx = P0->idx<sI>( i );
        auto sPhi = P0->phi<sF>( i );

        Vec3<F> pos = Vec3<F>( sPos.x, sPos.y, sPos.z );
        I       idx = sIdx.cast();
        F       dis = abs( phi1 - F( sPhi ) );

        for ( int j = 0; j < N; j++ ) {
          auto mask = dis < distances[j];

          swap( mask, distances[j], dis );
          swap( mask, p0_pos[j].x, pos.x );
          swap( mask, p0_pos[j].y, pos.y );
          swap( mask, p0_pos[j].z, pos.z );
          swap( mask, p0_idx[j], idx );
        }
      }
    }

    void TrackSeeding( HitsPlane* P0, HitsPlane* P1, HitsPlane* P2, LightTracksSoA* tracks ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      using sI   = SIMDWrapper::scalar::int_v;
      using sF   = SIMDWrapper::scalar::float_v;

      const int N_CANDIDATES = 3;
      Vec3<F>   p0_candidates_pos[N_CANDIDATES];
      I         p0_candidates_idx[N_CANDIDATES];

      if ( P0->size() == 0 || P1->size() == 0 || P2->size() == 0 ) return;

      int new_P1size = 0;
      for ( int h1 = 0; h1 < P1->size(); h1 += simd::size ) {
        auto loop_mask = simd::loop_mask( h1, P1->size() );

        const Vec3<F> p1   = P1->pos<F>( h1 );
        const F       phi1 = P1->phi<F>( h1 );
        const I       vh1  = P1->idx<I>( h1 );

        F bestFit = TrackParams::max_scatter_seeding;
        I vh0     = 0;
        I vh2     = 0;

        Vec3<F> bestP0 = Vec3<F>( 0, 0, 0 );
        Vec3<F> bestP2 = Vec3<F>( 0, 0, 0 );

        closestsHitsInPhi<N_CANDIDATES>( P0, phi1, p0_candidates_pos, p0_candidates_idx );

        for ( int h2 = 0; h2 < P2->size(); h2++ ) {
          const I        cid2 = P2->idx<sI>( h2 ).cast();
          const Vec3<sF> sp2  = P2->pos<sF>( h2 );
          const Vec3<F>  p2   = Vec3<F>( sp2.x, sp2.y, sp2.z );

          auto m_found_h0 = simd::mask_false();

          for ( int i = 0; i < N_CANDIDATES; i++ ) {
            if ( i >= P0->size() ) break;

            const Vec3<F> p0 = p0_candidates_pos[i];
            const I       h0 = p0_candidates_idx[i];

            const F vr = ( p2.z - p0.z ) / ( p1.z - p0.z );
            const F x  = ( p1.x - p0.x ) * vr + p0.x;
            const F y  = ( p1.y - p0.y ) * vr + p0.y;

            const F dx      = x - p2.x;
            const F dy      = y - p2.y;
            const F scatter = dx * dx + dy * dy;

            auto m_bestfit = loop_mask && ( scatter < bestFit );

            if ( none( m_bestfit ) ) continue;

            m_found_h0 = m_found_h0 || m_bestfit;

            bestFit  = select( m_bestfit, scatter, bestFit );
            bestP0.x = select( m_bestfit, p0.x, bestP0.x );
            bestP0.y = select( m_bestfit, p0.y, bestP0.y );
            bestP0.z = select( m_bestfit, p0.z, bestP0.z );
            vh0      = select( m_bestfit, h0, vh0 );
          } // n_0_candidates
          bestP2.x = select( m_found_h0, p2.x, bestP2.x );
          bestP2.y = select( m_found_h0, p2.y, bestP2.y );
          bestP2.z = select( m_found_h0, p2.z, bestP2.z );
          vh2      = select( m_found_h0, cid2, vh2 );
        } // h2

        auto bestH0 = loop_mask && ( bestFit < TrackParams::max_scatter_seeding );

        // Remove used hits
        auto m_remove_p1 = loop_mask && !bestH0;
        P1->compressstore_pos( new_P1size, m_remove_p1, p1 );
        P1->compressstore_phi( new_P1size, m_remove_p1, phi1 );
        P1->compressstore_idx( new_P1size, m_remove_p1, vh1 );
        new_P1size += simd::popcount( m_remove_p1 );

        if ( none( bestH0 ) ) continue;

        P0->remove( bestH0, vh0 );
        P2->remove( bestH0, vh2 );

        int i = tracks->size();

        tracks->compressstore_sumScatter( i, bestH0, bestFit );
        tracks->compressstore_p0( i, bestH0, bestP0 );
        tracks->compressstore_p1( i, bestH0, p1 );
        tracks->compressstore_p2( i, bestH0, bestP2 );
        tracks->store_nHits( i, I( 3 ) );
        tracks->store_skipped( i, I( 0 ) );
        tracks->compressstore_hit( i, 0, bestH0, vh0 );
        tracks->compressstore_hit( i, 1, bestH0, vh1 );
        tracks->compressstore_hit( i, 2, bestH0, vh2 );

        tracks->size() += simd::popcount( bestH0 );
      } // h1

      P1->size() = new_P1size;
    }

    void TrackForwarding( const LightTracksSoA* tracks, HitsPlane* P2, LightTracksSoA* tracks_forwarded,
                          LightTracksSoA* tracks_finalized ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      using sI   = SIMDWrapper::scalar::int_v;
      using sF   = SIMDWrapper::scalar::float_v;

      tracks_forwarded->size() = 0;

      for ( int t = 0; t < tracks->size(); t += simd::size ) {
        const auto mask = simd::loop_mask( t, tracks->size() );

        Vec3<F> p0 = tracks->p1<F>( t );
        Vec3<F> p1 = tracks->p2<F>( t );

        F       bestFit = TrackParams::max_scatter_forwarding;
        Vec3<F> bestP2  = p1;
        I       bestH2  = 0;

        const F tx = ( p1.x - p0.x ) / ( p1.z - p0.z );
        const F ty = ( p1.y - p0.y ) / ( p1.z - p0.z );

        for ( int h2 = 0; h2 < P2->size(); h2++ ) {
          const Vec3<sF> sp2 = P2->pos<sF>( h2 );
          const Vec3<F>  p2  = Vec3<F>( sp2.x, sp2.y, sp2.z );

          const F dz = p2.z - p0.z;
          const F x  = tx * dz + p0.x;
          const F y  = ty * dz + p0.y;

          const F dx      = x - p2.x;
          const F dy      = y - p2.y;
          const F scatter = dx * dx + dy * dy;

          const auto m_bestfit = mask && ( scatter < bestFit );

          if ( none( m_bestfit ) ) continue;

          bestFit  = select( m_bestfit, scatter, bestFit );
          bestP2.x = select( m_bestfit, p2.x, bestP2.x );
          bestP2.y = select( m_bestfit, p2.y, bestP2.y );
          bestP2.z = select( m_bestfit, p2.z, bestP2.z );
          bestH2   = select( m_bestfit, I( P2->idx<sI>( h2 ).cast() ), bestH2 );
        }

        auto bestH0 = mask && ( bestFit < TrackParams::max_scatter_forwarding );

        // Finish loading tracks
        const Vec3<F> tp0         = tracks->p0<F>( t );
        I             n_hits      = tracks->nHits<I>( t );
        I             skipped     = tracks->skipped<I>( t );
        F             sum_scatter = tracks->sumScatter<F>( t );

        // increment or reset to 0
        skipped = select( bestH0, 0, skipped + 1 );

        // increment only if we found a hit
        n_hits      = select( bestH0, n_hits + 1, n_hits );
        sum_scatter = select( bestH0, sum_scatter + bestFit, sum_scatter );

        p1.x = select( bestH0, p1.x, p0.x );
        p1.y = select( bestH0, p1.y, p0.y );
        p1.z = select( bestH0, p1.z, p0.z );

        auto m_forward = mask && ( skipped < I( TrackParams::max_allowed_skip + 1 ) );

        // Remove used hits
        P2->remove( bestH0, bestH2 );

        // Forward tracks
        {
          int i = tracks_forwarded->size();

          tracks_forwarded->compressstore_sumScatter( i, m_forward, sum_scatter );
          tracks_forwarded->compressstore_p0( i, m_forward, tp0 );
          tracks_forwarded->compressstore_p1( i, m_forward, p1 );
          tracks_forwarded->compressstore_p2( i, m_forward, bestP2 );
          tracks_forwarded->compressstore_nHits( i, m_forward, n_hits );
          tracks_forwarded->compressstore_skipped( i, m_forward, skipped );

          int max_n_hits = n_hits.hmax( m_forward );
          for ( int j = 0; j < max_n_hits; j++ ) {
            auto push_hit = bestH0 && ( n_hits == I( j + 1 ) );
            I    id       = select( push_hit, bestH2, tracks->hit<I>( t, j ) );
            tracks_forwarded->compressstore_hit( i, j, m_forward, id );
          }

          tracks_forwarded->size() += simd::popcount( m_forward );
        }

        // Finalize track
        auto m_final = mask && !m_forward && ( n_hits > 3 || sum_scatter < TrackParams::max_scatter_3hits );

        if ( none( m_final ) ) continue; // Nothing to finalize

        {
          int i = tracks_finalized->size();

          tracks_finalized->compressstore_sumScatter( i, m_final, sum_scatter );
          tracks_finalized->compressstore_p0( i, m_final, tp0 );
          tracks_finalized->compressstore_p1( i, m_final, p1 );
          tracks_finalized->compressstore_p2( i, m_final, bestP2 );
          tracks_finalized->compressstore_nHits( i, m_final, n_hits );
          tracks_finalized->compressstore_skipped( i, m_final, skipped );

          int max_n_hits = n_hits.hmax( m_final );
          for ( int j = 0; j < max_n_hits; j++ ) {
            I id = tracks->hit<I>( t, j );
            tracks_finalized->compressstore_hit( i, j, m_final, id );
          }

          tracks_finalized->size() += simd::popcount( m_final );
        }
      }
    }

    void copy_remaining( const LightTracksSoA* tracks_candidates, LightTracksSoA* tracks ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;

      for ( int t = 0; t < tracks_candidates->size(); t += simd::size ) {
        auto loop_mask = simd::loop_mask( t, tracks_candidates->size() );

        I n_hits      = tracks_candidates->nHits<I>( t );
        F sum_scatter = tracks_candidates->sumScatter<F>( t );

        auto m_final = loop_mask && ( n_hits > 3 || sum_scatter < TrackParams::max_scatter_3hits );

        int i = tracks->size();

        tracks->compressstore_sumScatter( i, m_final, sum_scatter );
        tracks->compressstore_p0( i, m_final, tracks_candidates->p0<F>( t ) );
        tracks->compressstore_p1( i, m_final, tracks_candidates->p1<F>( t ) );
        tracks->compressstore_p2( i, m_final, tracks_candidates->p2<F>( t ) );
        tracks->compressstore_nHits( i, m_final, n_hits );
        tracks->compressstore_skipped( i, m_final, tracks_candidates->skipped<I>( t ) );

        int max_n_hits = n_hits.hmax( m_final );
        for ( int j = 0; j < max_n_hits; j++ ) {
          I id = tracks_candidates->hit<I>( t, j );
          tracks->compressstore_hit( i, j, m_final, id );
        }

        tracks->size() += simd::popcount( m_final );
      }
    }

  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  ClusterTrackingSIMD::ClusterTrackingSIMD( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer(
            name, pSvcLocator,
            {KeyValue{"RawEventLocation", RawEventLocation::Default}, KeyValue{"DEVP", LHCb::Det::VP::det_path}},
            {KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
             KeyValue{"TracksLocation", "Rec/Track/Velo"}} ) {}

  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<Velo::Hits, Velo::Tracks, Velo::Tracks> ClusterTrackingSIMD::
                                                     operator()( const EventContext& evtCtx, const RawEvent& rawEvent, const DeVP& devp ) const {
    using Tracks = Velo::Tracks;
    using Hits   = Velo::Hits;

    std::tuple<Hits, Tracks, Tracks> result{std::allocator_arg, LHCb::getMemResource( evtCtx ),
                                            Zipping::generateZipIdentifier(), Zipping::generateZipIdentifier(),
                                            Zipping::generateZipIdentifier()};
    auto& [hits, tracksBackward, tracksForward] = result;

    const auto& tBanks = rawEvent.banks( RawBank::VP );
    if ( tBanks.empty() ) return result;

    const unsigned int version = tBanks[0]->version();
    if ( version != 2 ) {
      warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
      return result;
    }

    const uint32_t* VPRawBanks[VPInfos::NSensors];

    // Copy rawbanks pointers to protect against unordered data
    for ( auto iterBank = tBanks.begin(); iterBank != tBanks.end(); iterBank++ ) {
      const uint32_t sensor = ( *iterBank )->sourceID();
      VPRawBanks[sensor]    = ( *iterBank )->data();
    }

    // Clustering buffers
    constexpr int MAX_CLUSTERS_PER_SENSOR = 1024; // Max number of SP per bank
    uint32_t      pixel_SP[MAX_CLUSTERS_PER_SENSOR]; // SP to process
    int           pixel_L[MAX_CLUSTERS_PER_SENSOR]; // Label equivalences
    uint32_t      pixel_SX[MAX_CLUSTERS_PER_SENSOR]; // x sum of clusters' pixels
    uint32_t      pixel_SY[MAX_CLUSTERS_PER_SENSOR]; // y sum of clusters' pixels
    uint32_t      pixel_S[MAX_CLUSTERS_PER_SENSOR]; // number of clusters' pixels

    // Tracking buffers
    HitsPlane  raw_P0, raw_P1, raw_P2, tmp_P;
    HitsPlane *P0 = &raw_P0, *P1 = &raw_P1, *P2 = &raw_P2;

    LightTracksSoA  t_candidates, t_forwarded, tracks;
    LightTracksSoA *tracks_candidates = &t_candidates, *tracks_forwarded = &t_forwarded;

    // Do tracking per plane backward
    {
      const int i0 = VPInfos::NPlanes - 1, i1 = VPInfos::NPlanes - 2, i2 = VPInfos::NPlanes - 3;

      const int sensor0 = i0 * VPInfos::NSensorsPerPlane;
      const int sensor1 = i1 * VPInfos::NSensorsPerPlane;
      const int sensor2 = i2 * VPInfos::NSensorsPerPlane;

      getHits( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               devp, *P0, hits );

      getHits( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               devp, *P1, hits );

      getHits( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               devp, *P2, hits );

      TrackSeeding( P0, P1, P2, tracks_candidates );

      HitsPlane* tmp = P0;
      P0             = P1;
      P1             = P2;
      P2             = tmp;

      for ( int p = VPInfos::NPlanes - 4; p >= 0; p-- ) {
        const int sensor = p * VPInfos::NSensorsPerPlane;
        getHits( VPRawBanks, sensor, sensor + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
                 devp, *P2, hits );

        TrackForwarding( tracks_candidates, P2, tracks_forwarded, &tracks );

        LightTracksSoA* tmp_track = tracks_candidates;
        tracks_candidates         = tracks_forwarded;
        tracks_forwarded          = tmp_track;

        TrackSeeding( P0, P1, P2, tracks_candidates );

        HitsPlane* tmp = P0;
        P0             = P1;
        P1             = P2;
        P2             = tmp;
      }

      copy_remaining( tracks_candidates, &tracks );
    } // plane backward

    // Make LHCb tracks
    using simd = SIMDWrapper::avx256::types;
    using I    = simd::int_v;
    using F    = simd::float_v;
    for ( int t = 0; t < tracks.size(); t += simd::size ) {
      auto loop_mask = simd::loop_mask( t, tracks.size() );

      // Simple fit
      auto p1 = tracks.p0<F>( t );
      auto p2 = tracks.p2<F>( t );
      auto d  = p1 - p2;

      auto tx = d.x / d.z;
      auto ty = d.y / d.z;
      auto x0 = p2.x - p2.z * tx;
      auto y0 = p2.y - p2.z * ty;

      F z_beam = p1.z;
      F denom  = tx * tx + ty * ty;
      z_beam = select( denom < 0.001f * 0.001f, z_beam, -( x0 * tx + y0 * ty ) / denom );

      Vec3<F> dir = Vec3<F>( tx, ty, 1.f );

      auto backwards = ( z_beam > p2.z ) && loop_mask;

      auto n_hits   = tracks.nHits<I>( t );
      int  max_hits = n_hits.hmax( loop_mask );

      // Store backward tracks
      int i = tracksBackward.size();

      tracksBackward.compressstore_nHits( i, backwards, n_hits );
      tracksBackward.compressstore_stateDir( i, 0, backwards, dir );

      for ( int h = 0; h < max_hits; h++ ) {
        tracksBackward.compressstore_hit( i, h, backwards, tracks.hit<I>( t, h ) );
      }

      tracksBackward.size() += simd::popcount( backwards );

      // Store forward tracks
      auto forwards = ( !backwards ) && loop_mask;
      i             = tracksForward.size();

      tracksForward.compressstore_nHits( i, forwards, n_hits );
      tracksForward.compressstore_stateDir( i, 0, forwards, dir );

      auto end_pos = Vec3<F>( x0 + StateParameters::ZEndVelo * tx, y0 + StateParameters::ZEndVelo * ty,
                              StateParameters::ZEndVelo );
      tracksForward.compressstore_statePos( i, 1, forwards, end_pos );
      tracksForward.compressstore_stateDir( i, 1, forwards, dir );

      for ( int h = 0; h < max_hits; h++ ) { tracksForward.compressstore_hit( i, h, forwards, tracks.hit<I>( t, h ) ); }

      tracksForward.size() += simd::popcount( forwards );
    }

    // Fit forwards
    for ( int t = 0; t < tracksForward.size(); t += simd::size ) {
      auto loop_mask = simd::loop_mask( t, tracksForward.size() );

      auto closestToBeam = fitBackward<F, I>( loop_mask, tracksForward, t, hits, 0 );
      closestToBeam.transportTo( closestToBeam.zBeam() );
      tracksForward.store_statePos( t, 0, closestToBeam.pos() );
      tracksForward.store_stateDir( t, 0, closestToBeam.dir() );
      tracksForward.store_stateCovX( t, 0, closestToBeam.covX() );
      tracksForward.store_stateCovY( t, 0, closestToBeam.covY() );

      // Needed to avoid NaN in checkers
      tracksForward.store_stateCovX( t, 1, Vec3<F>( 100.f, 0, 1.f ) );
      tracksForward.store_stateCovY( t, 1, Vec3<F>( 100.f, 0, 1.f ) );
    }

    // Fit backwards
    for ( int t = 0; t < tracksBackward.size(); t += simd::size ) {
      auto loop_mask = simd::loop_mask( t, tracksBackward.size() );

      auto closestToBeam = fitForward<F, I>( loop_mask, tracksBackward, t, hits, 0 );
      closestToBeam.transportTo( closestToBeam.zBeam() );

      tracksBackward.store_statePos( t, 0, closestToBeam.pos() );
      tracksBackward.store_stateDir( t, 0, closestToBeam.dir() );
      tracksBackward.store_stateCovX( t, 0, closestToBeam.covX() );
      tracksBackward.store_stateCovY( t, 0, closestToBeam.covY() );
    }

    m_nbClustersCounter += hits.size();
    m_nbTracksCounter += tracks.size();

    return result;
  }
} // namespace LHCb::Pr::Velo
