/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFTINFO_H
#define PRFTINFO_H 1

#include <array>
#include <map>
#include <string>

/** Constant information of the detector
 *  @author Sebastien Ponce
 *  @date   2016-09-30
 */

namespace PrFTInfo {

  enum Numbers { NFTZones = 24, NFTXLayers = 6, NFTUVLayers = 6, NFTLayers = 12 };

  /** max number of mats is motivated by: 2 sides * 12 layers * 12 modules * 8 mats
   * previously it was given by 2<<11 for an unknown reason, i.e. 2^12 ..
   * TODO: find better (smaller?) number and reasoning
   */
  constexpr inline unsigned maxNumberMats = 4096;

  constexpr unsigned int nbZones() { return Numbers::NFTZones; }

  const std::string FTHitsLocation    = "FT/FTHits";
  const std::string SciFiHitsLocation = "FT/SciFiHits";
  const std::string FTCondLocation    = "Conditions/FT";
  const std::string FTZonesLocation   = "Conditions/FT/FTZones";

  // layer structure of the FT det
  constexpr auto xZonesUpper = std::array{1, 7, 9, 15, 17, 23};
  constexpr auto xZonesLower = std::array{0, 6, 8, 14, 16, 22};

  constexpr auto uvZonesUpper = std::array{3, 5, 11, 13, 19, 21};
  constexpr auto uvZonesLower = std::array{2, 4, 10, 12, 18, 20};

  constexpr auto stereoZones = std::array{2, 3, 4, 5, 10, 11, 12, 13, 18, 19, 20, 21};

} // namespace PrFTInfo

#endif // PRFTINFO_H
