/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "PrKernel/PrFTInfo.h"

#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Utils.h"
#include "SOAContainer/SOAContainer.h"

#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"

#include <Vc/Vc>
#include <vector>

namespace {

  template <int N, typename Container, typename F, typename I>
  I fast_lower_bound( const Container& vec, const I& start, const I& end, const F& value ) {
    I size = end - start;
    I low  = start;
    LHCb::Utils::unwind<0, N>( [&]() {
      I half = size / 2;
      // this multiplication avoids branching
      low += ( vec[low + half] < value ) * ( size - half );
      size = half;
    } );
    do {
      I half = size / 2;
      low += ( vec[low + half] < value ) * ( size - half );
      size = half;
    } while ( size > 0 );

    return low;
  }

} // namespace

/**
 *  @brief Namespace for code related to SciFi hit container
 */
namespace SciFiHits {

  /**
   * @brief increase n by size of one float vector register and make it a multiple
   * @param n size to increase
   * @returns size as multiple of float vector register size plus one register size
   */
  constexpr static std::size_t align_size( std::size_t n ) { return ( n / Vc::float_v::Size + 1 ) * Vc::float_v::Size; }

  /**
   *  @brief SoA container for SciFi hits
   *  @author André Günther
   *  @date 2019-12-17
   */
  struct PrSciFiHits {

    /**
     * the maximum number of possible hits in the SciFiTracker is a hardware limit determined by the
     * output bandwidth of the TELL40. The more exact number is 23450.
     * (for more details ask Sevda Esen and Olivier Le Dortz)
     */
    constexpr static unsigned maxNumberOfHits{align_size( 25000 )};

    // TODO: test ArenaAllocator
    alignas( 64 ) std::vector<float> _x{};
    alignas( 64 ) std::vector<float> _z0{};
    alignas( 64 ) std::vector<float> _yMins{};
    alignas( 64 ) std::vector<float> _yMaxs{};
    alignas( 64 ) std::vector<int> _planeCodes{};
    alignas( 64 ) std::vector<unsigned> _IDs{};
    alignas( 64 ) std::vector<float> _w{};
    alignas( 64 ) std::vector<float> _dzDy{};
    alignas( 64 ) std::vector<float> _dxDy{};

    PrSciFiHits() {
      _x.reserve( maxNumberOfHits );
      _z0.reserve( maxNumberOfHits );
      _yMins.reserve( maxNumberOfHits );
      _yMaxs.reserve( maxNumberOfHits );
      _planeCodes.reserve( maxNumberOfHits );
      _IDs.reserve( maxNumberOfHits );
      _w.reserve( maxNumberOfHits );
      _dzDy.reserve( maxNumberOfHits );
      _dxDy.reserve( maxNumberOfHits );
    };

    /**
     * 24 zones plus 2 extra entries for padding
     * the index in the array corresponds to the zone,
     * i.e. zoneIndexes[0] gives the start index of zone 0
     * upper zones have uneven, lower zones have even numbers
     */
    constexpr static unsigned paddedSize = PrFTInfo::NFTZones + 2;
    alignas( 64 ) std::array<unsigned, paddedSize> zoneIndexes{};

    /**
     * accessors for all above defined variables
     * the vectors can also be accessed directly as they are public
     * this is the case when loading several values into vector registers
     */
    float    x( unsigned index ) const { return _x[index]; }
    float    z( unsigned index ) const { return _z0[index]; }
    float    yMin( unsigned index ) const { return _yMins[index]; }
    float    yMax( unsigned index ) const { return _yMaxs[index]; }
    int      planeCode( unsigned index ) const { return _planeCodes[index]; }
    unsigned ID( unsigned index ) const { return _IDs[index]; }
    float    w( unsigned index ) const { return _w[index]; }
    float    dzDy( unsigned index ) const { return _dzDy[index]; }
    float    dxDy( unsigned index ) const { return _dxDy[index]; }

    float distanceXHit( unsigned index, float x_track ) const { return _x[index] - x_track; }
    float z( unsigned index, float y ) const { return _z0[index] + y * _dzDy[index]; }
    float x( unsigned index, float y ) const { return _x[index] + y * _dxDy[index]; }
    float distance( unsigned index, float x_track, float y_track ) const { return x( index, y_track ) - x_track; }

    /**
     * @brief get size of vectors in SciFiHits
     * @returns size of the _x vector as proxy for the others
     */
    auto size() const -> decltype( _x.size() ) {
      assert( _x.size() == _IDs.size() );
      return _x.size();
    }

    /**
     * @brief wraps std::lower_bound for indices
     * @param start first index of search interval
     * @param end last index of search interval (same behaviour as last iterator)
     * @param val value to compare elements to
     * @returns index of first element that is not less than val, or end if no such element is found
     */
    unsigned get_lower_bound( unsigned start, unsigned end, float val ) const noexcept {
      const auto begin_hits = _x.begin();
      return std::distance( begin_hits, std::lower_bound( begin_hits + start, begin_hits + end, val ) );
    }

    /**
     * @brief a faster binary search
     * @param N controls number of runrolled steps in the search
     * @param start first index of search interval
     * @param end last index of search interval (same behaviour as last iterator)
     * @param val value to compare elements to
     * @returns index of first element that is not less than val, or end if no such element is found
     */
    template <int N>
    unsigned get_lower_bound_fast( const unsigned& start, const unsigned& end, const float& val ) const noexcept {
      return fast_lower_bound<N>( _x, start, end, val );
    }

    /**
     * @brief method to access zone indices
     * @param zone zone number as assigned in PrFTInfo.h
     * @returns std::pair containing the start and end index of the zone
     */
    std::pair<unsigned, unsigned> getZoneIndices( unsigned zone ) const {
      return {zoneIndexes[zone], zoneIndexes[zone + 2] - 1};
    }
  };
  /**
   * @brief namespace for modifiable SciFiHits
   */
  namespace ModSciFiHits {

    /** @brief SoA layout modifiable SciFiHits
     *  @note makes use of SIMDWrapper, underlying std::vector functionality is not really
     * used and could be dropped in the future.
     */

    struct ModPrSciFiHits final {
      alignas( 64 ) std::vector<int> fulldexes{};
      alignas( 64 ) std::vector<float> coords{};
      alignas( 64 ) std::vector<int> permutations{};
      std::size_t capacity{0};
      std::size_t _size{0};

      ModPrSciFiHits() = default;
      ModPrSciFiHits( const std::size_t cap ) : capacity{align_size( cap )} {
        fulldexes.reserve( capacity );
        coords.reserve( capacity );
        permutations.reserve( capacity );
      }

      void updateSizeBy( const std::size_t n ) { _size += n; }
      void clear() { _size = 0; }
      void reserve( const std::size_t n ) {
        capacity = align_size( n );
        fulldexes.reserve( capacity );
        coords.reserve( capacity );
        permutations.reserve( capacity );
      }
      auto     size() const { return _size; }
      bool     isValid( unsigned index ) const { return coords[index] != std::numeric_limits<float>::max(); }
      void     setInvalid( unsigned index ) { coords[index] = std::numeric_limits<float>::max(); }
      unsigned getMainIndex( unsigned permutedIndex ) { return fulldexes[permutations[permutedIndex]]; }
      int      planeCode( unsigned permutedIndex, const PrSciFiHits& SciFiHits ) {
        return SciFiHits.planeCode( fulldexes[permutations[permutedIndex]] );
      }

      SOA_ACCESSOR( fulldex, fulldexes.data() )
      SOA_ACCESSOR( coord, coords.data() )
      SOA_ACCESSOR( permutation, permutations.data() )
    };

    /**
     * @brief AoS layout modifiable SciFiHits
     */
    struct ModPrSciFiHit final {
      ModPrSciFiHit() = default;
      ModPrSciFiHit( const unsigned index, float c, int pc ) : _fulldex{index}, _coord{c}, _planeCode{pc} {}

      unsigned _fulldex{0};
      float    _coord{0.};
      int      _planeCode{0};

      unsigned fulldex() const { return _fulldex; }
      float    coord() const { return _coord; }
      int      planeCode() const { return _planeCode; }
      bool     isValid() const { return _coord != std::numeric_limits<float>::max(); };
      void     setInvalid() { _coord = std::numeric_limits<float>::max(); };
    };

    /**
     * @brief SoA layout modifiable SciFiHits based on SOAContainer
     * The interface looks the same from the outside as for the AoS case above
     * fulldex is the index of hit in PrSciFiHits for easy access of needed numbers
     * coord may be any coordinate (x, y, z .... whatever needed)
     * ModPrSciFiSkin is the AoS skin around the SoA object
     * default getters and setters are inherited
     * @note It might be handy to to define:
     * @code
     * using ModPrSciFiHits = SOA::Container<std::vector, ModSciFiHits::ModPrSciFiSkin>;
     * template <std::size_t SZ>
     * using SmallModPrSciFiHits = SOA::Container<ModSciFiHits::small_vector<SZ>::template type,
     * ModSciFiHits::ModPrSciFiSkin>;
     * @endcode
     */
    SOAFIELD_TRIVIAL( fulldex, fulldex, unsigned );
    SOAFIELD_TRIVIAL( coord, coord, float );
    SOAFIELD_TRIVIAL( planeCode, planeCode, int );
    SOASKIN( ModPrSciFiSkin, fulldex, coord, planeCode ) {
      SOASKIN_INHERIT_DEFAULT_METHODS( ModPrSciFiSkin );
      bool isValid() const noexcept { return this->coord() != std::numeric_limits<float>::max(); }
      void setInvalid() noexcept { this->coord() = std::numeric_limits<float>::max(); }
    };

    /**
     * @brief use as predicate for comparison of ModPrSciFiHit (AoS or SoA)
     */
    struct byCoord {
      template <typename LHS, typename RHS>
      bool operator()( const LHS& lhs, const RHS& rhs ) const noexcept {
        return lhs.coord() < rhs.coord();
      }
    };

    struct byPlane {
      template <typename LHS, typename RHS>
      bool operator()( const LHS& lhs, const RHS& rhs ) const noexcept {
        return lhs.planeCode() < rhs.planeCode();
      }
    };

    template <std::size_t SZ>
    struct small_vector {
      template <typename T>
      using type = boost::container::small_vector<T, SZ>;
    };

    template <std::size_t SZ>
    struct static_vector {
      template <typename T>
      using type = boost::container::static_vector<T, SZ>;
    };

  } // namespace ModSciFiHits
} // namespace SciFiHits
