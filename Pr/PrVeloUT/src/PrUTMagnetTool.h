/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRUTMAGNETTOOL_H
#define PRUTMAGNETTOOL_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include <limits>

// Forward declarations
class IMagneticFieldSvc;
#include <Kernel/LUTForFunction.h>
#include <PrKernel/IPrUTMagnetTool.h>
#include <UTDet/DeUTDetector.h>

/** @class PrUTMagnetTool PrUTMagnetTool.h
 *
 *  Tool used by the upstream pattern recognition to obtain integrals
 *  of the B-field as a function of parameters of a Velo track
 *
 *  @author Mariusz Witek
 *  @date   2006-09-25
 *  @update for A-Team framework 2007-08-20 SHM
 *
 */

namespace LHCb::Pr {

  class UTMagnetTool final : public extends<GaudiTool, IPrUTMagnetTool> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    float bdlIntegral( float ySlopeVelo, float zOrigin, float zVelo ) const override {
      return m_noField ? s_bdlIntegral_NoB : m_lutBdl.getInterpolatedValue( {ySlopeVelo, zOrigin, zVelo} );
    }
    float zBdlMiddle( float ySlopeVelo, float zOrigin, float zVelo ) const override {
      return m_noField ? s_zBdlMiddle_NoB : m_lutZHalfBdl.getInterpolatedValue( {ySlopeVelo, zOrigin, zVelo} );
    }
    float dist2mom( float ySlope ) const override {
      return m_noField ? s_averageDist2mom_NoB : m_lutDxToMom.getValue( {ySlope} );
    }

    //=========================================================================
    // z middle of UT
    //=========================================================================
    float zMidUT() const override { return m_zCenterUT; }
    //=========================================================================
    // z middle of B field betweenz=0 and zMidUT
    //=========================================================================
    float zMidField() const override { return m_noField ? s_zMidField_NoB : m_zMidField; }
    //=========================================================================
    // averageDist2mom
    //=========================================================================
    float averageDist2mom() const override { return m_noField ? s_averageDist2mom_NoB : m_dist2mom; }

    const LUTForFunction<3, 30>&      DxLayTable() const override { return m_lutDxLay; }
    const LUTForFunction<30, 10, 10>& BdlTable() const override { return m_lutBdl; }

  private:
    void       prepareBdlTables();
    void       prepareDeflectionTables();
    StatusCode updateField();

    IMagneticFieldSvc* m_magFieldSvc = nullptr; /// pointer to mag field service
    DeUTDetector*      m_UTDet       = nullptr; ///< UT Detector element
    std::string        m_detLocation;

    std::array<float, 4> m_zLayers = {0};

    float m_zCenterUT = std::numeric_limits<float>::signaling_NaN();
    float m_zMidField = std::numeric_limits<float>::signaling_NaN();
    float m_dist2mom  = std::numeric_limits<float>::signaling_NaN();

    LUTForFunction<30, 10, 10> m_lutBdl{LUT::Range{-0.3, +0.3},
                                        LUT::Range{-250. * Gaudi::Units::mm, 250. * Gaudi::Units::mm},
                                        LUT::Range{0. * Gaudi::Units::mm, 800. * Gaudi::Units::mm}};
    LUTForFunction<30, 10, 10> m_lutZHalfBdl{LUT::Range{-0.3, +0.3},
                                             LUT::Range{-250. * Gaudi::Units::mm, 250. * Gaudi::Units::mm},
                                             LUT::Range{0. * Gaudi::Units::mm, 800. * Gaudi::Units::mm}};
    LUTForFunction<3, 30>      m_lutDxLay{LUT::Range{0., 3.},      // index of UT layer 1 to 4
                                     LUT::Range{0.0, 0.3}};   // slope y
    LUTForFunction<30>         m_lutDxToMom{LUT::Range{0.0, 0.3}}; // slope y;

    bool m_noField = false;
    // set parameters for no field run
    constexpr static float s_bdlIntegral_NoB     = 0.1;
    constexpr static float s_zBdlMiddle_NoB      = 1900.;
    constexpr static float s_zMidField_NoB       = 1900.;
    constexpr static float s_averageDist2mom_NoB = 0.00004;
  };
} // namespace LHCb::Pr
#endif // PRUTMAGNETTOOL_H
