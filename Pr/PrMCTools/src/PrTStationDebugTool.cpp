/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FTLiteCluster.h"
#include "Event/MCParticle.h"
#include "Event/OTTime.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/LHCbID.h"
#include "Linker/LinkedTo.h"
#include "PrKernel/IPrDebugTool.h" // Interface

//-----------------------------------------------------------------------------
// Implementation file for class : PrTStationDebugTool
//
// 2012-03-22 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class PrTStationDebugTool PrTStationDebugTool.h
 *
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 */
class PrTStationDebugTool : public GaudiTool, virtual public IPrDebugTool {
public:
  /// Standard constructor
  PrTStationDebugTool( const std::string& type, const std::string& name, const IInterface* parent );

  bool matchKey( LHCb::LHCbID id, int key ) const override;

  void printKey( MsgStream& msg, LHCb::LHCbID id ) const override;
};
// Declaration of the Tool Factory
DECLARE_COMPONENT( PrTStationDebugTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrTStationDebugTool::PrTStationDebugTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IPrDebugTool>( this );
}

//=========================================================================
//  Check if a given LHCbID is associated to the MCParticle of the specified key
//=========================================================================
bool PrTStationDebugTool::matchKey( LHCb::LHCbID id, int key ) const {
  if ( id.isOT() ) {
    LinkedTo<LHCb::MCParticle> oLink( evtSvc(), msgSvc(), LHCb::OTTimeLocation::Default );
    LHCb::OTChannelID          idO = id.otID();

    LHCb::MCParticle* part = oLink.first( idO );
    while ( 0 != part ) {
      if ( key == part->key() ) return true;
      part = oLink.next();
    }
  } else if ( id.isFT() ) {
    LinkedTo<LHCb::MCParticle> fLink( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );
    LHCb::FTChannelID          idO = id.ftID();

    LHCb::MCParticle* part = fLink.first( idO );
    while ( 0 != part ) {
      if ( key == part->key() ) return true;
      part = fLink.next();
    }
  }
  return false;
}
//=========================================================================
//  Print the list of MCParticle keys associated to the specified LHCbID
//=========================================================================
void PrTStationDebugTool::printKey( MsgStream& msg, LHCb::LHCbID id ) const {
  if ( id.isOT() ) {
    LinkedTo<LHCb::MCParticle> oLink( evtSvc(), msgSvc(), LHCb::OTTimeLocation::Default );
    LHCb::OTChannelID          idO = id.otID();

    LHCb::MCParticle* part = oLink.first( idO );
    if ( 0 != part ) msg << " MC:";
    while ( 0 != part ) {
      msg << " " << part->key();
      part = oLink.next();
    }
  } else if ( id.isFT() ) {
    LinkedTo<LHCb::MCParticle> fLink( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );
    LHCb::FTChannelID          idO = id.ftID();

    LHCb::MCParticle* part = fLink.first( idO );
    if ( 0 != part ) msg << " MC:";
    while ( 0 != part ) {
      msg << " " << part->key();
      part = fLink.next();
    }
  }
}
//=============================================================================
