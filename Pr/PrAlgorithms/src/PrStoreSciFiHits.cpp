/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrSciFiHits.h"

#include <array>
#include <limits>
#include <memory>
#include <string>

#include <Vc/Vc>

#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"
#include "PrKernel/PrFTZoneHandler.h"

#include <boost/numeric/conversion/cast.hpp>

/** @class PrStoreSciFiHits PrStoreSciFiHits.cpp
 *
 *  \brief Transforms FTLiteClusters into the input format needed by the PrForwardTracking
 */

namespace {
  using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
  using namespace SciFiHits;

  struct MatsCache {

    /**
     * partial SoA cache for mats, reserve enough (here 4096 which is more than enough)
     * space for all mats ( all mats should be less than 2 * 8 mats * 12 modules * 12 layers)
     */
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_dxdy;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_dzdy;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_globaldy;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_uBegin;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_halfChannelPitch;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_dieGap;
    std::array<float, PrFTInfo::maxNumberMats>             m_mats_sipmPitch;
    std::array<Gaudi::XYZPointF, PrFTInfo::maxNumberMats>  m_mats_mirrorPoint;
    std::array<Gaudi::XYZVectorF, PrFTInfo::maxNumberMats> m_mats_ddx;

    MatsCache( const DeFTDetector& ftDet ) {
      for ( auto station : ftDet.stations() ) {
        for ( auto layer : station->layers() ) {
          for ( auto quarter : layer->quarters() ) {
            for ( auto module : quarter->modules() ) {
              for ( auto mat : module->mats() ) {
                auto index                     = mat->elementID().uniqueMat();
                m_mats_dxdy[index]             = mat->dxdy();
                m_mats_dzdy[index]             = mat->dzdy();
                m_mats_globaldy[index]         = mat->globaldy();
                m_mats_uBegin[index]           = mat->uBegin();
                m_mats_halfChannelPitch[index] = mat->halfChannelPitch();
                m_mats_dieGap[index]           = mat->dieGap();
                m_mats_sipmPitch[index]        = mat->sipmPitch();
                m_mats_mirrorPoint[index]      = mat->mirrorPoint();
                m_mats_ddx[index]              = mat->ddx();
              }
            }
          }
        }
      }
    }
  };
} // namespace

class PrStoreSciFiHits : public Gaudi::Functional::Transformer<PrSciFiHits( FTLiteClusters const&, MatsCache const& ),
                                                               LHCb::DetDesc::usesConditions<MatsCache>> {
public:
  PrStoreSciFiHits( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"HitsLocation", LHCb::FTLiteClusterLocation::Default},
                      KeyValue{"MatsCache", "AlgorithmSpecific-" + name + "-MatsCache"}},
                     KeyValue{"Output", PrFTInfo::SciFiHitsLocation} ) {}

  StatusCode initialize() override {
    return Transformer::initialize().andThen( [&] {
      addConditionDerivation<MatsCache( const DeFTDetector& )>( DeFTDetectorLocation::Default,
                                                                inputLocation<MatsCache>() );

      // TODO: this should be ~80 micron; get this from a tool?
      std::array<float, 9> clusRes = {0.05f, 0.08f, 0.11f, 0.14f, 0.17f, 0.20f, 0.23f, 0.26f, 0.29f};
      std::transform( clusRes.begin(), clusRes.end(), m_invClusResolution.begin(),
                      []( const float& c ) { return 1.f / c; } );
    } );
  }

  PrSciFiHits operator()( FTLiteClusters const&, MatsCache const& ) const override;

private:
  /// Cached resolution
  std::array<float, 9> m_invClusResolution;
};

DECLARE_COMPONENT_WITH_ID( PrStoreSciFiHits, "PrStoreSciFiHits" )

PrSciFiHits PrStoreSciFiHits::operator()( FTLiteClusters const& clusters, MatsCache const& cache ) const {

  PrSciFiHits tmp{};

  auto& hitvec       = tmp._x;
  auto& z0vec        = tmp._z0;
  auto& yMinvec      = tmp._yMins;
  auto& yMaxvec      = tmp._yMaxs;
  auto& planeCodevec = tmp._planeCodes;
  auto& IDvec        = tmp._IDs;
  auto& werrvec      = tmp._w;
  auto& dzDyvec      = tmp._dzDy;
  auto& dxDyvec      = tmp._dxDy;

  constexpr auto xu  = PrFTInfo::xZonesUpper;
  constexpr auto uvu = PrFTInfo::uvZonesUpper;

  constexpr auto xd  = PrFTInfo::xZonesLower;
  constexpr auto uvd = PrFTInfo::uvZonesLower;

  // hits are stored in the same order as the layers are in z, i.e. x-u-v-x
  constexpr auto hitzones = std::array<int, PrFTInfo::NFTZones>{
      xd[0], uvd[0], uvd[1], xd[1], xd[2], uvd[2], uvd[3], xd[3], xd[4], uvd[4], uvd[5], xd[5],
      xu[0], uvu[0], uvu[1], xu[1], xu[2], uvu[2], uvu[3], xu[3], xu[4], uvu[4], uvu[5], xu[5]};

  auto& zoneIndexes = tmp.zoneIndexes;
  for ( auto i : hitzones ) {
    zoneIndexes[i] = hitvec.size();
    hitvec.emplace_back( -1.e9f );
    z0vec.emplace_back( -1.e9f );
    yMinvec.emplace_back( -1.e9f );
    yMaxvec.emplace_back( -1.e9f );
    planeCodevec.emplace_back( std::numeric_limits<int>::lowest() );
    IDvec.emplace_back( 0 );
    werrvec.emplace_back( -1.e9f );
    dzDyvec.emplace_back( 0.f ); // this should protect lowest in multiplications
    dxDyvec.emplace_back( 0.f );

    for ( int quarter = 0; quarter < 2; quarter++ ) {
      int  iQuarter = i * 2 + quarter;
      uint info     = ( iQuarter >> 1 ) | ( ( ( iQuarter << 4 ) ^ ( iQuarter << 5 ) ^ 128u ) & 128u );
      for ( auto const& clus : clusters.range( iQuarter ) ) { // TODO: store the cluster (it's and unsigned int)
                                                              // directly instead of oh the LHCb ID
        LHCb::FTChannelID id           = clus.channelID();    // for the fitting
        auto              index        = id.uniqueMat();
        float             dxdy         = cache.m_mats_dxdy[index];
        float             dzdy         = cache.m_mats_dzdy[index];
        float             globaldy     = cache.m_mats_globaldy[index];
        float             uFromChannel = cache.m_mats_uBegin[index] +
                             ( 2 * id.channel() + 1 + clus.fractionBit() ) * cache.m_mats_halfChannelPitch[index];
        if ( id.die() ) uFromChannel += cache.m_mats_dieGap[index];
        uFromChannel += id.sipm() * cache.m_mats_sipmPitch[index];

        auto  endPoint = cache.m_mats_mirrorPoint[index] + cache.m_mats_ddx[index] * uFromChannel;
        float yMin     = endPoint.y();
        float x0       = endPoint.x() - dxdy * yMin;
        float z0       = endPoint.z() - dzdy * yMin;
        float yMax     = yMin + globaldy;
        if ( id.isBottom() ) std::swap( yMin, yMax );

        assert( clus.pseudoSize() < 9 && "Pseudosize of cluster is > 8. Out of range." );
        float werrX = m_invClusResolution[clus.pseudoSize()];

        hitvec.emplace_back( x0 );
        z0vec.emplace_back( z0 );
        yMinvec.emplace_back( yMin );
        yMaxvec.emplace_back( yMax );
        planeCodevec.emplace_back( ( info & 63u ) >> 1 );
        IDvec.emplace_back( LHCb::LHCbID( id ).lhcbID() );
        werrvec.emplace_back( werrX * werrX );
        dzDyvec.emplace_back( dzdy );
        dxDyvec.emplace_back( dxdy );
      }
    }

    hitvec.emplace_back( 1.e9f );
    z0vec.emplace_back( 1.e9f );
    yMinvec.emplace_back( 1.e9f );
    yMaxvec.emplace_back( 1.e9f );
    planeCodevec.emplace_back( std::numeric_limits<int>::max() );
    IDvec.emplace_back( 0 );
    werrvec.emplace_back( 1.e9f );
    dzDyvec.emplace_back( 0.f ); // this should protect max in multiplication
    dxDyvec.emplace_back( 0.f );
  }
  /**padding, last element on the down side is the first on the up side
   * make sure that there is enough padded at the end, i.e. Vc::Size
   */
  zoneIndexes[PrFTInfo::NFTZones]     = zoneIndexes[xu[0]];
  zoneIndexes[PrFTInfo::NFTZones + 1] = hitvec.size();

  const Vc::float_v one_f_large = Vc::float_v::One() * 1.e9f;
  const Vc::float_v zero_f      = Vc::float_v::Zero();
  one_f_large.store( &*hitvec.end() );
  one_f_large.store( &*z0vec.end() );
  one_f_large.store( &*yMinvec.end() );
  one_f_large.store( &*yMaxvec.end() );
  one_f_large.store( &*werrvec.end() );
  ( Vc::int_v::One() * std::numeric_limits<int>::max() ).store( &*planeCodevec.end() );
  ( Vc::uint_v::Zero() ).store( &*IDvec.end() );
  zero_f.store( &*dzDyvec.end() );
  zero_f.store( &*dxDyvec.end() );

  return tmp;
}
