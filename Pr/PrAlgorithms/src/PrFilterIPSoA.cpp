/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex_v2.h"
#include "Kernel/AllocatorUtils.h"

namespace {
  using Tracks   = LHCb::Pr::Velo::Tracks;
  using Vertices = LHCb::Event::v2::RecVertices;
} // namespace

/**
 * Vectorized IPFilter for TracksVP
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */
struct FilterIP : public Gaudi::Functional::Transformer<Tracks( Tracks const&, Vertices const& )> {
  FilterIP( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"Input", "Rec/Track/Velo"},
                      KeyValue{"InputVertices", LHCb::Event::v2::RecVertexLocation::Primary}},
                     KeyValue{"Output", ""} ) {}

  Tracks operator()( const Tracks& tracks, const Vertices& vertices ) const override {
    using dType = SIMDWrapper::avx2::types;
    using F     = dType::float_v;

    const F ip_cut_value = m_ipcut.value();

    auto tracks_out =
        LHCb::make_obj_propagating_allocator<LHCb::Pr::Velo::Tracks>( tracks, Zipping::generateZipIdentifier() );

    for ( int i = 0; i < tracks.size(); i += dType::size ) {
      auto loop_mask = dType::loop_mask( i, tracks.size() ); // true if i<tracks.size() else false

      Vec3<F> B = tracks.statePos<F>( i, 0 ); // get the origin and direction
      Vec3<F> u = tracks.stateDir<F>( i, 0 ); // of the state

      F min_d = 10e3;
      for ( int j = 0; j < (int)vertices.size(); j++ ) {
        auto    PV = vertices[j].position();
        Vec3<F> A  = Vec3<F>( PV.x(), PV.y(), PV.z() );
        auto    d  = ( B - A ).cross( u ).mag2();
        min_d      = min( min_d, d );
      }

      auto d    = sqrt( min_d ) / u.mag(); // distance from closest PV to line
      auto mask = ip_cut_value < d;        // ip cut

      tracks_out.copy_back<dType>( tracks, i, mask && loop_mask ); // conditional push_back to the output
    }

    m_nbTracksCounter += tracks_out.size();
    return tracks_out;
  };

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  Gaudi::Property<float>                        m_ipcut{this, "IPcut", 0.0};
};

DECLARE_COMPONENT_WITH_ID( FilterIP, "PrFilterIPSoA" )
