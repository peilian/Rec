/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/Transformer.h"
#include "SelKernel/TrackZips.h"

namespace {
  template <typename T>
  using Output = decltype( std::declval<T>().unwrap() );
  template <typename T>
  using Base = Gaudi::Functional::Transformer<Output<T>( T const& )>;
} // namespace

/** @class Unwrap Unwrap.cpp
 *  @brief Put a scalar iterable version of a LHCb::Pr::Zip iterable on the TES
 *
 *  This creates an "unwrapped scalar" iterable version of an LHCb::Pr::Zip
 *  iterable and puts it on the TES. Here "unwrapped" means that the proxy
 *  object returned during iteration has accessors returning plain arithmetic
 *  types instead of SIMDWrapper::scalar types, and "scalar" means that each
 *  proxy object represents a single track, not a vector unit worth of tracks.
 *  These settings can be overriden by the user using the .with<simd, unwrap>()
 *  method on the iterable track objects.
 */
template <typename T>
struct Unwrap final : public Base<T> {
  Unwrap( const std::string& name, ISvcLocator* pSvcLocator )
      : Base<T>( name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}
  Output<T> operator()( T const& in ) const override { return in.unwrap(); }
};

DECLARE_COMPONENT_WITH_ID( Unwrap<LHCb::Pr::Fitted::Forward::TracksWithPVs>, "Unwrap__PrFittedForwardTracksWithPVs" )

DECLARE_COMPONENT_WITH_ID( Unwrap<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                           "Unwrap__PrFittedForwardTracksWithMuonID" )