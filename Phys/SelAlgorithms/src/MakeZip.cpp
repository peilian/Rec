/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrIterableMuonPIDs.h"
#include "Event/PrZip.h"
#include "GaudiAlg/Transformer.h"
#include "SelKernel/IterableVertexRelations.h"

namespace {
  template <typename... Inputs>
  struct Helper {
    static constexpr std::size_t NumInputs{sizeof...( Inputs )};
    static auto                  prepare( Inputs const&... in ) { return LHCb::Pr::make_zip( in... ); }
    using Output   = typename std::invoke_result_t<decltype( prepare ), Inputs const&...>;
    using Base     = typename Gaudi::Functional::Transformer<Output( Inputs const&... )>;
    using KeyValue = typename Base::KeyValue;
    static auto InputNames() {
      std::array<KeyValue, NumInputs> ret;
      for ( auto i = 0ul; i < NumInputs; ++i ) { ret[i] = KeyValue{"Input" + std::to_string( i + 1 ), ""}; }
      return ret;
    }
  };
} // namespace

/** @class MakeZip MakeZip.cpp
 *  @brief Puts an iterable zip of its input containers onto the TES
 *
 *  This algorithm is a very thin wrapper around LHCb::Pr::make_zip( in... ),
 *  given a set of N inputs (property name Input{1..N}) it stores the return
 *  value of LHCb::Pr::make_zip( in... ) on the TES (property name Output).
 *
 *  @tparam  Inputs... Types of the input containers taken from the TES
 *  @returns           Iterable, non-owning view type referring to the given
 *                     input containers.
 */
template <typename... Inputs>
struct MakeZip final : public Helper<Inputs...>::Base {
  using Help = Helper<Inputs...>;
  MakeZip( const std::string& name, ISvcLocator* pSvcLocator )
      : Help::Base( name, pSvcLocator, Help::InputNames(), {"Output", ""} ) {}
  typename Help::Output operator()( Inputs const&... in ) const override { return Help::prepare( in... ); }
};

using MakeZip__PrFittedForwardTracks = MakeZip<LHCb::Pr::Fitted::Forward::Tracks>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks, "MakeZip__PrFittedForwardTracks" )

using MakeZip__PrFittedForwardTracks__BestVertexRelations =
    MakeZip<LHCb::Pr::Fitted::Forward::Tracks, BestVertexRelations>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks__BestVertexRelations,
                           "MakeZip__PrFittedForwardTracks__BestVertexRelations" )

using MakeZip__PrFittedForwardTracks__PrMuonPIDs = MakeZip<LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks__PrMuonPIDs, "MakeZip__PrFittedForwardTracks__PrMuonPIDs" )