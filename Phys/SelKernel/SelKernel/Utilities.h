/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/detected.h"
#include "TrackKernel/TrackVertexUtils.h"
#include <vector>

/** @file  Utilities.h
 *  @brief Helper types and functions for selections.
 */

namespace LHCb {
  class Particle;
}

namespace Sel::Utils {
  /** Define plain bool versions that mirror the functions defined for
   *  SIMDWrapper's mask_v types. These are useful when writing generic
   *  functor code that works both for scalar and vector types.
   */
  constexpr bool all( bool x ) { return x; }
  constexpr bool any( bool x ) { return x; }
  constexpr bool none( bool x ) { return !x; }
  constexpr int  popcount( bool x ) { return x; }
  template <typename T>
  constexpr T select( bool x, T a, T b ) {
    return x ? a : b;
  }

  /** Helper to determine if the given type has a bestPV() method. */
  template <typename T>
  using has_bestPV_ = decltype( std::declval<T>().bestPV() );

  template <typename T>
  inline constexpr bool has_bestPV_v = Gaudi::cpp17::is_detected_v<has_bestPV_, T>;

  /** Helper to determine if the given type has a closestToBeamState() method.
   */
  template <typename T>
  using has_closestToBeamState_ = decltype( std::declval<T>().closestToBeamState() );

  template <typename T>
  inline constexpr bool has_closestToBeamState_v = Gaudi::cpp17::is_detected_v<has_closestToBeamState_, T>;

  /** Helpers to determine if a given type is "state like". */
  template <typename T>
  using has_x_ = decltype( std::declval<T>().x() );
  template <typename T>
  using has_y_ = decltype( std::declval<T>().y() );
  template <typename T>
  using has_z_ = decltype( std::declval<T>().z() );
  template <typename T>
  using has_tx_ = decltype( std::declval<T>().tx() );
  template <typename T>
  using has_ty_ = decltype( std::declval<T>().ty() );
  template <typename T>
  using has_covariance_ = decltype( std::declval<T>().covariance() );
  template <typename T>
  inline constexpr bool has_x_v = Gaudi::cpp17::is_detected_v<has_x_, T>;
  template <typename T>
  inline constexpr bool has_y_v = Gaudi::cpp17::is_detected_v<has_y_, T>;
  template <typename T>
  inline constexpr bool has_z_v = Gaudi::cpp17::is_detected_v<has_z_, T>;
  template <typename T>
  inline constexpr bool has_tx_v = Gaudi::cpp17::is_detected_v<has_tx_, T>;
  template <typename T>
  inline constexpr bool has_ty_v = Gaudi::cpp17::is_detected_v<has_ty_, T>;
  template <typename T>
  inline constexpr bool has_covariance_v = Gaudi::cpp17::is_detected_v<has_covariance_, T>;
  template <typename T>
  inline constexpr bool is_state_like =
      has_x_v<T>&& has_y_v<T>&& has_z_v<T>&& has_tx_v<T>&& has_ty_v<T>&& has_covariance_v<T>;

  template <typename T>
  constexpr bool is_legacy_particle = std::is_same_v<LHCb::Particle, T>;

  auto get_track_from_particle = []( auto const& p ) {
    auto const* pp = p.proto();
    return pp ? pp->track() : nullptr;
  };

  auto get_track_property_from_particle = []( auto const& p, auto&& accessor, auto&& invalid ) {
    auto const* track = get_track_from_particle( p );
    return track ? accessor( track ) : invalid;
  };

  /** Helpers for dispatching to the right fdchi2 calculation. */
  template <typename Vertex1, typename Vertex2>
  auto flightDistanceChi2( Vertex1 const& v1, Vertex2 const& v2 ) {
    // Lifted from LoKi::DistanceCalculator
    Gaudi::SymMatrix3x3 cov{v1.posCovMatrix() + v2.posCovMatrix()};
    if ( !cov.Invert() ) { return std::numeric_limits<double>::max(); }
    auto const&    p1 = v1.position();
    auto const&    p2 = v2.position();
    Gaudi::Vector3 delta{p1.X() - p2.X(), p1.Y() - p2.Y(), p1.Z() - p2.Z()};
    return ROOT::Math::Similarity( delta, cov );
  }

  /** @fn    impactParameterChi2
   *  @brief Helper for dispatching to the correct ipchi2 calculation.
   *
   * The input be particle-like, with an (x, y, z) position and 3/4-momentum, or
   * it could be state-like, with a (x, y, tx, ty[, q/p]) vector and covariance
   * matrix.
   *
   * LHCb::TrackVertexUtils::vertexChi2() has the ipchi2 calculation for a
   * state and [primary] vertex position/covariance.
   *
   * @todo Add a [template] version of LHCb::TrackVertexUtils::vertexChi2()
   *       that takes a particle-like? It only uses the 4x4 upper corner of
   *       the state-like covariance matrix, so we might be able to save
   *       something?
   */
  template <typename TrackOrParticle, typename Vertex>
  auto impactParameterChi2( TrackOrParticle const& obj, Vertex const& vertex ) {
    if constexpr ( has_closestToBeamState_v<TrackOrParticle> ) {
      return LHCb::TrackVertexUtils::vertexChi2( obj.closestToBeamState(), vertex.position(), vertex.covMatrix() );
    } else if constexpr ( is_state_like<TrackOrParticle> ) {
      return LHCb::TrackVertexUtils::vertexChi2( obj, vertex.position(), vertex.covMatrix() );
    } else {
      Gaudi::Matrix6x6 cov6tmp;
      cov6tmp.Place_at( obj.posCovMatrix(), 0, 0 );         // 3x3 position
      cov6tmp.Place_at( obj.threeMomCovMatrix(), 3, 3 );    // 3x3 momentum
      cov6tmp.Place_at( obj.posThreeMomCovMatrix(), 3, 0 ); // 3x3 position x momentum
      Gaudi::SymMatrix6x6 cov6{cov6tmp.LowerBlock()};
      /** @todo add an implementation for ipchi2-of-composite that just returns
       *        it without calculating the decay length [error] too. OL tried
       *        this in https://gitlab.cern.ch/snippets/894, but without a
       *        better study of which implementation copes better when the
       *        matrix inversion fails (which it inevitably does sometimes)
       *        lets not switch to using it.
       */
      double chi2{std::numeric_limits<double>::max()}, decaylength{std::numeric_limits<double>::max()},
          decaylength_err{std::numeric_limits<double>::max()};
      if ( LHCb::TrackVertexUtils::computeChiSquare( obj.referencePoint(), obj.threeMomentum(), cov6, vertex.position(),
                                                     vertex.covMatrix(), chi2, decaylength,
                                                     decaylength_err ) != LHCb::TrackVertexUtils::Success ) {
        chi2 = decaylength = decaylength_err = std::numeric_limits<double>::max();
      }
      return chi2;
    }
  }
} // namespace Sel::Utils
