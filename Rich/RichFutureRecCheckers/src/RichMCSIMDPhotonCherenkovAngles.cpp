/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

namespace Rich::Future::Rec::MC::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final : public Consumer<void( const Summary::Track::Vector&,                   //
                                                                const LHCb::Track::Selection&,                   //
                                                                const SIMDPixelSummaries&,                       //
                                                                const Rich::PDPixelCluster::Vector&,             //
                                                                const Relations::PhotonToParents::Vector&,       //
                                                                const LHCb::RichTrackSegment::Vector&,           //
                                                                const CherenkovAngles::Vector&,                  //
                                                                const SIMDCherenkovPhoton::Vector&,              //
                                                                const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                                const LHCb::MCRichDigitSummarys& ),
                                                          Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Selection&                   tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    // properties

    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.030f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.060f, 0.036f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.005f, 0.0025f}};

    /// The minimum track momentum (MeV/c)
    Gaudi::Property<float> m_minP{this, "MinP", float( 2 * Gaudi::Units::GeV ), "Minimum track momentum"};

    /// The maximum track momentum (MeV/c)
    Gaudi::Property<float> m_maxP{this, "MaxP", float( 100 * Gaudi::Units::GeV ), "Maximum track momentum"};

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{this, "SkipElectrons", false, "Skip eletrons from monitoring"};

  private:
    // cached data

    RadiatorArray<AIDA::IHistogram1D*> h_ckResAll      = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTrue     = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFake     = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_thetaRecTrue  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_phiRecTrue    = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_thetaRecFake  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_phiRecFake    = {{}};
    RadiatorArray<AIDA::IProfile1D*>   h_ckResTrueVP   = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResAllPion  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTruePion = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFakePion = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : Rich::radiators() ) {
    if ( m_rads[rad] ) {

      // Plots for MC true type
      ok &= saveAndCheck( h_ckResAll[rad], richHisto1D( HID( "ckResAll", rad ), "Rec-Exp Cktheta - True Type",
                                                        -m_ckResRange[rad], m_ckResRange[rad], nBins1D() ) );
      ok &= saveAndCheck( h_ckResTrue[rad], //
                          richHisto1D( HID( "ckResTrue", rad ), "Rec-Exp Cktheta - MC true photons - True Type",
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D() ) );
      ok &= saveAndCheck( h_ckResFake[rad], //
                          richHisto1D( HID( "ckResFake", rad ), "Rec-Exp Cktheta - MC fake photons - True Type",
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D() ) );
      ok &= saveAndCheck( h_thetaRecTrue[rad], //
                          richHisto1D( HID( "thetaRecTrue", rad ), "Reconstructed Ch Theta - MC true photons",
                                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), "Cherenkov Theta / rad" ) );
      ok &= saveAndCheck( h_phiRecTrue[rad], //
                          richHisto1D( HID( "phiRecTrue", rad ), "Reconstructed Ch Phi - MC true photons", 0.0,
                                       2.0 * Gaudi::Units::pi, nBins1D(), "Cherenkov Phi / rad" ) );
      ok &= saveAndCheck( h_thetaRecFake[rad], //
                          richHisto1D( HID( "thetaRecFake", rad ), "Reconstructed Ch Theta - MC fake photons",
                                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), "Cherenkov Theta / rad" ) );
      ok &= saveAndCheck( h_phiRecFake[rad], //
                          richHisto1D( HID( "phiRecFake", rad ), "Reconstructed Ch Phi - MC fake photons", 0.0,
                                       2.0 * Gaudi::Units::pi, nBins1D(), "Cherenkov Phi / rad" ) );
      ok &= saveAndCheck( h_ckResTrueVP[rad], //
                          richProfile1D( HID( "ckResTrueVP", rad ),
                                         "<Rec-Exp Cktheta> V P - MC true photons - True Type", m_minP, m_maxP,
                                         nBins1D(), "Track Momentum (MeV/c)", "<Rec-Exp Cktheta> / rad" ) );

      // Plots assuming pion hypo
      ok &= saveAndCheck( h_ckResAllPion[rad], //
                          richHisto1D( HID( "ckResAllPion", rad ), "Rec-Exp Cktheta - Pion Type", -m_ckResRange[rad],
                                       m_ckResRange[rad], nBins1D() ) );
      ok &= saveAndCheck( h_ckResTruePion[rad], //
                          richHisto1D( HID( "ckResTruePion", rad ), "Rec-Exp Cktheta - MC true photons - Pion Type",
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D() ) );
      ok &= saveAndCheck( h_ckResFakePion[rad], //
                          richHisto1D( HID( "ckResFakePion", rad ), "Rec-Exp Cktheta - MC fake photons - Pion Type",
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D() ) );
    }
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                            const LHCb::Track::Selection&                   tracks,        //
                                            const SIMDPixelSummaries&                       pixels,        //
                                            const Rich::PDPixelCluster::Vector&             clusters,      //
                                            const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&           segments,      //
                                            const CherenkovAngles::Vector&                  expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&              photons,       //
                                            const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                            const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) continue;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // reconstructed phi
        const auto phiRec = phot.CherenkovPhi()[i];

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, assume Pion (as in real data)
          if ( Rich::Unknown == pid ) { pid = Rich::Pion; }
          // skip electrons which are reconstructed badly..
          if ( UNLIKELY( m_skipElectrons && Rich::Electron == pid ) ) { continue; }

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          if ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] ) {

            // true Cherenkov signal ?
            const bool trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // expected CK theta ( for true type )
            const auto thetaExp = expCKangles[pid];

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // fill some plots
            h_ckResAll[rad]->fill( deltaTheta, mcPW );
            if ( !trueCKSig ) {
              h_ckResFake[rad]->fill( deltaTheta, mcPW );
              h_thetaRecFake[rad]->fill( thetaRec, mcPW );
              h_phiRecFake[rad]->fill( phiRec, mcPW );
            } else {
              h_ckResTrue[rad]->fill( deltaTheta, mcPW );
              h_thetaRecTrue[rad]->fill( thetaRec, mcPW );
              h_phiRecTrue[rad]->fill( phiRec, mcPW );
              h_ckResTrueVP[rad]->fill( pTot, deltaTheta, mcPW );
            }
          }

        } // loop over associated MCPs

        // Now plots when assuming all tracks are pions ( as in real data )
        const auto pionbeta = richPartProps()->beta( pTot, Rich::Pion );
        if ( pionbeta >= m_minBeta[rad] && pionbeta <= m_maxBeta[rad] ) {

          // expected CK theta ( for Pion )
          const auto thetaExp = expCKangles[Rich::Pion];

          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // fill some plots
          h_ckResAllPion[rad]->fill( deltaTheta );
          if ( trueCKMCPs.empty() ) {
            h_ckResFakePion[rad]->fill( deltaTheta );
          } else {
            h_ckResTruePion[rad]->fill( deltaTheta );
          }
        }

      } // scalar loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
