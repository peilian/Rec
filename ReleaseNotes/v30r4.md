

2019-05-22 Rec v30r4
===

This version uses Lbcom v30r4, LHCb v50r4, Gaudi v32r0 and LCG_95 with ROOT 6.16.00
<p>
This version is released on `master` branch.

Built relative to Rec v30r3, with the following changes:

### New features

- Introduce new algorithm SciFiTrackForwarding, !1514 (@chasse)   

- Added list of unused tracks as a PrSelection to output of BeamLineVertexFinder, !1512 (@wouter)   
  Can then be used   
  - as input to the forward tracking
  - as input to PatPV3D (or another PV finder) with looser settings for reconstructing beam gas events

- Make PrAddUTHitsTool's UT location configurable, !1446, !1449 (@nnolte)   

- Add service to dump VP geometry for use in Allen, !1497 (@raaij)   
  The same information is needed as is used to obtain conditions in `PrPixel`. To avoid code duplication, two files have been moved from `Pr/PrPixel` to public headers and the linker library in `Det/VPDet` in lhcb/LHCb!1884.  
    
- Add service to dump muon geometry info and fix the position lookup table, !1491, !1495 (@raaij)   

- GPU dumpers: dump muon geom, !1471 (@raaij)   
  
- TrackerDumper: add option to not write one ROOT file per event, !1520 (@dovombru)   

- Dump a lookup table of the positions of pads and strips in the muon system, !1445 (@raaij)   

- Apply source code style convention, !1373 (@clemenci)   
  - add CI check on code format  
  - global reformat


### Enhancements

- Updates to MuonMatch scripts and MuonMatchVeloUT algorithm for the TrMuonMatch tracking sequence, !1515 (@mramospe)   
  - MuonMatchVeloUT algorithm now makes use of the new MuonRawToHits algorithm.  
  - TrMuonMatch sequence in RecoUpgradeTracking runs MuonRawToHits instead of MuonRawToCoord + MuonCoordsToHits.  
  - Scripts to create n-tuples and study the performance of the algorithm have been changed, including MC-match information about the muon hits.  
  - Added new converter fromV2SelectionV1TrackVector needed to measure the performance using MC-matching information.

- Change TrMINIPCHI2 functors to accept v2 vertices, !1483 (@apearce)   

- Refactored MC checking code and made it fully functional, !1482 (@sponce)   

- Update tracker dumper, !1475 (@dovombru)   

- Add region to muon common hit dump, !1474 (@popovs)   
  
- Update of IsPhoton (gamma/pi0 separation) tool for Run 3., !1473, !1485 (@calvom)   

- Simplifications and fixes to the MuonMatchVeloUT algorithm, !1468 (@mramospe)   
  1. Extrapolation to the muon chambers has been simplified.  
  2. How we define the search windows has changed.  
  3. Remove negligible corrections to the magnet focal plane parametrization.  
    
  Performance of the algorithm practically didn't change.  

- VeloClusterTracking using avx512, !1456 (@ahennequ)   
  The Cluster and Tracking algorithm are merged for simplifying the data passing between the two.  
  All AVX512 functions have a scalar fallback. 

- TrackSys: make PrVeloUT use its default FinalFit setting, !1428 (@jihu)   
  Removes the coupling of the 2 options `PrForwardTracking.useMomentumGuidedSearchWindow` and `PrVeloUT.FinalFit`  

- PrForwardTracking: Initialize ModPrHit.coord to PrHit::x() value, avoid reading later, !1426 (@jihu)   

- ParamKalman additions, !1420 (@sstemmle)   
  Adds a few more states to the output track of the ```ParameterizedKalmanFit``` and updates some Kalman auxiliary algorithms that are needed for tuning and performance tests. In addition, the TrackResChecker is modified to also print the mean prob(chi2,ndof) of the tracks. 

- Fix MC dumper after !1406, !1416 (@dovombru)   
  
- First version of modified PrVeloUT, !1415 (@decianm)   
  - New "final fit" method with improved uncertainties for the magnet point, and a chi2 calculation, yield a much more accurate chi2 and a better momentum estimate.  
  - Fiducial cuts that reject tracks at the edge of the acceptance  
  - A cut on the final momentum and pT  
  - A linear discriminant, using the new chi2, to fight ghosts  

- Reduce the amount of usage of LHCb::Track when not necessary in MuonID and Hlt1Muon lines, !1413 (@rvazquez)   
  

- PrForwardtracking: simplify some calculations, remove double naming of variables in- and outside loop, !1412 (@decianm)   

- Used integers to count tracks in PrTrackAssociator, !1411 (@sponce)   

- add faster tools to get coords and hits from muon raw, !1398 (@rvazquez)   

- Calo track tool upgrade for future, !1298 (@wkrzemie)   


### Bug fixes

- Add missing delete in TMV_MLP_E and TMV_MLP_H, !1543 (@jmarchan)

- Require explicitly that the Velo state used in PrMatchNN is the one from EndVelo, !1529 (@sstahl)   

- Fixed creation of LinksByKey in PrTrackAssociator by setting source LinkId properly, !1528 (@sponce)   

- Set tracktype properly in parameterized Kalman, !1509 (@chasse)   

- RICH - Fix an issue that causes a zipping size mis-match exception when the track GEC fires., !1466 (@jonrob)  
  Also, increase the default GEC cuts to those used for lead processing.

- Change PrForwardTracking so that when it is run with OutputLevel set to DEBUG, it doesn't generate floating point exceptions, !1455 (@graven)


- PrForwardTracking: fix particle bending direction bug in the case of MagUp, !1436 (@jihu)   

- Fix wrong hit uncertainty in the FT, !1419 (@sstemmle)   
  NoCutWithFit throughput goes from ~8700 Hz to ~8200 Hz as a result of this fix.

- Tracking - AVX512 compilation fixes, !1405 (@jonrob)   


### Code modernisations and cleanups

- New CaloFutureDigitsFilterAlg, !1539 (@jmarchan)

- Update test reference to follow gaudi/Gaudi!904, !1537 (@cattanem)   
  
- Port CaloFutureClusterCovarianceAlg to new framework, !1524 (@ppais)   
  - Moved CaloFutureClusterCovarianceAlg to `Transformer`  
  - Updated counters for the algorithm and FutureClusterCovarianceMatrixTool   
  - Modified CaloFutureShowerOverlap to write to a new output cluster location 'EcalOverlap'

- Move local stand-alone utility function in anonymous namespace, !1521 (@graven)   
  this should avoid linking problems with other implementations of functions with the same name

- Prefer ADL lookup of updateHandleLocation over explicit Gaudi::Functional::updateHandleLocation, !1516 (@graven)   
  Fix deprecation warnings introduced by gaudi/Gaudi!897

- const correctness changes, !1513 (@graven)   

- Remove SPD/PRS and IIncidentListener from CaloFutureCorrections, !1505 (@dgolubko)   

- Remove unused local Variable in PrStoreFTHit.cpp, !1500 (@chasse)   

- Cleanup unused VectorClass statements, !1499 (@sponce)   

- Remove unused variables flagged by clang8, !1496 (@cattanem)   

- Remove Spd and Prs from 'list of cluster selector tools' in CaloFutureElectronAlg.h, !1487 (@aszabels)   

- tool handles in masterextrapolator + paramkalman, !1486 (@nnolte)   

- Streamline PrFTHitHandler, !1484 (@graven)   
  * move static functions to PrHybridSeeding, the only user of them, and remove the unused ones  
  * add getRange, get_until  

- Simplify zone-to-layer code in PrClustersResidual, !1476, !1492 (@casanche)   

- Follow changes in lhcb/LHCb!1770, !1467 (@graven)   
  
- Remove obsolete CVS keywords, !1462 (@cattanem)   
  
- Modernize PrForwardTracking, !1447, !1489 (@graven)   
  * prefer stand-alone functions over member functions  
  * prefer STL algorithms over explicit loops  
  * use auto  
  * reduce the amount of copying

- Remove spurious CMT file, !1434 (@cattanem)   
  
- PrHit: Fix double promotion, !1427 (@jihu)   

- PrForwardTracking: reducing float to double promotion, !1424 (@jihu)   

- PrForwardTracking: Initialize Vc::float_v entries to zero, !1418 (@jihu)   

- Converted PrTrackAssociator to functional, !1406 (@sponce)   

- Modified Pr/PrPixel to use ConditionAccessor and derived conditions, !1402 (@clemenci)   
  This is mostly a proof of concept, waiting for proper integration of ConditionAccessorHolder concept in `Gaudi::Functional`.  

- Change vectorclass.h include path to follow LHCb!1697, !1368 (@cattanem)   
  


### Monitoring changes

- Clean up of names in MC checking, !1495 (@sstahl)   
  Renamed PrChecker to PrCheckerAlgorithm.  
  Renamed typedef PrCheckerCounter2 to PrTrackChecker.  
  Renamed PrCounter2 to PrTrackCounter.  
  Renamed PrCheckerTTCounter to PrUTHitChecker.  
  Renamed PrTTCounter to PrUTCounter.  
  Renamed PrLHCbID2MCParticleVPUTFT to PrLHCbID2MCParticle.  
  Renamed property SelectId to HitTypesToCheck.  
  Removed references to TT, IT, OT, Velo.

- PrCounter2: Add the nhits distribution of ghost tracks, !1469 (@mengzhen)   

- PrChecker functional, !1465 (@sponce)   
  Major refactoring of the PrChecker to make it fully functional and thread safe.  

- Updated linking for track IP resolution checker., !1501 (@ldufour)   

- Calo future electron alg new counters, !1479 (@aszabels)   

- Update a test reference to follow gaudi/Gaudi!830, !1464 (@cattanem)   

- added #PVs counter in TBLVF, !1414 (@nnolte)   
