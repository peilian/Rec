/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "GaudiAlg/GaudiTool.h"
// Math
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

// using namespace LHCb;

#include "TMV_MLP_E.C"
#include "TMV_MLP_H.C"

/** @class neutralIDTool neutralIDTool.h
 *
 *
 *  @author Mostafa HOBALLAH
 *  @date   2013-07-25
 */
class FutureNeutralIDTool final : public extends<GaudiTool, LHCb::Calo::Interfaces::INeutralID> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  std::optional<double> isNotE( const LHCb::CaloHypo&                                  hypo,
                                const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const override;
  std::optional<double> isNotH( const LHCb::CaloHypo&                                  hypo,
                                const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const override;

  double isNotE( const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const override;
  double isNotH( const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const override;

private:
  Gaudi::Property<float> m_minPt{this, "MinPt", 75.};

  std::unique_ptr<ReadMLPE> m_reader0;
  std::unique_ptr<ReadMLPH> m_reader1;
};
