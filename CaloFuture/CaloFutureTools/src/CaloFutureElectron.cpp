/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/TrackDefaultParticles.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class CaloFutureElectron CaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class CaloFutureElectron final : public extends<GaudiTool, LHCb::Calo::Interfaces::IElectron> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  LHCb::Calo::Interfaces::hypoPairStruct getElectronBrem( LHCb::ProtoParticle const& proto ) const override;

  LHCb::State           caloState( LHCb::ProtoParticle const& proto ) const override;
  LHCb::State           closestState( LHCb::ProtoParticle const& proto ) const override;
  LHCb::CaloHypo const* electron( LHCb::ProtoParticle const& proto ) const override {
    return getElectronBrem( proto ).electronHypo;
  }
  LHCb::CaloHypo const* bremstrahlung( LHCb::ProtoParticle const& proto ) const override {
    return getElectronBrem( proto ).bremHypo;
  }
  LHCb::CaloMomentum bremCaloFutureMomentum( LHCb::ProtoParticle const& proto ) const override;
  double             eOverP( LHCb::ProtoParticle const& proto ) const override;
  double             caloTrajectoryL( LHCb::ProtoParticle const& proto, CaloPlane::Plane plane ) const override;

private:
  LHCb::State caloState( LHCb::ProtoParticle const& proto, CaloPlane::Plane plane, double delta = 0 ) const;

  // configuration-dependent state
  ITrackExtrapolator*          m_extrapolator = nullptr;
  Gaudi::Property<std::string> m_extrapolatorType{this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};
  Gaudi::Property<float>       m_tolerance{this, "Tolerance", 0.01};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureElectron )

namespace {
  // fix name clash with Track2Calo::closestState
  decltype( auto ) closest_state( const LHCb::Track& t, const Gaudi::Plane3D& p ) { return closestState( t, p ); }
} // namespace

//=============================================================================
StatusCode CaloFutureElectron::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );
  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorType, "Extrapolator", this );
  return StatusCode::SUCCESS;
}
//=============================================================================
LHCb::Calo::Interfaces::hypoPairStruct CaloFutureElectron::getElectronBrem( LHCb::ProtoParticle const& proto ) const {

  LHCb::Calo::Interfaces::hypoPairStruct hypoPair;

  if ( !proto.track() ) return {nullptr, nullptr};

  for ( const LHCb::CaloHypo* hypo : proto.calo() ) {
    if ( !hypo ) continue;
    switch ( hypo->hypothesis() ) {
    case LHCb::CaloHypo::Hypothesis::EmCharged:
      hypoPair.electronHypo = hypo;
      break;
    case LHCb::CaloHypo::Hypothesis::Photon:
      hypoPair.bremHypo = hypo;
      break;
    default:; // nothing;
    }
  }
  if ( !( hypoPair && hypoPair.electronHypo->position() ) ) { // Electron hypo is mandatory - brem. not
    return {nullptr, nullptr};
  }

  return hypoPair;
}
//=============================================================================
LHCb::State CaloFutureElectron::caloState( LHCb::ProtoParticle const& proto ) const {
  CaloPlane::Plane plane    = CaloPlane::ShowerMax;
  double           delta    = 0.;
  auto             hypoPair = getElectronBrem( proto );
  if ( hypoPair )
    return caloState( proto, plane, delta );
  else
    return {};
}

//=============================================================================
double CaloFutureElectron::eOverP( LHCb::ProtoParticle const& proto ) const {
  auto hypoPair = getElectronBrem( proto );
  return hypoPair ? hypoPair.electronHypo->e() / proto.track()->p() : 0.;
}
LHCb::CaloMomentum CaloFutureElectron::bremCaloFutureMomentum( LHCb::ProtoParticle const& proto ) const {

  auto hypoPair = getElectronBrem( proto );

  if ( !hypoPair || hypoPair.bremHypo == nullptr ) return {};

  Gaudi::XYZPoint     point;
  Gaudi::SymMatrix3x3 matrix;
  proto.track()->position( point, matrix );
  return {hypoPair.bremHypo, point, matrix};
}

//=============================================================================
LHCb::State CaloFutureElectron::closestState( LHCb::ProtoParticle const& proto ) const {

  auto hypoPair = getElectronBrem( proto );

  if ( !hypoPair ) return {};
  // get state on Front of Ecal
  LHCb::State calostate = caloState( proto, CaloPlane::Front );
  if ( calostate.z() == 0 ) return {};

  // get frontPlane
  auto                detCalo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  ROOT::Math::Plane3D frontPlane = detCalo->plane( CaloPlane::Front );

  // get hypo position
  const auto& params = hypoPair.electronHypo->position()->parameters();
  double      x      = params( LHCb::CaloPosition::Index::X );
  double      y      = params( LHCb::CaloPosition::Index::Y );

  // Define calo line (from transversal barycenter) and track line in Ecal
  Gaudi::XYZVector  normal = frontPlane.Normal();
  double            zEcal  = ( -normal.X() * x - normal.Y() * y - frontPlane.HesseDistance() ) / normal.Z(); // tilt
  Gaudi::XYZPoint   point{x, y, zEcal};
  Gaudi::Math::Line cLine{point, frontPlane.Normal()};
  Gaudi::Math::Line tLine{calostate.position(), calostate.slopes()};

  // Find points of closest distance between calo Line and track Line
  Gaudi::XYZPoint cP, tP;
  Gaudi::Math::closestPoints( cLine, tLine, cP, tP );

  // default to electron
  auto pid = LHCb::Tr::PID::Electron();
  // propagate the state the new Z of closest distance
  StatusCode sc = m_extrapolator->propagate( calostate, tP.Z(), pid );

  if ( sc.isFailure() ) return {};
  return calostate;
}
//=============================================================================
double CaloFutureElectron::caloTrajectoryL( LHCb::ProtoParticle const& proto, CaloPlane::Plane refPlane ) const {
  LHCb::State      theState = closestState( proto );
  LHCb::State      refState = caloState( proto, refPlane );
  Gaudi::XYZVector depth    = theState.position() - refState.position();

  auto                detCalo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  ROOT::Math::Plane3D plane   = detCalo->plane( refPlane );
  double              dist    = plane.Distance( theState.position() ); // signed distance to refPlane
  return depth.R() * dist / fabs( dist );
}

//=============================================================================
LHCb::State CaloFutureElectron::caloState( LHCb::ProtoParticle const& proto, CaloPlane::Plane plane,
                                           double delta ) const {
  if ( !getElectronBrem( proto ) ) return {};

  // get caloPlane
  auto                detCalo  = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  ROOT::Math::Plane3D refPlane = detCalo->plane( plane );
  // propagate state to refPlane
  const LHCb::Tr::PID pid       = LHCb::Tr::PID::Pion();
  LHCb::State         calostate = closest_state( *proto.track(), refPlane );
  StatusCode          sc        = m_extrapolator->propagate( calostate, refPlane, m_tolerance, pid );
  if ( sc.isFailure() ) return {};

  if ( 0. == delta ) return calostate;

  Gaudi::XYZVector dir( calostate.tx(), calostate.ty(), 1. );
  Gaudi::XYZPoint  point = calostate.position() + delta * dir / dir.R();
  // extrapolate to the new point
  sc = m_extrapolator->propagate( calostate, point.z(), pid );
  if ( sc.isFailure() ) return {};
  return calostate;
}
//=============================================================================
