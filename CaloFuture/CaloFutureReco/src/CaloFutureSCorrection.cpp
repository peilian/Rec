/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloFutureSCorrection.h"
#include "Event/CaloHypo.h"
#include "GaudiKernel/SystemOfUnits.h"

/** @file
 *  Implementation file for class : CaloFutureSCorrection
 *
 *  @date 2003-03-10
 *  @author Xxxx XXXXX xxx@xxx.com
 *
 *  Adam Szabelski
 *  date 2019-10-15
 *
 */

DECLARE_COMPONENT( CaloFutureSCorrection )

CaloFutureSCorrection::CaloFutureSCorrection( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : extends( type, name, parent ) {

  // define conditionName
  const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
  if ( uName.find( "ELECTRON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/ElectronSCorrection";
  } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/SplitPhotonSCorrection";
  } else if ( uName.find( "PHOTON" ) ) {
    m_conditionName = "Conditions/Reco/Calo/PhotonSCorrection";
  }
}
// ============================================================================

StatusCode CaloFutureSCorrection::finalize() {
  m_hypos.clear();
  return CaloFutureCorrectionBase::finalize();
}
// ============================================================================

StatusCode CaloFutureSCorrection::initialize() {
  StatusCode sc = CaloFutureCorrectionBase::initialize();
  if ( sc.isFailure() ) { return Error( "Unable initialize the base class CaloFutureCorrectionBase!", sc ); }
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Condition name : " << m_conditionName << endmsg;

  return StatusCode::SUCCESS;
}

// ============================================================================
StatusCode CaloFutureSCorrection::process( LHCb::span<LHCb::CaloHypo* const> hypos ) const {
  for ( auto* hypo : hypos ) {

    auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo->hypothesis() );
    if ( m_hypos.end() == h ) return Error( "Invalid hypothesis!", StatusCode::SUCCESS );

    if ( hypo->e() < 0. ) {
      if ( m_counterFlag.value() ) ++m_counterSkipNegativeEnergyCorrection;
      continue;
    }

    // get cluster  (special case for SplitPhotons)
    const LHCb::CaloCluster* MainCluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, true );
    if ( !MainCluster ) {
      Warning( "CaloCLuster* points to NULL -> no correction applied", StatusCode::SUCCESS ).ignore();
      continue;
    }

    const LHCb::CaloCluster::Entries&          entries = MainCluster->entries();
    LHCb::CaloCluster::Entries::const_iterator iseed =
        LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::SeedCell );
    if ( entries.end() == iseed ) {
      Warning( "The seed cell is not found -> no correction applied", StatusCode::SUCCESS ).ignore();
      continue;
    }

    const LHCb::CaloDigit* seed = iseed->digit();
    if ( !seed ) {
      Warning( "Seed digit points to NULL -> no correction applied", StatusCode::SUCCESS ).ignore();
      continue;
    }

    const LHCb::CaloPosition& position = MainCluster->position();
    const double              xBar     = position.x();
    const double              yBar     = position.y();
    const double              z        = position.z();
    const LHCb::CaloCellID&   cellID   = seed->cellID();
    auto                      seedPos  = m_det->cellCenter( cellID );
    SCorrInputParams          params{cellID, seedPos, xBar, yBar, z};

    /** here all information is available
     *
     *  ( ) Ecal energy in 3x3     :   ( not used )
     *  ( ) Prs and Spd energies   :   ( not available )
     *  (3) weighted barycenter    :    xBar, yBar
     *  ( ) Zone/Area in Ecal      :    area        ( not used )
     *  (5) SEED digit             :    seed   (NOT FOR SPLITPHOTONS !!)
     *  (6) CellID of seed digit   :    cellID
     *  (7) Position of seed cell  :    seedPos
     */

    struct SCorrResults results = calculateSCorrections( params );
    double              xCor    = results.xCor;
    double              yCor    = results.yCor;

    double dXhy_dXcl = results.dXhy_dXcl;
    double dYhy_dYcl = results.dYhy_dYcl;

    // protection against unphysical d(Xhypo)/d(Xcluster) == 0 or d(Yhypo)/d(Ycluster) == 0
    if ( fabs( dXhy_dXcl ) < 1e-10 ) {
      warning() << "unphysical d(Xhypo)/d(Xcluster) = " << dXhy_dXcl << " reset to 1 as if Xhypo = Xcluster" << endmsg;
      dXhy_dXcl = 1.;
    }
    if ( fabs( dYhy_dYcl ) < 1e-10 ) {
      warning() << "unphysical d(Yhypo)/d(Ycluster) = " << dYhy_dYcl << " reset to 1 as if Yhypo = Ycluster" << endmsg;
      dYhy_dYcl = 1.;
    }

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) && m_correctCovariance ) { debugDerivativesCalculation( params, results ); }

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { printDebugInfo( hypo, params, xBar, yBar, xCor, yCor ); }

    updatePosition( xCor, yCor, hypo );
    if ( m_counterFlag.value() ) {
      m_counterDeltaX += xCor - xBar;
      m_counterDeltaY += yCor - yBar;
    }

    if ( m_correctCovariance ) { updateCovariance( dXhy_dXcl, dYhy_dYcl, hypo ); }
  }
  return StatusCode::SUCCESS;
}

void CaloFutureSCorrection::printDebugInfo( const LHCb::CaloHypo* hypo, const struct SCorrInputParams& params,
                                            double xBar, double yBar, double xCor, double yCor ) const {
  debug() << "Calo Hypothesis :" << hypo->hypothesis() << endmsg;
  debug() << "cellID          : " << params.cellID << endmsg;
  debug() << "Hypo E :  " << hypo->position()->e() << " " << params.cellID << endmsg;
  debug() << "xBar/yBar " << xBar << "/" << yBar << endmsg;
  debug() << "xg/yg  " << hypo->position()->x() << "/" << hypo->position()->y() << endmsg;
  debug() << "xNew/yNew " << xCor << "/" << yCor << endmsg;
  debug() << "xcel/ycel " << params.seedPos.x() << "/" << params.seedPos.y() << endmsg;
}

void CaloFutureSCorrection::updatePosition( double xCor, double yCor, LHCb::CaloHypo* hypo ) const {
  LHCb::CaloPosition::Parameters& parameters = hypo->position()->parameters();
  parameters( LHCb::CaloPosition::Index::X ) = xCor;
  parameters( LHCb::CaloPosition::Index::Y ) = yCor;
}

void CaloFutureSCorrection::updateCovariance( double dXhy_dXcl, double dYhy_dYcl, LHCb::CaloHypo* hypo ) const {
  LHCb::CaloPosition::Covariance& covariance = hypo->position()->covariance();

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { debug() << "before s-cor cov.m. = \n" << covariance << endmsg; }

  // cov.m packing in double array[5] following ROOT::Math::SMatrix<double,3,3>::Array()
  // for row/column indices (X:0, Y:1, E:2), see comments in CaloFutureECorrection::process()
  double c1[6];

  c1[0] = covariance( LHCb::CaloPosition::Index::X,
                      LHCb::CaloPosition::Index::X ); // arr[0] not relying on LHCb::CaloPosition::Index::X == 0
  c1[2] = covariance( LHCb::CaloPosition::Index::Y,
                      LHCb::CaloPosition::Index::Y ); // arr[2] not relying on LHCb::CaloPosition::Index::Y == 1
  c1[5] = covariance( LHCb::CaloPosition::Index::E,
                      LHCb::CaloPosition::Index::E ); // arr[5] not relying on LHCb::CaloPosition::Index::E == 2
  c1[1] = covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ); // arr[1]
  c1[3] = covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ); // arr[3]
  c1[4] = covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ); // arr[4]

  // cov1 = (J * cov0 * J^T) for the special case of diagonal Jacobian for (X,Y,E) -> (X1=X1(X), Y1=Y1(Y), E1=E)
  c1[0] *= dXhy_dXcl * dXhy_dXcl;
  c1[1] *= dXhy_dXcl * dYhy_dYcl;
  c1[2] *= dYhy_dYcl * dYhy_dYcl;
  c1[3] *= dXhy_dXcl;
  c1[4] *= dYhy_dYcl;
  // c1[5] remains unchanged (energy is not changed by S-correction)

  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) = c1[0]; // cov1(0,0);
  covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) = c1[2]; // cov1(1,1);
  covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E ) = c1[5]; // cov1(2,2);
  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ) = c1[1]; // cov1(0,1);
  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ) = c1[3]; // cov1(0,2);
  covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ) = c1[4]; // cov1(1,2);

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { debug() << "after s-cor cov.m. = \n" << covariance << endmsg; }
}

// ============================================================================

struct CaloFutureSCorrection::SCorrResults
CaloFutureSCorrection::calculateSCorrections( const struct SCorrInputParams& params ) const {
  struct SCorrResults results {
    0, 0, 0, 0
  };
  const LHCb::CaloCellID& cellID  = params.cellID;
  const Gaudi::XYZPoint&  seedPos = params.seedPos;
  const double&           z       = params.z;
  double                  xBar    = params.x;
  double                  yBar    = params.y;

  double CellSize = m_det->cellSize( cellID );
  double Asx      = -( xBar - seedPos.x() ) / CellSize;
  double Asy      = -( yBar - seedPos.y() ) / CellSize;

  // Sshape correction :
  auto AsxCorDer =
      getCorrectionAndDerivative( CaloFutureCorrection::shapeX, cellID, Asx ).value_or( CorrectionResult{Asx, 1.} );
  Asx                = AsxCorDer.value; // Asx1
  const auto DshapeX = AsxCorDer.derivative;
  auto       AsyCorDer =
      getCorrectionAndDerivative( CaloFutureCorrection::shapeY, cellID, Asy ).value_or( CorrectionResult{Asy, 1.} );
  Asy                = AsyCorDer.value; // Asy1
  const auto DshapeY = AsyCorDer.derivative;

  // Angular correction (if any) [ NEW  - inserted between Sshape and residual correction ]
  const double xs  = seedPos.x() - Asx * CellSize; // xscor
  const double ys  = seedPos.y() - Asy * CellSize; // yscor
  const double thx = myatan2( xs, z );
  const double thy = myatan2( ys, z );
  const auto [daX, DangularX] =
      getCorrectionAndDerivative( CaloFutureCorrection::angularX, cellID, thx ).value_or( CorrectionResult{0., 0.} );
  const auto [daY, DangularY] =
      getCorrectionAndDerivative( CaloFutureCorrection::angularY, cellID, thy ).value_or( CorrectionResult{0., 0.} );
  Asx -= daX;
  Asy -= daY;

  // residual correction (if any):
  auto dcXCorDer =
      getCorrectionAndDerivative( CaloFutureCorrection::residual, cellID, Asx ).value_or( CorrectionResult{0., 0.} );
  auto dcX = dcXCorDer.value;
  if ( dcX == 0. ) {
    // check X-specific correction
    dcXCorDer =
        getCorrectionAndDerivative( CaloFutureCorrection::residualX, cellID, Asx ).value_or( CorrectionResult{0., 0.} );
    dcX = dcXCorDer.value;
  }
  const auto DresidualX = dcXCorDer.derivative;
  auto       dcYCorDer =
      getCorrectionAndDerivative( CaloFutureCorrection::residual, cellID, Asy ).value_or( CorrectionResult{0., 0.} );
  auto dcY = dcYCorDer.value;
  if ( dcY == 0. ) {
    // check Y-specific correction
    dcYCorDer =
        getCorrectionAndDerivative( CaloFutureCorrection::residualY, cellID, Asy ).value_or( CorrectionResult{0., 0.} );
    dcY = dcYCorDer.value;
  }
  const auto DresidualY = dcYCorDer.derivative;
  Asx -= dcX;
  Asy -= dcY;

  // left/right - up/down asymmetries correction (if any) :
  const auto [ddcX, DasymX] =
      getCorrectionAndDerivative( ( xBar < 0 ) ? CaloFutureCorrection::asymM : CaloFutureCorrection::asymP, cellID,
                                  Asx )
          .value_or( CorrectionResult{0., 0.} );
  const auto [ddcY, DasymY] =
      getCorrectionAndDerivative( ( yBar < 0 ) ? CaloFutureCorrection::asymM : CaloFutureCorrection::asymP, cellID,
                                  Asy )
          .value_or( CorrectionResult{0., 0.} );
  Asx += ddcX; // Asx4
  Asy += ddcY; // Asy4

  // Recompute position and fill CaloFuturePosition

  results.xCor = seedPos.x() - Asx * CellSize;
  results.yCor = seedPos.y() - Asy * CellSize;

  /* DG,20140714: derivative calculation for  d(Xhypo)/d(Xcluster)
   *
   * Asx0 =-(xBar - seedPos.x)/CellSize; // xBar = Xcluster
   * Asx1 = shapeX(Asx0)
   * xs   = seedPos.x - Asx1*CellSize
   * thx  = atan(xs/z); // in principle, this brings in an implicit dependence on cluster E, but it's logarithmic so
   * let's neglect it daX  = angular(thx) Asx2 = Asx1 - daX dcX  = residual(Asx2) != 0 ? residual(Asx2) :
   * residualX(Asx2); // add an auxiliary bool residualX_flag Asx3 = Asx2 - dcX ddcX = asym(Asx3) Asx4 = Asx3 + ddcX =
   * Asx Xhypo= xCor(Asx4)  = seedPos.x - Asx4*CellSize
   *
   * d(Xhypo)/d(Xcluster) = d(xCor)/d(Asx4) * product[ d(Asx%i)/d(Asx%{i-1}), for i=1..4 ] * d(Asx0)/d(Xcluster)
   *
   * d(xCor)/d(Asx4)      =-CellSize
   * d(Asx0)/d(Xcluster)  = d(Asx0)/d(xBar)     = -1/CellSize
   * d(Asx1)/d(Asx0)      = DshapeX(Asx0)
   * d(thx)/d(Asx1)       = d(thx)/d(xs) * d(xs)/d(Asx1) =-CellSize/(1+(xc/z)**2)*(1/z)
   * d(xs)/d(Asx1)        =-CellSize
   * d(Asx2)/d(Asx1)      = 1 - d(daX)/d(Asx1)  = 1 - Dangular(thx)*d(thx)/d(Asx1) = 1 +
   * Dangular(thx)*CellSize/z/(1+(xs/z)**2) d(Asx3)/d(Asx2)      = 1 - d(dcX)/d(Asx2)  = 1 - ( residual(Asx2) != 0 ?
   * Dresidual(Asx2) : DresidualX(Asx2) ) residualX_flag       = residual(Asx2) != 0 ? false : true d(Asx4)/d(Asx3) = 1
   * + d(ddcX)/d(Asx3) = 1 + Dasym(Asx3)
   *
   *
   * d(Xhypo)/d(Xcluster) = (1 + Dasym(Asx3)) * (1 - (resudualX_flag ? DresidualX(Asx2) : Dresidual(Asx2)))
   *                       *(1 + Dangular(thx)*CellSize/z/(1+(xs/z)**2)) * DshapeX(Asx0)
   */

  if ( m_correctCovariance ) {
    // calculation of the analytic derivatives:
    // NB: printouts comparing analytic calculations with numeric derivatives which are commented-out below
    // are useful for debugging in case of changes in the correction function code
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "---------- analytic derivatives of individual S-correction functions ---------------" << endmsg;

    double tx = xs / z;
    double ty = ys / z;

    results.dXhy_dXcl =
        ( 1. + DasymX ) * ( 1. - DresidualX ) * ( 1. + DangularX * CellSize / z / ( 1. + tx * tx ) ) * DshapeX;
    results.dYhy_dYcl =
        ( 1. + DasymY ) * ( 1. - DresidualY ) * ( 1. + DangularY * CellSize / z / ( 1. + ty * ty ) ) * DshapeY;
  }
  return results;
}

void CaloFutureSCorrection::debugDerivativesCalculation( const struct SCorrInputParams& inParams,
                                                         const struct SCorrResults&     outParams ) const {
  const double d_rel( 1.e-5 ); // dx ~ 0.1 mm for numeric derivative calculation

  debug() << " ---------- calculation of numeric derivative dXhypo/dXcluster follows -----------" << endmsg;
  struct SCorrInputParams inParams1( inParams );
  inParams1.x     = inParams1.x * ( 1 + d_rel );
  inParams1.y     = inParams1.y;
  auto outParams1 = calculateSCorrections( inParams1 );

  debug() << " ---------- calculation of numeric derivative dYhypo/dYcluster follows -----------" << endmsg;
  struct SCorrInputParams inParams2( inParams );
  inParams2.x     = inParams2.x;
  inParams2.y     = inParams2.y * ( 1 + d_rel );
  auto outParams2 = calculateSCorrections( inParams2 );

  double xCor_x = outParams1.xCor;
  double yCor_x = outParams1.yCor;

  double xCor_y = outParams2.xCor;
  double yCor_y = outParams2.yCor;

  double xBar = inParams.x;
  double yBar = inParams.y;

  double xCor      = outParams.xCor;
  double yCor      = outParams.yCor;
  double dXhy_dXcl = outParams.dXhy_dXcl;
  double dYhy_dYcl = outParams.dYhy_dYcl;

  const double dn_xCor_dx = ( xCor_x - xCor ) / xBar / d_rel;
  const double dn_yCor_dx = ( yCor_x - yCor ) / xBar / d_rel; // sanity test, should be 0
  const double dn_xCor_dy = ( xCor_y - xCor ) / yBar / d_rel; // sanity test, should be 0
  const double dn_yCor_dy = ( yCor_y - yCor ) / yBar / d_rel;

  if ( fabs( ( dXhy_dXcl - dn_xCor_dx ) / dXhy_dXcl ) > 0.02 || fabs( ( dYhy_dYcl - dn_yCor_dy ) / dYhy_dYcl ) > 0.02 ||
       fabs( dn_yCor_dx ) > 1e-8 || fabs( dn_xCor_dy ) > 1e-7 )
    debug() << " SCorrection numerically-calculated Jacobian differs (by > 2%) from analytically-calculated one"
            << endmsg;

  debug() << "================== Jacobian elements ============= " << endmsg;
  debug() << "  semi-analytic dXhy_dXcl = " << dXhy_dXcl << " numeric dn_xCor_dx = " << dn_xCor_dx
          << " dn_xCor_dy = " << dn_xCor_dy << endmsg;
  debug() << "  semi-analytic dYhy_dYcl = " << dYhy_dYcl << " numeric dn_yCor_dy = " << dn_yCor_dy
          << " dn_yCor_dx = " << dn_yCor_dx << endmsg;

  return;
}

// WK: This was part of the comment - maybe it should be moved as kind of unit tests?
// // ---- calculation of numeric derivatives of individual correction functions, important for debugging in case of
// code changes --- debug() << "---------- numeric derivatives of individual S-correction functions ---------------"
// << endmsg; double tmpd = ( fabs(Asx0) > 1.e-5 ) ? Asx0*2.e-2 : 2.e-7; double dn_shapeX    = (
// getCorrection(CaloFutureCorrection::shapeX, cellID, Asx0 + tmpd, Asx0 + tmpd) - Asx1 )/tmpd; tmpd = ( fabs(Asy0)
// > 1.e-5 ) ? Asy0*2.e-2 : 2.e-7; double dn_shapeY    = ( getCorrection(CaloFutureCorrection::shapeY, cellID, Asy0
// + tmpd, Asy0 + tmpd) - Asy1 )/tmpd;
//
// double dn_angularX  = ( getCorrection(CaloFutureCorrection::angularX, cellID, thx*1.002, 0.) - daX )/thx/2e-3;
// double dn_angularY  = ( getCorrection(CaloFutureCorrection::angularY, cellID, thy*1.002, 0.) - daY )/thy/2e-3;
//
// tmpd = ( fabs(Asx2) > 1.e-5 ) ? Asx2*2.e-3 : 2.e-8;
// double dn_residualX = ( getCorrection((residualX_flag ? CaloFutureCorrection::residualX :
// CaloFutureCorrection::residual),
//                                                                                          cellID, Asx2 + tmpd, 0.)
//                                                                                          - dcX )/tmpd;
// tmpd = ( fabs(Asy2) > 1.e-5 ) ? Asy2*2.e-3 : 2.e-8;
// double dn_residualY = ( getCorrection((residualY_flag ? CaloFutureCorrection::residualY :
// CaloFutureCorrection::residual),
//                                                                                         cellID, Asy2 + tmpd, 0.)
//                                                                                         - dcY )/tmpd;
// tmpd = ( fabs(Asx3) > 1.e-5 ) ? Asx3*2.e-3 : 2.e-8;
// double dn_asymX     = (xBar < 0 ) ?
//   ( getCorrection(CaloFutureCorrection::asymM , cellID , Asx2 + tmpd , 0.) - ddcX )/tmpd :
//   ( getCorrection(CaloFutureCorrection::asymP , cellID , Asx2 + tmpd , 0.) - ddcX )/tmpd  ;
//
// tmpd = ( fabs(Asy3) > 1.e-5 ) ? Asy3*2.e-3 : 2.e-8;
// double dn_asymY     = (yBar < 0 ) ?
//   ( getCorrection(CaloFutureCorrection::asymM , cellID , Asy2 + tmpd , 0.) - ddcY )/tmpd :
//   ( getCorrection(CaloFutureCorrection::asymP , cellID , Asy2 + tmpd , 0.) - ddcY )/tmpd  ;
// //
// -------------------------------------------------------------------------------------------------------------------------------
//
// // DG: my little paranoia, should be always ok since Covariance is SMatrix<3,3,double> internally represented
// as double array[5] assert( covariance(LHCb::CaloFuturePosition::Index::X, LHCb::CaloFuturePosition::Index::Y)
// == covariance(LHCb::CaloFuturePosition::Index::Y, LHCb::CaloFuturePosition::Index::X)); assert(
// covariance(LHCb::CaloFuturePosition::Index::X, LHCb::CaloFuturePosition::Index::E) ==
// covariance(LHCb::CaloFuturePosition::Index::E, LHCb::CaloFuturePosition::Index::X)); assert(
// covariance(LHCb::CaloFuturePosition::Index::Y, LHCb::CaloFuturePosition::Index::E) ==
// covariance(LHCb::CaloFuturePosition::Index::E, LHCb::CaloFuturePosition::Index::Y));
// alternatively, a code fragment for a general-form Jacobian (cf. a similar comment in
// CaloFutureECorrection::process()) TMatrixD jac(3, 3); // just a diagonal Jacobian in case of (X,Y,E) -> (X1(X),
// Y1(Y), E) transformation jac(0,0) = dXhy_dXcl; jac(1,1) = dYhy_dYcl; jac(2,2) = 1.; if ( msgLevel( MSG::DEBUG)
// ){ debug() << "s-cor jacobian = " << endmsg; jac.Print(); } TMarixDSym cov0(3) = ...          // to be
// initilized from hypo->position()->covariance() TMarixDSym cov1(3);               // resulting extrapolated
// cov.m. recalculate_cov(jac, cov0, cov1); // calculate:  cov1 = (J * cov0 * J^T)
