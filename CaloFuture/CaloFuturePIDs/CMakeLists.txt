###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: CaloFuturePIDs
################################################################################
gaudi_subdir(CaloFuturePIDs v5r22)

gaudi_depends_on_subdirs(CaloFuture/CaloFutureInterfaces
                         CaloFuture/CaloFutureUtils
                         GaudiAlg
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         Kernel/Relations
                         Tr/TrackKernel
                         Tr/TrackInterfaces)

find_package(AIDA)
find_package(Boost)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(CaloFuturePIDs
                 src/*.cpp
                 INCLUDE_DIRS AIDA Tr/TrackInterfaces Tr/TrackKernel
                 LINK_LIBRARIES CaloFutureUtils GaudiAlgLib LHCbKernel TrackKernel LHCbMathLib RelationsLib)

gaudi_install_python_modules()

gaudi_env(SET CALOFUTUREPIDSOPTS \${CALOFUTUREPIDSROOT}/options)

