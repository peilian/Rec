/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class BremPIDeAlgFuture  BremPIDeAlgFuture.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================
namespace LHCb::Calo {
  class BremPIDeAlg final : public ID2DLL {
  public:
    /// Standard protected constructor
    BremPIDeAlg( const std::string& name, ISvcLocator* pSvc ) : ID2DLL( name, pSvc ) {
      using CaloFutureAlgUtils::CaloFutureIdLocation;

      updateHandleLocation( *this, "Input", CaloFutureIdLocation( "BremChi2" ) );
      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "BremPIDe" ) );

      setProperty( "nVlong", Gaudi::Utils::toString( 200 ) ).ignore();
      setProperty( "nVvelo", Gaudi::Utils::toString( 200 ) ).ignore();
      setProperty( "nVupstr", Gaudi::Utils::toString( 200 ) ).ignore();
      setProperty( "nMlong", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMvelo", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMupstr", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) ).ignore();

      setProperty( "HistogramU", "DLL_Long" ).ignore();
      setProperty( "HistogramL", "DLL_Long" ).ignore();
      setProperty( "HistogramV", "DLL_Long" ).ignore();
      setProperty( "ConditionName", "Conditions/ParticleID/Calo/BremPIDe" ).ignore();

      setProperty( "HistogramU_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3" ).ignore();
      setProperty( "HistogramL_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3" ).ignore();
      setProperty( "HistogramV_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3" ).ignore();

      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( Track::Types::Velo, Track::Types::Long, Track::Types::Upstream ) )
          .ignore();
    };
  };
} // namespace LHCb::Calo
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::BremPIDeAlg, "BremPIDeAlgFuture" )

// ============================================================================
