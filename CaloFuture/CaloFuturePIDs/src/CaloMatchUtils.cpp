/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloMatchUtils.h"
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureInterfaces/CaloFutureMatch3D.h"
#include "Event/CaloPosition.h"
#include "Event/State.h"

namespace LHCb::Calo {
  /// get 2D-information form CaloPosition
  Match2D getMatch2D( const LHCb::CaloPosition& c ) {
    const auto& par = c.center();
    const auto& cov = c.spread();

    Match2D::Matrix matrix;
    matrix( 0, 0 ) = cov( 0, 0 );
    matrix( 0, 1 ) = cov( 0, 1 );
    matrix( 1, 1 ) = cov( 1, 1 );
    return {{par( 0 ), par( 1 )}, matrix};
  }

  // get 2D-information from State
  Match2D getMatch2D( const LHCb::State& s ) {
    const auto& par = s.stateVector();
    const auto& cov = s.covariance();

    Match2D::Matrix matrix;
    matrix( 0, 0 )         = cov( 0, 0 );
    matrix( 0, 1 )         = cov( 0, 1 );
    matrix( 1, 1 )         = cov( 1, 1 );
    Match2D::Vector vector = {par( 0 ), par( 1 )};
    return {vector, matrix};
  }

  /// get 3D-infomration form CaloPosition
  Match3D getMatch3D( const LHCb::CaloPosition& c ) {
    const auto& par = c.parameters();
    const auto& cov = c.covariance();

    Match3D::Vector vector;
    vector( 0 ) = par( LHCb::CaloPosition::Index::X );
    vector( 1 ) = par( LHCb::CaloPosition::Index::Y );
    vector( 2 ) = par( LHCb::CaloPosition::Index::E );
    Match3D::Matrix matrix;
    matrix( 0, 0 ) = cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X );
    matrix( 0, 1 ) = cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y );
    matrix( 0, 2 ) = cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E );
    matrix( 1, 1 ) = cov( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y );
    matrix( 1, 2 ) = cov( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E );
    matrix( 2, 2 ) = cov( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E );
    return {vector, matrix};
  }

  /// get 2D-infomration from CaloPosition for Bremstrahlung
  Match2D getBremMatch2D( const LHCb::CaloPosition& c ) {
    const auto& par = c.parameters();
    const auto& cov = c.covariance();

    Match2D::Vector vector;
    vector( 0 ) = par( LHCb::CaloPosition::Index::X );
    vector( 1 ) = par( LHCb::CaloPosition::Index::Y );
    Match2D::Matrix matrix;
    matrix( 0, 0 ) = cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X );
    matrix( 0, 1 ) = cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y );
    matrix( 1, 1 ) = cov( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y );
    return {vector, matrix};
  }
} // namespace LHCb::Calo
