/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatchAlg.h"

// ============================================================================
/** @class FuturePhotonMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
namespace LHCb::Calo {
  using TABLE           = RelationWeighted2D<CaloCluster, Track, float>;
  using CALOFUTURETYPES = CaloClusters;

  struct PhotonMatchAlg final : TrackMatchAlg<TABLE, CALOFUTURETYPES> {
    static_assert( std::is_base_of_v<CaloFuture2Track::IClusTrTable2D, TABLE>,
                   "TABLE must inherit from IClusTrTable2D" );
    PhotonMatchAlg( const std::string& name, ISvcLocator* pSvc ) : TrackMatchAlg<TABLE, CALOFUTURETYPES>( name, pSvc ) {
      updateHandleLocation( *this, "Calos", CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" ) );
      updateHandleLocation( *this, "Output", CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" ) );
      updateHandleLocation( *this, "Filter", CaloFutureAlgUtils::CaloFutureIdLocation( "InEcal" ) );

      setProperty( "Tool", "CaloFuturePhotonMatch/FuturePhotonMatch" ).ignore();
      setProperty( "Threshold", "1000" ).ignore();
      // track types:
      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( Track::Types::Long, Track::Types::Downstream, Track::Types::Ttrack ) )
          .ignore();
      setProperty( "TableSize", "5000" ).ignore();
    }
  };
} // namespace LHCb::Calo
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::PhotonMatchAlg, "FuturePhotonMatchAlg" )
