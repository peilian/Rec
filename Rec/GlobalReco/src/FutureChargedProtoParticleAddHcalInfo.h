/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddHcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H 1

// from Gaudi
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

/** @class FutureChargedProtoParticleAddHcalInfo FutureChargedProtoParticleAddHcalInfo.h
 *
 *  Updates the CALO HCAL information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddHcalInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg {

public:
  /// Standard constructor
  FutureChargedProtoParticleAddHcalInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Load the Calo Hcal tables
  bool getHcalData();

  /// Add Calo Hcal information to the given ProtoParticle
  bool addHcal( LHCb::ProtoParticle* proto ) const;

private:
  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inHcalPath;
  std::string m_hcalEPath;
  std::string m_hcalPIDePath;
  std::string m_hcalPIDmuPath;

  const LHCb::CaloFuture2Track::ITrAccTable*  m_InHcalTable    = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable* m_HcalETable     = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable* m_dlleHcalTable  = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable* m_dllmuHcalTable = nullptr;
};

#endif // GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H
