/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "GaudiAlg/Consumer.h"

// using LegacyBaseClass_t = Gaudi::Functional::Traits::BaseClass_t<::Algorithm>;
namespace {
  using BaseClass_t = Gaudi::Functional::Traits::BaseClass_t<Gaudi::Algorithm>;
}

struct PrintProtoParticles final : Gaudi::Functional::Consumer<void( LHCb::ProtoParticles const& ), BaseClass_t> {
  PrintProtoParticles( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {"Input", ""} ){

        };

  void operator()( LHCb::ProtoParticles const& protos ) const override {
    always() << "Print all proto particles of an event" << endmsg;
    for ( auto const* proto : protos ) {
      always() << "{";
      always() << " Track " << ( proto->track() != nullptr ) << " CaloHypos " << proto->calo().size() << " RichPID "
               << ( proto->richPID() != nullptr ) << " MuonPID " << ( proto->muonPID() != nullptr ) << "\n";
      always() << "ExtraInfo [";
      for ( const auto& i : proto->extraInfo() ) {
        const auto info = static_cast<LHCb::ProtoParticle::additionalInfo>( i.first );
        always() << " " << info << "=" << i.second;
      }
      always() << " ] } \n";
    }

    always() << "Printing ended" << endmsg;
  }
};
DECLARE_COMPONENT( PrintProtoParticles )
