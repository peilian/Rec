/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDAlg.cpp
 *
 *  Implementation file for ANN Combined PID algorithm ChargedProtoANNPIDAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/09/2010
 */
//-------------------------------------------------------------------------------

// STL
#include <fstream>
#include <memory>
#include <sstream>

// Gaudi
#include "GaudiKernel/IJobOptionsSvc.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// local
#include "ChargedProtoANNPIDAlgBase.h"

// boost
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDAlg ChargedProtoANNPIDAlg.h
   *
   *  Adds ANN PID information to ProtoParticles
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoANNPIDAlg final : public ChargedProtoANNPIDAlgBase {

  public:
    /// Standard constructor
    ChargedProtoANNPIDAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : ChargedProtoANNPIDAlgBase( name, pSvcLocator ) {
      // JOs
      declareProperty( "TrackType", m_trackType = "UNDEFINED" );
      declareProperty( "PIDType", m_pidType = "UNDEFINED" );
      declareProperty( "NetworkVersion", m_netVersion = "MCUpTuneV1" );
      // turn off histos and ntuples
      setProperty( "HistoProduce", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      setProperty( "NTupleProduce", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      setProperty( "EvtColsProduce", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Destructor
    virtual ~ChargedProtoANNPIDAlg() = default;

    /// Initialise
    StatusCode initialize() override {

      const auto sc = ChargedProtoANNPIDAlgBase::initialize();
      if ( !sc ) return sc;

      // Create a new network configation
      m_netConfig = std::make_unique<NetConfig>( m_trackType, m_pidType, m_netVersion, this );
      if ( !m_netConfig->isOK() ) { return Error( "Failed to configure the classifier" ); }

      // Determine the track type to fill
      m_tkType = LHCb::Track::TypesToType( m_trackType );

      // Proto variable to fill
      if ( "Electron" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNe;
      } else if ( "Muon" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNmu;
      } else if ( "Pion" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNpi;
      } else if ( "Kaon" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNk;
      } else if ( "Proton" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNp;
      } else if ( "Deuteron" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNd;
      } else if ( "Ghost" == m_pidType ) {
        m_protoInfo = LHCb::ProtoParticle::additionalInfo::ProbNNghost;
      } else {
        return Error( "Unknown particle type '" + m_pidType + "'" );
      }

      // return
      return sc;
    }

    /// Execute
    StatusCode execute() override {

      // Load the charged ProtoParticles
      auto protos = getIfExists<LHCb::ProtoParticles>( m_protoPath );
      if ( !protos ) return StatusCode::SUCCESS;

      // shortcut to mva
      const auto mva = m_netConfig->netHelper();

      // local cache for input variable storage
      auto vars = mva->inputStorage();

      // Loop over ProtoParticles
      for ( auto proto : *protos ) {

        // Select Tracks
        if ( UNLIKELY( !proto->track() ) ) { return Error( "Charged ProtoParticle has NULL Track pointer" ); }
        if ( !proto->track()->checkType( m_tkType ) ) continue;

        // Clear current ANN PID information
        if ( UNLIKELY( proto->hasInfo( m_protoInfo ) ) ) {
          //       std::ostringstream mess;
          //       mess << "ProtoParticle already has '" << m_protoInfo
          //            << "' information -> Replacing.";
          //       Warning( mess.str(), StatusCode::SUCCESS, 1 ).ignore();
          proto->eraseInfo( m_protoInfo );
        }

        // ANN Track Selection.
        if ( UNLIKELY( !m_netConfig->passCuts( proto ) ) ) continue;

        // get the ANN output for this proto
        const auto nnOut = mva->getOutput( proto, vars );

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "ProtoParticle " << *proto << endmsg;
          verbose() << " -> Inputs :";
          for ( const auto& in : mva->inputs() ) { verbose() << " " << in->name() << "=" << in->value( proto ); }
          verbose() << endmsg;
          verbose() << " -> ANN value = " << nnOut << endmsg;
        }

        // add to protoparticle
        proto->addInfo( m_protoInfo, nnOut );

      } // loop over protos

      return StatusCode::SUCCESS;
    }

  private:
    /// Track type
    std::string m_trackType;

    /// PID type
    std::string m_pidType;

    /// The version of the PID networks training to use
    std::string m_netVersion;

    /// Network Configuration
    std::unique_ptr<NetConfig> m_netConfig;

    /// The extra info to fill on the ProtoParticle
    LHCb::ProtoParticle::additionalInfo m_protoInfo = LHCb::ProtoParticle::additionalInfo::NoPID;

    /// The track type for this instance
    LHCb::Track::Types m_tkType = LHCb::Track::Types::TypeUnknown;
  };

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoANNPIDAlg )

  //=============================================================================

} // namespace ANNGlobalPID
