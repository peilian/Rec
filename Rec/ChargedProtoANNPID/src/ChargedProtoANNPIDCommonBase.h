/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDCommonBase.h
 *
 *  Declaration file for ANNGlobalPID::ChargedProtoANNPIDCommonBase
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   03/02/2011
 */
//-------------------------------------------------------------------------------

#pragma once

// STL
#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <vector>

// Gaudi
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/ToolHandle.h"

// interfaces
#include "TrackInterfaces/ITrackStateProvider.h"

// Event Model
#include "Event/ODIN.h"
#include "Event/ProtoParticle.h"
#include "Event/RecSummary.h"

// Kernel LHCbMath
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"

#include "TMVAIClassifierReader.h"

// TMVA
#include "TMVA/IMethod.h"
#include "TMVA/Reader.h"

// TMVA Factory
#include "TMVAImpFactory.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

//-----------------------------------------------------------------------------
/** @namespace ANNGlobalPID
 *
 *  General namespace for Global PID ANN software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2010-03-09
 */
//-----------------------------------------------------------------------------

namespace ANNGlobalPID {
  /** @class ChargedProtoANNPIDCommonBase ChargedProtoANNPIDCommonBase.h
   *
   *  Common base class
   *
   *  @author Chris Jones
   *  @date   03/02/2011
   */

  template <class PBASE>
  class ChargedProtoANNPIDCommonBase : public PBASE {

  public:
    // inherit constructors
    using PBASE::PBASE;

    /// destructor
    virtual ~ChargedProtoANNPIDCommonBase() = default;

  public:
    /// Initialization after creation
    StatusCode initialize() override {
      return PBASE::initialize().andThen( [&] {
        // Avoid auto-loading of state provider
        m_trStateP.disable();
      } );
    }

  protected:
    /// Type for list of inputs
    using StringInputs = std::vector<std::string>;

  protected:
    /** @class Input ChargedProtoANNPIDCommonBase.h
     *
     *  Base class for all 'input' classes, that handle getting
     *  a particular value from a given ProtoParticle
     *
     *  @author Chris Jones
     *  @date   15/04/2013
     */
    class Input {
    public:
      /// Destructor
      virtual ~Input() = default;

    public:
      /// Access the input value for a given ProtoParticle
      virtual double value( const LHCb::ProtoParticle* proto ) const = 0;

    public:
      /// Shared pointer type
      using SmartPtr = std::unique_ptr<const Input>;
      /// Type for a vector of inputs
      using ConstVector = std::vector<SmartPtr>;

    public:
      /// Access the input name
      const std::string& name() const { return m_name; }
      /// Set the name
      void setName( const std::string& name ) { m_name = name; }

    private:
      /// Name
      std::string m_name;
    };

  private:
    /// ProtoParticle Extra Info object
    class InProtoExInfo final : public Input {
    public:
      InProtoExInfo( const LHCb::ProtoParticle::additionalInfo info, const double def = -999 )
          : m_info( info ), m_def( def ) {}

    private:
      const LHCb::ProtoParticle::additionalInfo m_info;
      const double                              m_def{-999};

    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override { return proto->info( m_info, m_def ); }
    };

    /// Track Extra Info object
    class InTrackExInfo final : public Input {
    public:
      InTrackExInfo( const LHCb::Track::AdditionalInfo info, const double def = -999 ) : m_info( info ), m_def( def ) {}

    private:
      const LHCb::Track::AdditionalInfo m_info = LHCb::Track::AdditionalInfo::AdditionalInfoUnknown;
      const double                      m_def{-999};

    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return proto->track()->info( m_info, m_def );
      }
    };

    /// Track Momentum
    class InTrackP final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        constexpr auto MaxP = 5000.0 * Gaudi::Units::GeV;
        const auto     var  = proto->track()->p();
        return ( var < MaxP ? var : -999 );
      }
    };

    /// Track Transverse Momentum
    class InTrackPt final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        constexpr auto MaxPt = 1000.0 * Gaudi::Units::GeV;
        const auto     var   = proto->track()->pt();
        return ( var < MaxPt ? var : -999 );
      }
    };

    /// Track (x,y,z) position
    class InTrackXYZ final : public Input {
    public:
      /// The coordinate to return
      enum class Coord { Undefined, X, Y, Z };
      /// The position in LHCb for the state information
      enum class Where { Undefined, Vertex, RICH1Entry, RICH2Entry, RICH1Exit, RICH2Exit };

    public:
      /// No default constructor
      InTrackXYZ() = delete;
      /// Constructor
      InTrackXYZ( const Coord                                coord,  //
                  const Where                                where,  //
                  const ChargedProtoANNPIDCommonBase<PBASE>* parent, //
                  const double                               defValue = -9999 )
          : m_coord{coord}, m_where{where}, m_parent{parent}, m_defValue{defValue} {
        assert( Coord::Undefined != m_coord );
        assert( Where::Undefined != m_where );
        // z positions for each RICHes entry and exit planes
        switch ( m_where ) {
        case Where::RICH1Entry:
          m_z = 990;
          break;
        case Where::RICH1Exit:
          m_z = 2165;
          break;
        case Where::RICH2Entry:
          m_z = 9450;
          break;
        case Where::RICH2Exit:
          m_z = 11900;
          break;
        default:
          m_z = 0;
          break;
        }
      }

    private:
      Coord                                      m_coord{Coord::Undefined}; ///< The coordinate to return (X, Y or Z).
      Where                                      m_where{Where::Undefined}; ///< The position in LHCb
      const ChargedProtoANNPIDCommonBase<PBASE>* m_parent = nullptr;        ///< Pointer to parent
      double m_defValue{0}; ///< Default value to return when requested info is missing.
      double m_z{0};        ///< The z position for the information
    private:
      /// returns the required coordinate from a given vector-like object
      template <typename VECT>
      inline double coord( const VECT& v ) const {
        switch ( m_coord ) {
        case Coord::X:
          return v.x();
        case Coord::Y:
          return v.y();
        case Coord::Z:
          return v.z();
        default:
          return m_defValue;
        }
      }

    public:
      /// Compute the input value for a given protoparticle
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        if ( m_where == Where::Vertex ) {
          // just use the first (vertex) state
          return coord( proto->track()->firstState() );
        } else {
          // copy the vertex state
          auto state = proto->track()->firstState();
          // move to required z position
          const auto sc = m_parent->stateProvider().stateFromTrajectory( state, *proto->track(), m_z );
          // return the required coord
          return ( sc ? coord( state ) : m_defValue );
        }
      }
    };

    /// Track Likelihood
    class InTrackLikelihood final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        const auto var = proto->track()->likelihood();
        return ( var > -120.0 ? var : -999 );
      }
    };

    /// Track Ghost Probability
    class InTrackGhostProb final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        const auto var = proto->track()->ghostProbability();
        return ( var > 0.00001 ? var : -999 );
      }
    };

    /// Track Clone distance
    class InTrackCloneDist final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        const auto var = proto->track()->info( LHCb::Track::AdditionalInfo::CloneDist, -999 );
        return ( var >= 0 ? var : -999 );
      }
    };

    /// Track DOCA
    class InTrackDOCA final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        const auto&                s = proto->track()->firstState();
        const Gaudi::Math::XYZLine z_axis( Gaudi::XYZPoint( 0, 0, 0 ), Gaudi::XYZVector( 0, 0, 1 ) );
        const Gaudi::Math::XYZLine track_line( Gaudi::XYZPoint( s.x(), s.y(), s.z() ),
                                               Gaudi::XYZVector( s.tx(), s.ty(), 1.0 ) );
        return Gaudi::Math::distance( track_line, z_axis );
      }
    };

    /// Used RICH Aerogel
    class InRichUsedAerogel final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->usedAerogel() : 0 );
      }
    };

    /// Used RICH Rich1 Gas
    class InRichUsedR1Gas final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->usedRich1Gas() : 0 );
      }
    };

    /// Used RICH Rich2 Gas
    class InRichUsedR2Gas final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->usedRich2Gas() : 0 );
      }
    };

    /// RICH above electron threshold
    class InRichAboveElThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->electronHypoAboveThres() : 0 );
      }
    };

    /// RICH above muon threshold
    class InRichAboveMuThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->muonHypoAboveThres() : 0 );
      }
    };

    /// RICH above pion threshold
    class InRichAbovePiThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->pionHypoAboveThres() : 0 );
      }
    };

    /// RICH above kaon threshold
    class InRichAboveKaThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->kaonHypoAboveThres() : 0 );
      }
    };

    /// RICH above proton threshold
    class InRichAbovePrThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->protonHypoAboveThres() : 0 );
      }
    };

    /// RICH above deuteron threshold
    class InRichAboveDeThres final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->deuteronHypoAboveThres() : 0 );
      }
    };

    /// RICH DLL accessor
    class InRichDLL final : public Input {
    public:
      InRichDLL( const Rich::ParticleIDType type ) : m_type( type ) {}

    private:
      const Rich::ParticleIDType m_type;

    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->richPID() ? proto->richPID()->particleDeltaLL( m_type ) : -999 );
      }
    };

    /// Muon IsMuon flag
    class InMuonIsMuon final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() ? proto->muonPID()->IsMuon() : 0 );
      }
    };

    /// Muon IsMuonLoose flag
    class InMuonIsMuonLoose final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() ? proto->muonPID()->IsMuonLoose() : 0 );
      }
    };

    /// Muon # shared hits
    class InMuonNShared final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return proto->info( LHCb::ProtoParticle::additionalInfo::MuonNShared, -1.0 ) + 1.0;
      }
    };

    /// Muon Muon likelihood
    class InMuonLLMu final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->MuonLLMu() : -999 );
      }
    };

    /// Muon background likelihood
    class InMuonLLBkg final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->MuonLLBg() : -999 );
      }
    };

    /// Muon Chi2Corr
    class InMuonChi2Corr final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->chi2Corr() : -999 );
      }
    };

    /// Muon MVA1
    class InMuonMVA1 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA1() : -999 );
      }
    };

    /// Muon MVA2
    class InMuonMVA2 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA2() : -999 );
      }
    };

    /// Muon MVA3
    class InMuonMVA3 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA3() : -999 );
      }
    };

    /// Muon MVA4
    class InMuonMVA4 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return ( proto->muonPID() && proto->muonPID()->IsMuonLoose() ? proto->muonPID()->muonMVA4() : -999 );
      }
    };

    /// Number ProtoParticles
    class InNumProtos : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        const auto* protos = dynamic_cast<const LHCb::ProtoParticles*>( proto->parent() );
        return ( protos ? static_cast<double>( protos->size() ) : -999 );
      }
    };

    /// Number of CALO Hypos
    class InNumCaloHypos final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        return static_cast<double>( proto->calo().size() );
      }
    };

    /// Access a variable from the RecSummary
    class InRecSummary final : public Input {
    public:
      InRecSummary( const LHCb::RecSummary::DataTypes info, const ChargedProtoANNPIDCommonBase<PBASE>* parent,
                    const double defValue = 0 )
          : m_info( info ), m_parent( parent ), m_defValue( defValue ) {}

    private:
      const LHCb::RecSummary::DataTypes          m_info;             ///< The info variable to access
      const ChargedProtoANNPIDCommonBase<PBASE>* m_parent = nullptr; ///< Pointer to parent
      double m_defValue{0}; ///< Default value to return when requested info is missing.
    public:
      virtual double value( const LHCb::ProtoParticle* ) const override {
        return ( m_parent && m_parent->recSummary() ? m_parent->recSummary()->info( m_info, m_defValue ) : -999 );
      }
    };

    /// Calo Ecal chi^2
    class InCaloEcalChi2 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, -999 );
        if ( var < -100 ) {
          var = -999;
        } else if ( var > 9999.99 ) {
          var = -999;
        }
        return var;
      }
    };

    /// Calo Brem chi^2
    class InCaloBremChi2 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, -999 );
        if ( var < -100 ) {
          var = -999;
        } else if ( var > 9999.99 ) {
          var = -999;
        }
        return var;
      }
    };

    /// Calo Cluster chi^2
    class InCaloClusChi2 final : public Input {
    public:
      virtual double value( const LHCb::ProtoParticle* proto ) const override {
        auto var = proto->info( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, -999 );
        if ( var < -100 ) {
          var = -999;
        } else if ( var > 999.99 ) {
          var = -999;
        }
        return var;
      }
    };

    /// Access ODIN Run number
    class InODINRunNumber final : public Input {
    public:
      InODINRunNumber( const ChargedProtoANNPIDCommonBase<PBASE>* parent, const double defValue = 0 )
          : m_parent( parent ), m_defValue( defValue ) {}

    private:
      const ChargedProtoANNPIDCommonBase<PBASE>* m_parent = nullptr; ///< Pointer to parent
      double m_defValue{0}; ///< Default value to return when requested info is missing.
    public:
      virtual double value( const LHCb::ProtoParticle* ) const override {
        const auto odin = m_parent->odin();
        return ( odin ? odin->runNumber() : m_defValue );
      }
    };

    /// Access ODIN event number
    class InODINEventNumber final : public Input {
    public:
      InODINEventNumber( const ChargedProtoANNPIDCommonBase<PBASE>* parent, const double defValue = 0 )
          : m_parent( parent ), m_defValue( defValue ) {}

    private:
      const ChargedProtoANNPIDCommonBase<PBASE>* m_parent = nullptr; ///< Pointer to parent
      double m_defValue{0}; ///< Default value to return when requested info is missing.
    public:
      virtual double value( const LHCb::ProtoParticle* ) const override {
        const auto odin = m_parent->odin();
        return ( odin ? odin->eventNumber() : m_defValue );
      }
    };

  protected:
    /** @class Cut ChargedProtoANNPIDCommonBase.h
     *
     *  ProtoParticle selection cut
     *
     *  @author Chris Jones
     *  @date   2010-03-09
     */
    class Cut {

    public:
      /// Shared Pointer
      using UniquePtr = std::unique_ptr<const Cut>;
      /// Vector of cuts
      using ConstVector = std::vector<UniquePtr>;

    private:
      /// Delimitor enum
      enum class Delim { UNDEFINED, GT, LT, GE, LE };

    public:
      /// Constructor
      Cut( const std::string& desc = "NOTDEFINED", const ChargedProtoANNPIDCommonBase<PBASE>* parent = nullptr );
      /// No Copy Constructor
      Cut( const Cut& ) = delete;

    public:
      /// Is this object well defined
      bool isOK() const noexcept { return m_OK; }
      /// Does the ProtoParticle pass the cut
      bool isSatisfied( const LHCb::ProtoParticle* proto ) const {
        const auto var = m_variable->value( proto );
        switch ( m_delim ) {
        case Delim::GT:
          return var > m_cut;
        case Delim::LT:
          return var < m_cut;
        case Delim::GE:
          return var >= m_cut;
        case Delim::LE:
          return var <= m_cut;
        default:
          return false;
        }
      }
      /// Cut description
      const std::string description() const noexcept { return m_desc; }

    public:
      /// Overload output to ostream
      friend inline std::ostream& operator<<( std::ostream& s, const Cut& cut ) {
        return s << "'" << cut.description() << "'";
      }
      /// Overload output to ostream
      friend inline std::ostream& operator<<( std::ostream& s, const Cut::UniquePtr& cut ) {
        return s << "'" << cut->description() << "'";
      }

    private:
      /// Set the delimitor enum from a string
      bool setDelim( const std::string& delim ) noexcept {
        bool ok = false;
        if ( ">" == delim ) {
          m_delim = Delim::GT;
          ok      = true;
        } else if ( "<" == delim ) {
          m_delim = Delim::LT;
          ok      = true;
        } else if ( ">=" == delim ) {
          m_delim = Delim::GE;
          ok      = true;
        } else if ( "<=" == delim ) {
          m_delim = Delim::LE;
          ok      = true;
        }
        return ok;
      }

    private:
      std::string              m_desc;                    ///< The cut description
      bool                     m_OK{false};               ///< Is this cut well defined
      typename Input::SmartPtr m_variable;                ///< The variable to cut on
      double                   m_cut{0};                  ///< The cut value
      Delim                    m_delim{Delim::UNDEFINED}; ///< The delimitor
    };

  protected:
    /** @class ANNHelper ChargedProtoANNPIDCommonBase.h
     *
     *  Base class for ANN helpers
     *
     *  @author Chris Jones
     *  @date   2010-03-09
     */
    class ANNHelper {

    protected:
      /// Type for list of inputs
      using Inputs = typename Input::ConstVector;

    protected:
      /// No default constructor
      ANNHelper() = default;

    public:
      /** Constructor from information
       *  @param inputs The list of inputs needed for this network
       *  @param parent Pointer to parent algorithm
       */
      ANNHelper( const ChargedProtoANNPIDCommonBase<PBASE>::StringInputs& inputs,
                 const ChargedProtoANNPIDCommonBase<PBASE>*               parent )
          : m_inputs( parent->getInputs( inputs ) ) {}

      /// Destructor
      virtual ~ANNHelper() = default;

    public:
      /// Are we configured properly
      bool isOK() const noexcept { return m_mva && m_mva->IsStatusClean(); }

    public:
      /// Type for input values
      using InVars = std::vector<double>;

    public:
      /// Access the inputs
      const Inputs& inputs() const noexcept { return m_inputs; }

      /// Generate input storage vector
      decltype( auto ) inputStorage() const { return InVars( inputs().size(), 0 ); }

      /// Fill the input variables for given ProtoParticle
      inline void fillInputVars( const LHCb::ProtoParticle* proto, InVars& vars ) const {
        const auto& ins = this->inputs();
        assert( vars.size() == ins.size() );
        std::transform( ins.begin(), ins.end(), vars.begin(),
                        [&proto]( const auto& input ) { return input->value( proto ); } );
      }

    private:
      /// Compute the ANN output for the given variables
      decltype( auto ) getOutput( const InVars& vars ) const {
        // FPE Guard for MVA call
        FPE::Guard guard( true );
        // call the specific IClassifierReader instance to use
        return m_mva->GetMvaValue( vars );
      }

    public:
      /// Compute the ANN output for the given ProtoParticle using the given input storage
      inline decltype( auto ) getOutput( const LHCb::ProtoParticle* proto, InVars& vars ) const {
        // fill inputs
        fillInputVars( proto, vars );
        // compute MVA and return
        return getOutput( vars );
      }

      /// Compute the ANN output for the given ProtoParticle
      inline decltype( auto ) getOutput( const LHCb::ProtoParticle* proto ) const {
        // input vars storage
        auto vars = inputStorage();
        // compute MVA and return
        return getOutput( proto, vars );
      }

    private:
      /// The list of inputs for this network
      Inputs m_inputs;

    protected:
      /// Pointer to the underlying MVA implementation. Set by specific implementation.
      IClassifierReader* m_mva{nullptr};
    };

    /** @class TMVAReaderANN ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class for TMVA ANN networks via the Reader
     *
     *  @author Chris Jones
     *  @date   2013-03-09
     */
    class TMVAReaderANN final : public ANNHelper, public IClassifierReader {

    public:
      /// No default constructor
      TMVAReaderANN() = delete;

    public:
      /** Constructor from information
       *  @param paramFileName Network tuning parameter file
       *  @param inputs        The list of inputs needed for this network
       *  @param parent        Pointer to parent algorithm
       */
      TMVAReaderANN( const std::string&                         paramFileName, //
                     const StringInputs&                        inputs,        //
                     const ChargedProtoANNPIDCommonBase<PBASE>* parent )
          : ANNHelper( inputs, parent )
          , m_reader(
                std::make_unique<TMVA::Reader>( parent->msgLevel( MSG::DEBUG ) ? "!Color:!Silent" : "!Color:Silent" ) )
          , m_vars( inputs.size(), 0 ) {
        // init the inputs
        unsigned int i = 0;
        for ( const auto& input : inputs ) { m_reader->AddVariable( input.c_str(), &( m_vars[i++] ) ); }
        m_reader->BookMVA( "PID", paramFileName.c_str() );
        // check and set status
        this->fStatusIsClean = m_reader && m_reader->FindMVA( "PID" );
        // set the pointer to the IClassifierReader instance used by the public API
        this->m_mva = this;
      }

    private:
      /// Compute the ANN output for the given vector of inputs
      double GetMvaValue( const typename ANNHelper::InVars& vars ) const override;

    private:
      std::unique_ptr<TMVA::Reader> m_reader;   ///< The TMVA reader
      mutable std::vector<float>    m_vars;     ///< Working array for network inputs
      mutable std::mutex            m_varsLock; ///< mutex lock
    };

    /** @class TMVAImpANN ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class for TMVA ANN networks via the C++ implementation
     *
     *  @author Chris Jones
     *  @date   2013-03-09
     */
    class TMVAImpANN final : public ANNHelper {

    public:
      /// No default constructor
      TMVAImpANN() = delete;

    public:
      /** Constructor from information
       *  @param config   The tune name
       *  @param particle The particle type
       *  @param track    The track type
       *  @param inputs   The list of inputs needed for this network
       *  @param parent   Pointer to parent algorithm
       */
      TMVAImpANN( const std::string&                         config,   //
                  const std::string&                         particle, //
                  const std::string&                         track,    //
                  const StringInputs&                        inputs,   //
                  const ChargedProtoANNPIDCommonBase<PBASE>* parent )
          : ANNHelper( inputs, parent ) //
          , m_reader( tmvaFactory().create( config, particle, track, inputs ) ) {
        // set the pointer to the IClassifierReader instance used by the public API
        this->m_mva = m_reader.get();
      }
      /// Destructor
      virtual ~TMVAImpANN() = default;

    private:
      std::unique_ptr<IClassifierReader> m_reader; ///< The owned compiled TMVA instance
    };

  protected:
    /** @class NetConfig ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class that encapsulates the configration of an ANN nework
     *
     *  @author Chris Jones
     *  @date   2014-06-27
     */
    class NetConfig final {

    public:
      /// Constructor
      NetConfig( const std::string&                         trackType,  //
                 const std::string&                         pidType,    //
                 const std::string&                         netVersion, //
                 const ChargedProtoANNPIDCommonBase<PBASE>* parent );

      /// Access the Network object
      inline decltype( auto ) netHelper() const noexcept { return m_netHelper.get(); }

      /// Status
      inline bool isOK() const noexcept { return netHelper() && netHelper()->isOK(); }

      /// Access the track type
      inline const std::string& trackType() const noexcept { return m_trackType; }

      /// Access the particle type
      inline const std::string& particleType() const noexcept { return m_particleType; }

      /// Check a ProtoParticle against the configured cuts
      inline bool passCuts( const LHCb::ProtoParticle* proto ) const noexcept {
        return std::all_of( m_cuts.begin(), m_cuts.end(), //
                            [&proto]( const auto& cut ) { return cut->isSatisfied( proto ); } );
      }

    private:
      /// Clean up
      inline void cleanUp() {
        m_netHelper.reset();
        m_cuts.clear();
        m_particleType = "";
        m_trackType    = "";
      }

    private:
      /// Network Helper
      std::unique_ptr<const ANNHelper> m_netHelper;

      /// Vector of cuts to apply
      typename Cut::ConstVector m_cuts;

      /// The particle type
      std::string m_particleType;

      /// The track type
      std::string m_trackType;
    };

  public:
    /** Get the Input object for a given input name
     *  @attention Created on the heap therefore user takes ownership
     */
    typename Input::SmartPtr getInput( const std::string& name ) const;

    /** Get a vector of input objects for a given set of names
     *  @attention Created on the heap therefore user takes ownership
     */
    typename Input::ConstVector getInputs( const StringInputs& names ) const;

    /// Get the track state provider
    const ITrackStateProvider& stateProvider() const {
      if ( UNLIKELY( !m_trStateP.get() ) ) { m_trStateP.retrieve().ignore(); }
      assert( m_trStateP.get() );
      return *m_trStateP.get();
    }

  private:
    /// Access on demand the RecSummary object
    const LHCb::RecSummary* recSummary() const;

    /// Access on demand the ODIN object
    const LHCb::ODIN* odin() const;

  protected:
    /// Location in TES of ProtoParticles
    Gaudi::Property<std::string> m_protoPath{this, "ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged};

    /// Location in TES for RecSummary object
    Gaudi::Property<std::string> m_recSumPath{this, "RecSummaryLocation", LHCb::RecSummaryLocation::Default};

    /// Location in TES for ODIN object
    Gaudi::Property<std::string> m_odinPath{this, "ODINLocation", LHCb::ODINLocation::Default};

    /// Track state provider
    ToolHandle<const ITrackStateProvider> m_trStateP{this, "TrackStateProvider", "TrackStateProvider/StateProvider"};
  };

} // namespace ANNGlobalPID
