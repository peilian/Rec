/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/System.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Context.h"
#include "LoKi/HybridBase.h"
#include "LoKi_v2/ITrackFunctorAntiFactory.h"
#include "LoKi_v2/ITrackFunctorFactory.h"
#include "LoKi_v2/TrackFactoryLock.h"
// ============================================================================
/** @file
 *
 *  definition and implementation file for class LoKi::Hybrid::Pr::TrackFunctorFactory
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV, Sascha Stahl
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid::Pr {
    // ========================================================================
    /** @class TrackFunctorFactory
     *
     *  Concrete impelmentation of LoKi::ITrHybridTool interface
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV, Sascha Stahl
     */
    class TrackFunctorFactory : public virtual LoKi::Hybrid::Base,
                                public virtual LoKi::Pr::ITrackFunctorFactory,
                                public virtual LoKi::Pr::ITrackFunctorAntiFactory {
    public:
      // ======================================================================
      /// finalization   of the tool
      StatusCode finalize() override;
      // ======================================================================
    public:
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get( const std::string& pycode, LoKi::Pr::Types::TrCut& cuts, const std::string& context ) override {
        return _get( pycode, m_trcuts, cuts, context );
      }
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get( const std::string& pycode, LoKi::Pr::Types::TrFun& func, const std::string& context ) override {
        return _get( pycode, m_trfunc, func, context );
      }
      // ========================================================================
    public: // "Anti-Factory"
      // ========================================================================
      /// set the C++ predicate for LHCb::Track
      void set( const LoKi::Pr::Types::TrCuts& cut ) override { LoKi::Hybrid::Base::_set( m_trcuts, cut ); }
      /// set the C++ function for LHCb::Track
      void set( const LoKi::Pr::Types::TrFunc& fun ) override { LoKi::Hybrid::Base::_set( m_trfunc, fun ); }
      // ======================================================================
      /// constructor
      TrackFunctorFactory( const std::string& type, const std::string& name, const IInterface* parent );
      // ======================================================================
    private:
      // ======================================================================
      /// helper method to save many lines:
      template <class TYPE1, class TYPE2>
      inline StatusCode _get( const std::string& pycode, std::unique_ptr<LoKi::Functor<TYPE1, TYPE2>>& local,
                              typename LoKi::Assignable<LoKi::Functor<TYPE1, TYPE2>>::Type& output,
                              const std::string&                                            context );
      // ======================================================================
    protected:
      // ======================================================================
      // local holder of cuts
      std::unique_ptr<LoKi::Pr::Types::TrCuts> m_trcuts;
      std::unique_ptr<LoKi::Pr::Types::TrFunc> m_trfunc;
      typedef std::vector<std::string>         Modules;
      Modules                                  m_modules;
      std::string                              m_actor = "LoKi.Hybrid.Pr.TrackEngine()";
      typedef std::vector<std::string>         Lines;
      Lines                                    m_lines;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Hybrid::Pr
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// helper method to save many lines:
// ============================================================================
template <class TYPE1, class TYPE2>
inline StatusCode LoKi::Hybrid::Pr::TrackFunctorFactory::_get(
    const std::string& pycode, std::unique_ptr<LoKi::Functor<TYPE1, TYPE2>>& local,
    typename LoKi::Assignable<LoKi::Functor<TYPE1, TYPE2>>::Type& output, const std::string& context ) {
  // ==========================================================================
  std::lock_guard guard( m_mutex );
  // consistency check:
  const LoKi::Functor<TYPE1, TYPE2>* tmp = &output;
  StatusCode                         sc  = ( 0 != tmp ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
  // prepare the actual python code
  std::string code = makeCode( m_modules, m_actor, pycode, m_lines, context );
  // define the scope:  ATTENTION: the scope is locked!!
  LoKi::Hybrid::Pr::TrackFactoryLock lock( this, make_context() );
  // use the base class method
  sc = LoKi::Hybrid::Base::_get_( code, local, output );
  if ( sc.isFailure() ) { return Error( "Invalid object for the code '" + pycode + "'" ); } // RETURN
  //
  return StatusCode::SUCCESS;
  // =========================================================================
}
// ============================================================================
// Standard constructor
// ============================================================================
LoKi::Hybrid::Pr::TrackFunctorFactory::TrackFunctorFactory( const std::string& type, const std::string& name,
                                                            const IInterface* parent )
    // : base_class ( type , name , parent )
    : LoKi::Hybrid::Base( type, name, parent )
///
{
  declareInterface<LoKi::Pr::ITrackFunctorFactory>( this );
  declareInterface<LoKi::Pr::ITrackFunctorAntiFactory>( this );
  //
  m_modules.push_back( "LoKiTrack_v2.decorators" );
  m_modules.push_back( "LoKiCore.math" );
  m_modules.push_back( "LoKiCore.functions" );
  m_modules.push_back( "LoKiTrack_v2.functions" );
  //
  declareProperty( "Modules", m_modules, "Python modules to be imported" );
  declareProperty( "Actor", m_actor, "The processing engine" );
  declareProperty( "Lines", m_lines, "Additional Python lines to be executed" );
  // ==========================================================================
  // C++
  // ==========================================================================
  m_cpplines.push_back( "#include \"LoKi_v2/LoKiTrack.h\"" );
  // ==========================================================================
}
// ============================================================================
// finalization of the tool
// ============================================================================
StatusCode LoKi::Hybrid::Pr::TrackFunctorFactory::finalize() {
  //
  m_trcuts.reset();
  m_trfunc.reset();
  // finalize the base
  return LoKi::Hybrid::Base::finalize();
}
// ============================================================================
DECLARE_COMPONENT( LoKi::Hybrid::Pr::TrackFunctorFactory )
// ============================================================================

// ============================================================================
// The END
// ============================================================================
