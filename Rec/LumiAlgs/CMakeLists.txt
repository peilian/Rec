###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(LumiAlgs)

gaudi_depends_on_subdirs(Det/DetDesc
                         Event/DAQEvent
                         Event/EventBase
                         Event/LumiEvent
                         Event/RecEvent
                         GaudiAlg
                         GaudiConf
                         Kernel/FSRAlgs
                         Kernel/HltInterfaces
                         Kernel/LHCbAlgs
                         Kernel/LHCbKernel
                         DAQ/DAQSys)

find_package(Boost COMPONENTS iostreams)
find_package(RELAX)

find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(LumiAlgs
                 src/*.cpp
                 INCLUDE_DIRS Boost RELAX Event/EventBase Event/LumiEvent Kernel/FSRAlgs
                 LINK_LIBRARIES Boost RELAX DetDescLib DAQEventLib RecEvent GaudiAlgLib HltInterfaces LHCbKernel)

gaudi_install_python_modules()

gaudi_add_dictionary(LumiAlgs
                     dict/LumiAlgsDict.h
                     dict/LumiAlgsDict.xml
                     LINK_LIBRARIES Boost GaudiKernel)

gaudi_install_headers(LumiAlgs)

# Prepare test Git CondDB overlay
file(REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/test/Lumi_IOV)
file(COPY tests/data/Lumi_IOV/ DESTINATION test/Lumi_IOV/)
execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/Lumi_IOV)

gaudi_add_test(QMTest QMTEST ENVIRONMENT TEST_OVERLAY_ROOT=${CMAKE_CURRENT_BINARY_DIR}/test/Lumi_IOV)



# FIXME: workaround for bug GAUDI-1007
set(test_names
    lumi0conddbchecker
    lumi0writer
    lumi1writer
    lumi2reader
    lumi3conftest
    lumi40fsrwriter
    lumi4fsrwriter
    lumi5fsrreader
    lumi60fsrintegrator
    lumi6fsrintegrator
    lumi7fsr2file
    lumi8merger
    lumi9fsrintegrator
    lumi9mergesmallfiles)

list(GET test_names 0 prereq)
list(REMOVE_AT test_names 0)

while(test_names)
  list(GET test_names 0 test_name)
  list(REMOVE_AT test_names 0)
  set_property(TEST LumiAlgs.${test_name} APPEND PROPERTY DEPENDS LumiAlgs.${prereq})
  set(prereq ${test_name})
endwhile()
