/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATPV_PVSEED3DOFFLINETOOL_H
#define PATPV_PVSEED3DOFFLINETOOL_H 1

// Include files
#include <optional>
// from Gaudi
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "IPVSeeding.h" // Interface
#include "TrackInterfaces/ITrackExtrapolator.h"

namespace SimplePVSpace {

  struct seedPoint final {
    Gaudi::XYZPoint position;
    Gaudi::XYZPoint error;
    int             multiplicity = 0;

    seedPoint() = default;
  };

  struct seedState final {
    LHCb::State lbstate;
    int         nclose = 0;
    double      dclose = 0;
    int         used   = 0;

    seedState() = default;
  };

  struct closeNode final {
    seedState*      seed_state = nullptr;
    double          distance   = 0;
    Gaudi::XYZPoint closest_point;
    bool            take = false;

    closeNode() = default;
  };

} // namespace SimplePVSpace

/** @class PVSeed3DOfflineTool PVSeed3DOfflineTool.h tmp/PVSeed3DOfflineTool.h
 *
 *
 *  @author Mariusz Witek
 *  @date   2005-11-19
 */
class PVSeed3DOfflineTool : public extends<GaudiTool, IPVSeeding> {
public:
  /// Standard constructor
  using extends::extends;

  std::vector<Gaudi::XYZPoint> getSeeds( const std::vector<const LHCb::Track*>& inputTracks,
                                         const Gaudi::XYZPoint&                 beamspot ) const override;

private:
  ToolHandle<ITrackExtrapolator> m_fullExtrapolator{this, "FullExtrapolator", "TrackMasterExtrapolator"};

  std::optional<SimplePVSpace::seedPoint> simpleMean( std::vector<SimplePVSpace::closeNode>& close_nodes ) const;
  SimplePVSpace::seedPoint                wMean( const std::vector<SimplePVSpace::closeNode>& close_nodes,
                                                 const SimplePVSpace::seedState&              base_state ) const;

  Gaudi::Property<double> m_TrackPairMaxDistance{this, "TrackPairMaxDistance", 5. * Gaudi::Units::mm,
                                                 "maximum distance between tracks"};
  Gaudi::Property<double> m_TrackPairMaxDistanceChi2{this, "TrackPairMaxDistanceChi2", 25.,
                                                     "maximum distance pseudo Chi2 between tracks"};
  Gaudi::Property<double> m_zMaxSpread{this, "zMaxSpread", 1000. * Gaudi::Units::mm, "for truncated mean"};
  Gaudi::Property<int>    m_MinCloseTracks{this, "MinCloseTracks", 3};
};
#endif // PATPV_PVSEED3DOFFLINETOOL_H
