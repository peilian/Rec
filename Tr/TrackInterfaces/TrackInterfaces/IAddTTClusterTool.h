/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IADDTTCLUSTERTOOL_H
#define TRACKINTERFACES_IADDTTCLUSTERTOOL_H 1

// Include files
// -------------
#include <vector>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

/** @class IAddTTClusterTool IAddTTClusterTool.h TrackInterfaces/IAddClusterTool.h
 *
 *  Interface for tools that add TT hits to long tracks like PatAddTTCoord
 *  Naming is mostly historical
 *
 *  @author:  Jeroen van Tilburg Jeroen.van.Tilburg@cern.nl
 *  @author:  Michel De Cian (removed obsolete pure virtual functions)
 *  @date:    04-02-2010
 */

// forward declaration
namespace LHCb {
  class State;
  class STCluster;

} // namespace LHCb
class PatTTHit;

struct IAddTTClusterTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IAddTTClusterTool, 2, 0 );

  /// Add TT clusters to matched tracks
  virtual StatusCode addTTClusters( LHCb::Track& track ) = 0;
  virtual StatusCode returnTTClusters( LHCb::State& state, std::vector<PatTTHit*>& ttHits, double& finalChi2,
                                       double p )        = 0;
};
#endif // TRACKINTERFACES_IADDTTCLUSTERTOOL_H
