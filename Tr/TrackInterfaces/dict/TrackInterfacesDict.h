/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_TRACKINTERFACESDICT_H
#define DICT_TRACKINTERFACESDICT_H 1

#include "TrackInterfaces/IAddTTClusterTool.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IHltV0Upgrade.h"
#include "TrackInterfaces/IMatchTool.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/IMeasurementProviderProjector.h"
#include "TrackInterfaces/IPVOfflineTool.h"
#include "TrackInterfaces/IPrVeloUTFit.h"
#include "TrackInterfaces/IPromoteClusters.h"
#include "TrackInterfaces/IPtTransporter.h"
#include "TrackInterfaces/ISTClusterCollector.h"
#include "TrackInterfaces/IStateCorrectionTool.h"
#include "TrackInterfaces/ITrackCaloMatch.h"
#include "TrackInterfaces/ITrackChi2Calculator.h"
#include "TrackInterfaces/ITrackCloneFinder.h"
#include "TrackInterfaces/ITrackExtraSelector.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackInterpolator.h"
#include "TrackInterfaces/ITrackKalmanFilter.h"
#include "TrackInterfaces/ITrackManipulator.h"
#include "TrackInterfaces/ITrackMatch.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"
#include "TrackInterfaces/ITrackSelector.h"
#include "TrackInterfaces/ITrackStateInit.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackInterfaces/ITrackVertexer.h"
#include "TrackInterfaces/ITracksFromTrack.h"
#include "TrackInterfaces/ITrajFitter.h"

#endif // DICT_TRACKINTERFACESDICT_H
