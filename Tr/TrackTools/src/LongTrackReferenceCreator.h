/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _LongTrackReferenceCreator_H
#define _LongTrackReferenceCreator_H

/** @class LongTrackReferenceCreator LongTrackReferenceCreator.h
 *
 * Implementation of TrackCaloMatch tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   30/12/2005
 */

#include "GaudiAlg/GaudiTool.h"

#include "Event/Track.h"
#include "TrackInterfaces/ITrackManipulator.h"
#include <string>

namespace LHCb {
  class State;
  class Measurement;
} // namespace LHCb

struct ITrackExtrapolator;

class LongTrackReferenceCreator : public extends<GaudiTool, ITrackManipulator> {

public:
  /** constructer */
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  /** add reference info to the track */
  StatusCode execute( LHCb::Track& aTrack ) const override;

private:
  ITrackExtrapolator* m_extrapolator = nullptr;
};

#endif
