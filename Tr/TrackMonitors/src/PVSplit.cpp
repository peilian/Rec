/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/PartitionPosition.h"
#include "TrackInterfaces/IPVOfflineTool.h"
#include "VeloDet/DeVelo.h"
#include "boost/dynamic_bitset.hpp"
#include <algorithm>
#include <numeric>

using Mask = boost::dynamic_bitset<>;

//-----------------------------------------------------------------------------
// Implementation file for class : PVSplit
//
// 2012-06-21 : Colin Barschel, Rosen Matev
//-----------------------------------------------------------------------------

/** @class PVSplit PVSplit.h
 *  Algorithm to split tracks belonging to found vertices, fit them and
 *  write split vertices to TES.
 *
 *  For each vertex in m_inputVerticesLocation, the following is executed.
 *  0) The tracks associated with the vertex are randomly shuffled according to option
 *  1) They are split into two parts according to split method:
 *     a) Middle - split into two halves with equal number of tracks
 *        if total number of tracks is even. Otherwise, (if total number of
 *        tracks is odd), one of the split containers (randomly chosen) has
 *        one track more.
 *     b) VeloHalf - a track is put in first (second) container if number of
 *        hits in left (right) sensors is larger. If number of left and right hits
 *        are the same, container is randomly chosen.
 *     c) MiddlePerVeloHalf - split into two nearly equal parts each having
 *        nearly equal number of left and right tracks
 *     d) VeloTopBottom - a track is put in first (second) container if number of
 *        hits in top (bottom) phi sensors is larger.
 *  2) The two track containers are fitted with
 *     IPVOfflineTool::reconstructSinglePVFromTracks() (using the PV as seed) or
 *     with IPVOfflineTool::reconstructMultiPVFromTracks() (making a new seed).
 *  3) Fitted vertices are written to a TES location. If CopyPV is true, each
 *     input vertex is copied before the two split vertices. Each split vertex is
 *     'tagged' with the parent vertex' index and Z. Additionally, for each split
 *     vertex, the sum of the weights (from the parent vertex) of all
 *     constituting tracks is stored. To extract parent vertex index and sum of
 *     weights do:
 *       int index = (int)vertex.info(PVSplit::ParentVertexIndex, -1);
 *       double wsum = vertex.info(PVSplit::SumOfParentWeights, 0.);
 *     If a split vertex is not successfully reconstructed, a vertex is still
 *     written with a technique=Unknown and an empty tracks container.
 *
 *  @author Colin Barschel, Rosen Matev
 *  @date   2012-07-17, 2015-04-15
 */

//-----------------------------------------------------------------------------
class PVSplit : public Gaudi::Functional::Transformer<LHCb::RecVertices( LHCb::RecVertices const& )> {

public:
  PVSplit( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode        initialize() override; ///< Algorithm initialization
  LHCb::RecVertices operator()( LHCb::RecVertices const& ) const override;

private:
  std::array<int, 2> countVeloLhcbIDsLeftRight( const LHCb::Track* track ) const;
  std::array<int, 2> countVeloLhcbIDsTopBottom( const LHCb::Track* track ) const;
  bool               isLeftTrack( const LHCb::Track* track ) const;
  bool               isTopTrack( const LHCb::Track* track ) const;

  void debugVertex( const LHCb::RecVertex* vx ) const;

  void randomShuffleTracks( LHCb::span<const LHCb::Track*> tracks, LHCb::span<double> weights ) const;
  Mask splitTracksByMiddle( LHCb::span<const LHCb::Track*> tracks ) const;
  Mask splitTracksByVeloHalf( LHCb::span<const LHCb::Track*> tracks ) const;
  Mask splitTracksByVeloTopBottom( LHCb::span<const LHCb::Track*> tracks ) const;
  Mask splitTracksByMiddlePerVeloHalf( LHCb::span<const LHCb::Track*> tracks ) const;

  using SplitTracksFun = Mask ( PVSplit::* )( LHCb::span<const LHCb::Track*> tracks ) const;

  Gaudi::Property<bool>        m_randomShuffle{this, "RandomShuffle", true}; ///< Whether to shuffle tracks first
  SplitTracksFun               m_splitTracksFun = nullptr;
  Gaudi::Property<std::string> m_splitMethodStr{
      this, "SplitMethod", "middle",
      [=]( Property& ) {
        std::transform( m_splitMethodStr.begin(), m_splitMethodStr.end(), m_splitMethodStr.begin(), ::tolower );
        if ( m_splitMethodStr.value() == "middle" )
          m_splitTracksFun = &PVSplit::splitTracksByMiddle;
        else if ( m_splitMethodStr.value() == "velohalf" )
          m_splitTracksFun = &PVSplit::splitTracksByVeloHalf;
        else if ( m_splitMethodStr.value() == "middlepervelohalf" )
          m_splitTracksFun = &PVSplit::splitTracksByMiddlePerVeloHalf;
        else if ( m_splitMethodStr.value() == "velotopbottom" )
          m_splitTracksFun = &PVSplit::splitTracksByVeloTopBottom;
        else {
          throw std::runtime_error{"Unknown split method '" + m_splitMethodStr + "'!"};
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; ///< How to split track container (see enum SplitMethod
                                                                 ///< for possible values)
  Gaudi::Property<bool> m_usePVForSeed{this, "UsePVForSeed", true}; ///< Use the PV for seed of the split vertex fit
  Gaudi::Property<bool> m_copyPV{this, "CopyPV", false};            ///< Whether to copy PVs to the output container
  Gaudi::Property<int>  m_keyOfParentVertexIndex{this, "KeyOfParentVertexIndex", 1000001};
  Gaudi::Property<int>  m_keyOfSumOfParentWeights{this, "KeyOfSumOfParentWeights", 1000002};
  Gaudi::Property<int>  m_keyOfParentVertexZ{this, "KeyOfParentVertexZ", 1000003};

  ToolHandle<IPVOfflineTool> m_pvsfit{this, "PVOfflineTool", "PVOfflineTool"};
  mutable Rndm::Numbers      m_rndm;
  const DeVelo*              m_velo = nullptr;
};

DECLARE_COMPONENT( PVSplit )

unsigned int randomMiddle( unsigned int n, int oddShift ) { return n / 2 + ( n % 2 ) * oddShift; }

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PVSplit::PVSplit( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputVerticesLocation", LHCb::RecVertexLocation::Primary},
                  {"OutputVerticesLocation", "Rec/Vertex/Split"}} {}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode PVSplit::initialize() {
  return Transformer::initialize()
      .andThen( [&] { return m_rndm.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } )
      .andThen( [&] { m_velo = getDet<DeVelo>( DeVeloLocation::Default ); } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::RecVertices PVSplit::operator()( LHCb::RecVertices const& inputVertices ) const {

  LHCb::RecVertices outputVertices;

  for ( const LHCb::RecVertex* vx : inputVertices ) {

    auto vxKey = vx->key();

    if ( m_copyPV.value() ) {
      LHCb::RecVertex* vxCopy = vx->clone();
      vxCopy->addInfo( m_keyOfParentVertexIndex, vxKey );
      outputVertices.insert( vxCopy );
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Original vertex:\n";
      debugVertex( vx );
    }
    std::vector<const LHCb::Track*> tracks;
    std::vector<double>             weights;

    tracks.assign( vx->tracks().begin(), vx->tracks().end() );
    if ( vx->weights().size() == vx->tracks().size() )
      weights.assign( vx->weights().begin(), vx->weights().end() );
    else
      weights.assign( vx->tracks().size(), 1.0 );

    if ( m_randomShuffle.value() ) randomShuffleTracks( tracks, weights );

    // create mask to split tracks
    auto mask = std::invoke( m_splitTracksFun, this, tracks );

    // Split tracks
    auto n               = LHCb::cxx::partitionByMask( tracks, mask );
    auto splitTracks     = std::array{LHCb::make_span( tracks ).first( n ), LHCb::make_span( tracks ).subspan( n )};
    auto splitTracksWSum = std::array{0., 0.};
    for ( unsigned i = 0; i != weights.size(); ++i ) splitTracksWSum[mask[i] ? 0 : 1] += weights[i];
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Split track containers size: n1=" << splitTracks[0].size() << ", n2=" << splitTracks[1].size()
              << endmsg;

    for ( int j = 0; j < 2; j++ ) {
      LHCb::RecVertex splitVx; // sensible defaults, see RecVertex() and VertexBase()
      bool            ok = false;

      std::vector<const LHCb::Track*> trks{splitTracks[j].begin(), splitTracks[j].end()};
      if ( m_usePVForSeed.value() ) {
        ok = m_pvsfit->reconstructSinglePVFromTracks( vx->position(), trks, splitVx ).isSuccess();
      } else {
        std::vector<LHCb::RecVertex> rvts; // output for reconstructMultiPVFromTracks()
        ok = m_pvsfit->reconstructMultiPVFromTracks( trks, rvts )
                 .andThen( [&] {
                   if ( !rvts.empty() ) {
                     ok      = true;
                     splitVx = rvts.front();
                   }
                 } )
                 .isSuccess();
      }

      // The next is to protect from the future (setTechnique() is redundant as
      // the default is Unknown and the fitter should set Primary on success.)
      if ( ok ) {
        splitVx.setTechnique( LHCb::RecVertex::RecVertexType::Primary );
      } else {
        splitVx.setTechnique( LHCb::RecVertex::RecVertexType::Unknown );
        splitVx.clearTracks(); // number of tracks is used to identify success
      }

      splitVx.addInfo( m_keyOfParentVertexIndex, vxKey );
      splitVx.addInfo( m_keyOfSumOfParentWeights, splitTracksWSum[j] );
      splitVx.addInfo( m_keyOfParentVertexZ, vx->position().z() );

      outputVertices.insert( splitVx.clone() );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Split vertex " << j + 1 << ":\n";
        debugVertex( &splitVx );
      }
    }
  }
  return outputVertices;
}

void PVSplit::randomShuffleTracks( LHCb::span<const LHCb::Track*> tracks, LHCb::span<double> weights ) const {
  unsigned int n = tracks.size();
  for ( unsigned int i = n - 1; i > 0; --i ) {
    // pick random int from 0 to i inclusive
    int j = static_cast<int>( m_rndm.shoot() * ( i + 1 ) );
    std::swap( tracks[i], tracks[j] );
    std::swap( weights[i], weights[j] );
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Randomized tracks (n=" << tracks.size() << "):\n";
    for ( unsigned int i = 0; i != n; ++i ) debug() << tracks[i] << " " << weights[i] << "\n";
    debug() << endmsg;
  }
}

Mask PVSplit::splitTracksByMiddle( LHCb::span<const LHCb::Track*> tracks ) const {
  int          oddShift = ( m_rndm.shoot() > 0.5 ); // add 0 or 1 if odd number of tracks
  unsigned int n1       = randomMiddle( tracks.size(), oddShift );
  return Mask{static_cast<unsigned int>( tracks.size() )}.set( 0, n1, true );
}

Mask PVSplit::splitTracksByVeloTopBottom( LHCb::span<const LHCb::Track*> tracks ) const {
  Mask mask{static_cast<unsigned int>( tracks.size() )};
  for ( unsigned int i = 0; i != tracks.size(); ++i ) mask.set( i, isTopTrack( tracks[i] ) );
  return mask;
}

Mask PVSplit::splitTracksByVeloHalf( LHCb::span<const LHCb::Track*> tracks ) const {
  Mask mask{static_cast<unsigned int>( tracks.size() )};
  for ( unsigned int i = 0; i != tracks.size(); i++ ) mask.set( i, isLeftTrack( tracks[i] ) );
  return mask;
}

Mask PVSplit::splitTracksByMiddlePerVeloHalf( LHCb::span<const LHCb::Track*> tracks ) const {

  std::vector<unsigned int> lrIndices[2];
  for ( unsigned int i = 0; i < tracks.size(); i++ ) { lrIndices[isLeftTrack( tracks[i] )].push_back( i ); }

  // Determine the number of tracks for the first split vertex from the
  // left and right set of tracks.
  int          oddShift = ( m_rndm.shoot() > 0.5 ); // 0 or 1
  unsigned int n1[2]    = {randomMiddle( lrIndices[0].size(), oddShift ),
                        randomMiddle( lrIndices[1].size(), 1 - oddShift )};

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "splitTracksByMiddlePerVeloHalf():\n";
    for ( int h = 0; h < 2; ++h ) {
      debug() << ( h == 0 ? "    left:   " : "    right:  " ) << "n1 = " << n1[h]
              << "  n2 = " << lrIndices[h].size() - n1[h] << '\n';
    }
    debug() << endmsg;
  }

  Mask mask{static_cast<unsigned int>( tracks.size() )};
  for ( int h = 0; h < 2; ++h ) {
    for ( unsigned int k = 0; k < lrIndices[h].size(); k++ ) { mask.set( lrIndices[h][k], k < n1[h] ); }
  }
  return mask;
}

std::array<int, 2> PVSplit::countVeloLhcbIDsLeftRight( const LHCb::Track* track ) const {
  const auto& ids = track->lhcbIDs();
  return std::accumulate( ids.begin(), ids.end(), std::array{0, 0}, [&]( auto a, const LHCb::LHCbID& id ) {
    if ( id.isVelo() ) {
      const DeVeloSensor* sensor = m_velo->sensor( id.veloID() );
      ++a[sensor->isLeft() ? 0 : 1];
    }
    return a;
  } );
}

std::array<int, 2> PVSplit::countVeloLhcbIDsTopBottom( const LHCb::Track* track ) const {
  const auto& ids = track->lhcbIDs();
  return std::accumulate( ids.begin(), ids.end(), std::array{0, 0}, [&]( auto a, const LHCb::LHCbID& id ) {
    if ( id.isVelo() ) {
      const LHCb::VeloChannelID veloID = id.veloID();
      if ( const auto* sensor = m_velo->phiSensor( veloID ); sensor ) { // check if sensor is indeed phi type
        // can use globalPhiOfStrip() as well (makes tiny difference)
        bool top = sin( sensor->idealPhiOfStrip( veloID.strip() ) ) > 0;
        ++a[top ? 0 : 1];
      }
    }
    return a;
  } );
}

bool PVSplit::isLeftTrack( const LHCb::Track* track ) const {
  auto [nLeftIDs, nRightIDs] = countVeloLhcbIDsLeftRight( track );
  return ( nLeftIDs == nRightIDs ) ? ( m_rndm.shoot() < 0.5 ) : ( nLeftIDs > nRightIDs );
}

bool PVSplit::isTopTrack( const LHCb::Track* track ) const {
  auto [nTopIDs, nBottomIDs] = countVeloLhcbIDsTopBottom( track );
  return ( nTopIDs != nBottomIDs ) ? ( nTopIDs > nBottomIDs ) : ( m_rndm.shoot() < 0.5 );
}

void PVSplit::debugVertex( const LHCb::RecVertex* vx ) const {
  std::ostringstream oss;
  vx->fillStream( oss );
  debug() << oss.str() << endmsg;
}
