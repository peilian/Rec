###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackFitter
################################################################################
gaudi_subdir(TrackFitter v5r8)

gaudi_depends_on_subdirs(Event/TrackEvent
                         GaudiAlg
                         GaudiKernel
                         Kernel/LHCbMath
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

find_package(GSL)

gaudi_add_module(TrackFitter
                 src/*.cpp
                 INCLUDE_DIRS GSL Tr/TrackInterfaces
                 LINK_LIBRARIES GSL TrackEvent GaudiAlgLib GaudiKernel LHCbMathLib TrackFitEvent TrackKernel)

gaudi_install_python_modules()
