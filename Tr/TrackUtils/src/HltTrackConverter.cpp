/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IANNSvc.h"
#include <bitset>
#include <utility>
//-----------------------------------------------------------------------------
// Implementation file for class : HltTrackConverter
//
// 2010-05-03 : Albert Frithjof Bursche
//-----------------------------------------------------------------------------
namespace details {
  struct UseHitsFrom : private std::bitset<9> {
    UseHitsFrom( std::initializer_list<LHCb::LHCbID::channelIDtype> t ) {
      for ( auto i : t ) set( i );
    }
    bool                 test( LHCb::LHCbID id ) const { return bitset::test( id.detectorType() ); }
    friend StatusCode    parse( UseHitsFrom& result, const std::string& input );
    friend std::ostream& toStream( UseHitsFrom value, std::ostream& os );
  };
  namespace {
    const auto labels = std::array{
        std::pair{"AllTrackingStations", UseHitsFrom{LHCb::LHCbID::channelIDtype::Velo, LHCb::LHCbID::channelIDtype::TT,
                                                     LHCb::LHCbID::channelIDtype::IT, LHCb::LHCbID::channelIDtype::OT}},
        std::pair{"ST", UseHitsFrom{LHCb::LHCbID::channelIDtype::TT, LHCb::LHCbID::channelIDtype::IT}},
        std::pair{"Muon", UseHitsFrom{LHCb::LHCbID::channelIDtype::Muon}},
        std::pair{"Velo", UseHitsFrom{LHCb::LHCbID::channelIDtype::Velo}},
        std::pair{"TT", UseHitsFrom{LHCb::LHCbID::channelIDtype::TT}},
        std::pair{"IT", UseHitsFrom{LHCb::LHCbID::channelIDtype::IT}},
        std::pair{"OT", UseHitsFrom{LHCb::LHCbID::channelIDtype::OT}}};
  } // namespace
  StatusCode parse( UseHitsFrom& result, const std::string& input ) {
    std::vector<std::string> temp;
    using Gaudi::Parsers::parse;
    return parse( temp, input ).andThen( [&]() -> StatusCode {
      result.reset();
      for ( auto& it : temp ) {
        auto kt = std::find_if( labels.begin(), labels.end(), [&]( const auto& l ) { return it == l.first; } );
        if ( kt == labels.end() ) return StatusCode::FAILURE;
        result |= kt->second;
      }
      return StatusCode::SUCCESS;
    } );
  }
  std::ostream& toStream( UseHitsFrom value, std::ostream& os ) {
    std::vector<std::string> vs;
    for ( auto [label, mask] : labels ) {
      if ( ( value & mask ) == mask ) {
        vs.emplace_back( label );
        value &= ~mask;
      }
    }
    using Gaudi::Utils::toStream;
    return toStream( vs, os );
  }
} // namespace details

/** @class HltTrackConverter HltTrackConverter.h
 *
 *
 *  @author Albert Frithjof Bursche
 *  @date   2010-05-03
 */
class HltTrackConverter : public Gaudi::Functional::Transformer<LHCb::Tracks( const LHCb::HltSelReports& )> {
public:
  /// Standard constructor
  HltTrackConverter( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::Tracks operator()( const LHCb::HltSelReports& selReports ) const override;

private:
  Gaudi::Property<details::UseHitsFrom> m_useHitsFrom{
      this, "UseHitsFrom", details::labels[0].second,
      "Names of stations of which hits should be used"}; // may cause segfault in case LHCb::LHCbID::channelIDype is
                                                         // changed
  mutable Gaudi::Property<std::vector<std::string>> m_HltLines{
      this, "HltLinesToUse", {"Hlt1Global"}, "Names of Trigger lines of which hits should be used"}; // see Thomasz mail
                                                                                                     // for better
                                                                                                     // solution
  mutable Gaudi::Property<bool> m_HltLinesFrom1stEvent{this, "ReadHltLinesFrom1stEvent", false,
                                                       "Names of Trigger lines of which hits should be used"};
  Gaudi::Property<bool>         m_requireTrackClassID{
      this, "RequireTrackClassID", true, "Require that the HLTObjectSummaryClID is equal to LHCb::Track::classID()"};
  Gaudi::Property<bool>         m_addFirstState{this, "AddFirstState", true, "Add a state to the track."};
  Gaudi::Property<double>       m_CloneOverlapTreshold{this, "CloneOverlapTreshold", 0.7,
                                                 "Remove every track if there is another track with "
                                                 "'CloneOverlapTreshold' overlap. The other track is then kept."};
  Gaudi::Property<unsigned int> m_MinimalHits{
      this, "MinimalHits", 3,
      "Remove every track having less than this number of hits (in the selected tracking stations)"};

  void executeRecursive( LHCb::Track::Vector& tracks, const LHCb::HltObjectSummary& SelRep ) const;
  void RemoveClones( LHCb::Track::Vector& tracks ) const;
  void initializeTriggerLists() const;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltTrackConverter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltTrackConverter::HltTrackConverter( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"SelReportsLocation", LHCb::HltSelReportsLocation::Default},
                  {"TrackDestination", "Rec/Track/ConvertedHltTracks"}} {}

void HltTrackConverter::initializeTriggerLists() const {
  const auto& ann_svc     = svc<IANNSvc>( "ANNDispatchSvc" );
  const auto& m_hlt1_init = ann_svc->keys( Gaudi::StringKey( "Hlt1SelectionID" ) );
  const auto& m_hlt2_init = ann_svc->keys( Gaudi::StringKey( "Hlt2SelectionID" ) );
  m_HltLines.value().insert( m_HltLines.end(), m_hlt1_init.begin(), m_hlt1_init.end() );
  m_HltLines.value().insert( m_HltLines.end(), m_hlt2_init.begin(), m_hlt2_init.end() );

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << "Available Hlt1 Triggers\n";
    for ( const auto& it : m_hlt1_init ) debug() << it << "\n";
    debug() << endmsg;
    debug() << "Available Hlt2 Triggers\n";
    for ( const auto& it : m_hlt2_init ) debug() << it << "\n";
    debug() << endmsg;
  }
}

//=============================================================================
// Main execution
//=============================================================================
namespace {
  LHCb::Track::Types SetTrackType( const LHCb::Track& t ) {
    //  enum  	Types  {
    //    TypeUnknown = 0, Velo, VeloR, Long,
    //    Upstream, Downstream, Ttrack, Muon,
    //    Calo, TT, VeloPix
    //  }
    const auto& lhcbIDs    = t.lhcbIDs();
    bool        hasIT      = false;
    bool        hasOT      = false;
    bool        hasVelo    = false;
    bool        hasVeloPhi = false;
    bool        hasTT      = false;
    //  bool hasMuon = false;

    std::for_each( lhcbIDs.begin(), lhcbIDs.end(), [&]( auto id ) {
      switch ( id.detectorType() ) {
      case LHCb::LHCbID::channelIDtype::Velo:
        hasVelo = true;
        if ( id.isVeloPhi() ) hasVeloPhi = true;
        return;
      case LHCb::LHCbID::channelIDtype::TT:
        hasTT = true;
        return;
      case LHCb::LHCbID::channelIDtype::IT:
        hasIT = true;
        return;
        ;
      case LHCb::LHCbID::channelIDtype::OT:
        hasOT = true;
        return;
        ;
      case LHCb::LHCbID::channelIDtype::Muon:
        //	hasMuon = true;
      default:
        return;
      }
    } );
    bool hasT = hasIT || hasOT;

    if ( hasVelo && hasT ) return LHCb::Track::Types::Long;
    if ( hasVelo && hasTT ) return LHCb::Track::Types::Upstream;
    if ( hasTT && hasT ) return LHCb::Track::Types::Downstream;
    if ( hasVelo ) return hasVeloPhi ? LHCb::Track::Types::Velo : LHCb::Track::Types::VeloR;
    return LHCb::Track::Types::TypeUnknown;
  }

  template <class T>
  struct StringWithSetter {
    StringWithSetter( std::string aname, void ( T::*afunction )( double ) )
        : name( std::move( aname ) ), function( afunction ) {}
    std::string name;
    void ( T::*function )( double );
  };
} // namespace

void HltTrackConverter::RemoveClones( LHCb::Track::Vector& tracks ) const {
  std::vector<LHCb::Track*> alltracks( tracks.begin(), tracks.end() );
  std::sort( alltracks.begin(), alltracks.end(),
             []( const LHCb::Track* lhs, const LHCb::Track* rhs ) { return lhs->nLHCbIDs() > rhs->nLHCbIDs(); } );
  tracks.clear();
  for ( auto& alltrack : alltracks ) {
    auto found = std::any_of( tracks.begin(), tracks.end(), [&]( const LHCb::Track* trk ) {
      size_t nOverlap = trk->nCommonLhcbIDs( *alltrack );
      size_t minN     = std::min( trk->lhcbIDs().size(), alltrack->lhcbIDs().size() );
      return nOverlap >= minN * m_CloneOverlapTreshold;
    } );
    if ( found ) {
      delete alltrack;
      alltrack = nullptr;
    } else {
      tracks.push_back( alltrack );
    }
  }
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "CloneRemoval kept " << tracks.size() << " out of " << alltracks.size() << " tracks." << endmsg;
}

LHCb::Tracks HltTrackConverter::operator()( const LHCb::HltSelReports& selReports ) const {
  if ( m_HltLinesFrom1stEvent.value() ) {
    initializeTriggerLists(); // side effects
    m_HltLinesFrom1stEvent.value() = false;
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  LHCb::Track::Vector tracks;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Retrieved HltSelReports with size: " << selReports.size() << endmsg;
  for ( auto& l : m_HltLines ) {
    if ( const LHCb::HltObjectSummary* selReport = selReports.selReport( l ); selReport )
      executeRecursive( tracks, *selReport );
  }
  RemoveClones( tracks );
  LHCb::Tracks trackcontainer;
  for ( auto& track : tracks ) trackcontainer.insert( track );

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Inserting track collection with " << trackcontainer.size() << " tracks to " << outputLocation()
            << endmsg;

  return trackcontainer;
}

void HltTrackConverter::executeRecursive( LHCb::Track::Vector& tracks, const LHCb::HltObjectSummary& SelRep ) const {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "called executeRecursive(" << SelRep << ");" << endmsg;
  for ( auto& child : SelRep.substructure() )
    if ( child ) executeRecursive( tracks, *child );

  if ( SelRep.summarizedObjectCLID() == LHCb::Track::classID() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "found track" << endmsg; // never prints anything
  } else if ( m_requireTrackClassID.value() )
    return;

  // collect the LHCbIDs
  LHCb::Track::LHCbIDContainer acceptedlhcbids;
  for ( auto& LhcbID : SelRep.lhcbIDs() )
    if ( m_useHitsFrom.value().test( LhcbID ) ) {
      if ( LhcbID.isOT() && LhcbID.otID().straw() == 0 )
        warning() << "Skipping invalid LHCbID: " << LhcbID << endmsg;
      else {
        acceptedlhcbids.push_back( LhcbID );
        if ( msgLevel( MSG::DEBUG ) ) debug() << "added" << LhcbID << " to track." << endmsg;
      }
    }

  if ( acceptedlhcbids.size() >= m_MinimalHits ) {

    // create a state from the info fields
    bool        stateIsValid = false;
    LHCb::State state;
    if ( m_addFirstState.value() ) {

      // we could also do this in initialize. but then, who cares.
      std::array<StringWithSetter<LHCb::State>, 6> trackStateFields{
          {{"0#Track.firstState.z", &LHCb::State::setZ},
           {"1#Track.firstState.x", &LHCb::State::setX},
           {"2#Track.firstState.y", &LHCb::State::setY},
           {"3#Track.firstState.tx", &LHCb::State::setTx},
           {"4#Track.firstState.ty", &LHCb::State::setTy},
           {"5#Track.firstState.qOverP", &LHCb::State::setQOverP}}};

      // decode first state from numericalInfo() and add it to track
      stateIsValid                                      = true;
      const LHCb::HltObjectSummary::Info& numericalInfo = SelRep.numericalInfo();
      for ( auto& trackStateField : trackStateFields ) {
        auto aninfo = numericalInfo.find( trackStateField.name );
        if ( aninfo != numericalInfo.end() )
          std::invoke( trackStateField.function, state, aninfo->second );
        else {
          warning() << "Cannot find numerical info for field " << trackStateField.name << endmsg;
          stateIsValid = false;
        }
      }
    }

    if ( stateIsValid || !m_addFirstState.value() ) {
      // create the track, fill it and add to track list
      auto* t = new LHCb::Track();
      t->setLhcbIDs( acceptedlhcbids );
      t->setType( SetTrackType( *t ) );
      t->setHistory( LHCb::Track::History::HLTImportedTrack );
      if ( stateIsValid ) t->addToStates( state );
      tracks.push_back( t );
    }
  }
}

//=============================================================================
