/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "TrackInterfaces/ITrackFitter.h"
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <map>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : ChargedParticlesToTracks
//
// 2012-10-08 : Frederic Dupertuis
//-----------------------------------------------------------------------------

/** @class ChargedParticlesToTracks ChargedParticlesToTracks.h
 *
 *
 *  @author Frederic Dupertuis
 *  @date   2012-10-08
 */
class ChargedParticlesToTracks : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  void DumpTracks( LHCb::Particle::ConstVector particles, LHCb::Track::Container& out );

  Gaudi::Property<bool>                     m_refit{this, "RefitTracks", false};
  Gaudi::Property<float>                    m_masswindow{this, "MassWindow", -1.};
  Gaudi::Property<float>                    m_massoffset{this, "MassOffset", 0.};
  Gaudi::Property<std::vector<std::string>> m_partloc{
      this, "ParticlesLocations", {"/Event/CharmCompleteEvent/Phys/D2hhCompleteEventPromptD2KPiLine/Particles"}};
  DataObjectWriteHandle<LHCb::Track::Container> m_trackOutputLocation{this, "TracksOutputLocation",
                                                                      "/Event/Rec/Track/MyBest"};

  ToolHandle<ITrackFitter> m_trackFit{this, "Fitter", "TrackMasterFitter/fit"};
  ToolHandle<ITrackFitter> m_trackPreFit{this, "PreFitter"
                                               "TrackMasterFitter/preFit"};

  std::map<std::string, std::string> m_linesname;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedParticlesToTracks )

//=============================================================================
// Initialization
//=============================================================================
StatusCode ChargedParticlesToTracks::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&] {
    if ( fullDetail() ) {
      std::transform( m_partloc.begin(), m_partloc.end(), std::inserter( m_linesname, m_linesname.end() ),
                      [tokens = std::vector<std::string>{}]( const std::string& s ) mutable {
                        tokens.clear();
                        boost::split( tokens, s, boost::is_any_of( "/" ) );
                        return std::make_pair( s, *std::prev( tokens.end(), 2 ) );
                      } );
    }
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedParticlesToTracks::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  auto trackCont = new LHCb::Track::Container();
  m_trackOutputLocation.put( trackCont );

  auto propertysvc = service<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  LHCb::Particle::ConstVector particles;

  for ( const auto& loc : m_partloc ) {
    if ( !exist<LHCb::Particle::Range>( loc ) ) continue;
    for ( const auto& p : get<LHCb::Particle::Range>( loc ) ) {
      auto prop = propertysvc->find( p->particleID() );
      if ( m_masswindow < 0. || std::abs( p->measuredMass() - ( prop->mass() + m_massoffset ) ) < m_masswindow ) {
        particles.push_back( p );
        if ( fullDetail() ) {
          plot( p->measuredMass(), m_linesname[loc], prop->mass() + m_massoffset - 1.5 * m_masswindow,
                prop->mass() + m_massoffset + 1.5 * m_masswindow, (int)( 3 * m_masswindow ) );
        }
      }
    }
  }

  if ( particles.empty() ) {
    setFilterPassed( false );
    return StatusCode::SUCCESS;
  }

  DumpTracks( particles, *trackCont );

  return StatusCode::SUCCESS;
}

//=============================================================================

void ChargedParticlesToTracks::DumpTracks( LHCb::Particle::ConstVector particles, LHCb::Track::Container& trackCont ) {
  for ( const auto& p : particles ) {
    if ( p->isBasicParticle() ) {
      if ( auto proto = p->proto(); proto && proto->charge() != 0 ) {
        if ( auto track = p->proto()->track(); track ) {
          auto refittedtrack = const_cast<LHCb::Track*>( p->proto()->track() );
          if ( m_refit.value() ) {
            ( *m_trackPreFit )( *refittedtrack ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
            ( *m_trackFit )( *refittedtrack ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
          }
          if ( find( trackCont.begin(), trackCont.end(), refittedtrack ) == trackCont.end() ) {
            trackCont.add( refittedtrack );
          }
        }
      }
    } else {
      DumpTracks( p->daughtersVector(), trackCont );
    }
  }
}
