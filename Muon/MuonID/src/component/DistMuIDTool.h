/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DISTMUIDTOOL_H
#define DISTMUIDTOOL_H 1

// Include files
#include <bitset>
// from Gaudi
#include "Chi2MuIDTool.h"
#include "GaudiAlg/GaudiTool.h"

/** @class DistMuIDTool DistMuIDTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2009-03-12
 */
class DistMuIDTool : public Chi2MuIDTool {
public:
  /// Standard constructor
  using Chi2MuIDTool::Chi2MuIDTool;

  StatusCode muonQuality( LHCb::Track& muTrack, double& Quality ) override;

private:
  StatusCode computeDistance( const LHCb::Track& muTrack, double& dist, std::bitset<4> sts_used );

  Gaudi::Property<bool> m_applyIsmuonHits{this, "ApplyIsmuonHits", false};
};
#endif // DISTMUIDTOOL_H
