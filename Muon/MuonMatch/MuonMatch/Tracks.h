/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_TRACKS_H
#define MUONMATCH_TRACKS_H 1

// STL
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// LHCb
#include "Event/RecVertex_v2.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "PrKernel/PrSelection.h"

namespace MuonMatch {

  /// Track aliases
  using Track          = LHCb::Event::v2::Track;
  using Tracks         = std::vector<Track>;
  using TrackSelection = Pr::Selection<Track>;
  using Vertex         = LHCb::Event::v2::RecVertex;
  using Vertices       = std::vector<Vertex>;

  /// Definition of the muon chamber indexes.
  enum MuonChamber { M2, M3, M4, M5 };

  /// Definition of the track types
  enum TrackType { VeryLowP, LowP, MediumP, HighP };

  /// Calculate the squared impact parameter of a track with respect to a vertex
  template <class TRACK = Track> // CHANGE THIS WHEN TRACK VERSION IS SET
  inline auto squared_impact_parameter( const TRACK& track, const Vertex& vertex ) {
    const auto& pos   = vertex.position();
    const auto& state = track.closestState( pos.Z() );
    const auto  line  = Gaudi::Math::Line{state.position(), state.slopes()};
    const auto  ipvec = Gaudi::Math::closestPoint( pos, line ) - pos;

    return ipvec.Mag2();
  }

  /// Return the track type associated to a given momentum
  template <class value>
  inline TrackType trackTypeFromMomentum( value p ) {
    // The following quantities have been set from the references
    // for offline muons in the MuonID, giving room for resolution.

    if ( p < 2.5 * Gaudi::Units::GeV ) // 3 GeV/c for offline muons
      return VeryLowP;
    else if ( p < 7 * Gaudi::Units::GeV ) // 6 GeV/c for offline muons
      return LowP;
    else if ( p < 12 * Gaudi::Units::GeV ) // 10 GeV/c for offline muons
      return MediumP;
    else
      return HighP;
  }

  /// Calculate the projection in the magnet for the "x" axis
  template <class value>
  inline auto xStraight( const LHCb::State& state, value z ) {
    return state.x() + ( z - state.z() ) * state.tx();
  }

  /// Calculate the projection in the magnet for the "y" axis
  template <class value>
  inline auto yStraight( const LHCb::State& state, value z ) {
    return state.y() + ( z - state.z() ) * state.ty();
  }

  /// Calculate the projection in the magnet for the "y" axis
  template <class value>
  inline std::tuple<value, value> yStraightWindow( const LHCb::State& state, value z, value yw ) {
    const auto dz = z - state.z();
    const auto y  = state.y() + dz * state.ty();
    const auto r  = dz * std::sqrt( state.errTy2() ) + yw;

    return {y - r, y + r};
  }

} // namespace MuonMatch

#endif // MUONMATCH_TRACKS_H
