/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files
// from STL

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// local
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonTilePosition.h"

/** @class MuonFastPosTool MuonFastPosTool.h
 *
 *  Convert an MuonTileID into an xyz position in the detector (with size)
 *  No abstract interface as I do not want to make more than one of these...
 *
 *  @author David Hutchcroft
 *  @date   07/03/2002
 */

namespace {
  StatusCode adjust( LHCb::Muon::ComputeTilePosition::Result r, double& x, double& deltax, double& y, double& deltay,
                     double& z ) {
    x      = r.p.X();
    y      = r.p.Y();
    z      = r.p.Z();
    deltax = r.dX;
    deltay = r.dY;
    return StatusCode::SUCCESS;
  }
} // namespace

class MuonFastPosTool : public extends<GaudiTool, IMuonFastPosTool> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override {

    StatusCode sc = GaudiTool::initialize();
    if ( sc.isFailure() ) return sc;
    DeMuonDetector* muonDetector = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" );
    m_compute.emplace( *muonDetector );
    return StatusCode::SUCCESS;
  }

  /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
   * this ignores gaps: these can never be read out independently
   */
  StatusCode calcTilePos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay, double& z,
                          double& /*deltaz*/ ) const override {
    return adjust( m_compute->tilePosition( tile ), x, deltax, y, deltay, z );
  }

  StatusCode calcStripXPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& /*deltaz*/ ) const override {
    return adjust( m_compute->stripXPosition( tile ), x, deltax, y, deltay, z );
  }

  StatusCode calcStripYPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& /*deltaz*/ ) const override {
    return adjust( m_compute->stripYPosition( tile ), x, deltax, y, deltay, z );
  }

private:
  std::optional<LHCb::Muon::ComputeTilePosition> m_compute;
};
DECLARE_COMPONENT( MuonFastPosTool )

//=============================================================================
